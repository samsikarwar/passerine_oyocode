<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use DB;
use Carbon\Carbon;
use App\Customer,App\Role;
use App\Reservation;
use App\Room;
use App\FoodCategory;
use App\Order,App\OrderItem,App\OrderHistory;
use Illuminate\Support\Facades\Hash;
class ApiController extends Controller
{
    //
    public function getCheckinDetails(Request $request)
    {
     $room_number=$request->room_number;
     
     $r=DB::table('reservations')->where('room_num',$room_number)->first();
     if($r)
     {
     $c=DB::table('customers')->where('id',$r->customer_id)->first();         
     $array['roomDetails']['roomId']=$r->room_num;
     $array['roomDetails']['roomNo']=$r->room_num;
     $array['roomDetails']['propertyCode']=1;
     $array['roomDetails']['checkInDate']=$r->check_in;
     $array['roomDetails']['checkOutDate']=$r->check_out;
     $array['roomDetails']['bookingId']=$r->unique_id;
     $array['roomDetails']['status']="CHECK_IN";
     $array['roomDetails']['noOfGuests']['total']=$r->adult ?? 0 + $r->kids ?? 0;
     $array['roomDetails']['noOfGuests']['Adults']=$r->adult ?? 0;
     $array['roomDetails']['noOfGuests']['Children']=$r->kids ?? 0;
     
     $array['roomDetails']['rateCode']='';
     $array['roomDetails']['remark']='';
     $array['roomDetails']['package']='';
     $array['roomDetails']['billingInstruction']='';
     $array['roomDetails']['onlineOrderPref']['payNow']=true;
     $array['roomDetails']['onlineOrderPref']['payAtCheckout']=false;
     
     $array['roomDetails']['guestDetails']['guestId']='';
     $array['roomDetails']['guestDetails']['firstName']=$c->name;
     $array['roomDetails']['guestDetails']['middleName']='';
     $array['roomDetails']['guestDetails']['lastName']='';
     $array['roomDetails']['guestDetails']['gender']=$c->gender;
     $array['roomDetails']['guestDetails']['email']=$c->email;
     $array['roomDetails']['guestDetails']['mobile']=$c->mobile;
     $array['roomDetails']['guestDetails']['nationality']='';
     $array['roomDetails']['guestDetails']['homeCountry']=''; 
     return response()->json(['data'=>$array,'status'=>true],200);
     }else
     {
         return response()->json(['data'=>[],'status'=>false],500);
     }
    }
    
    public function emailLogin(Request $request)
    {
        
        $email=$request->email;
        $password=$request->password;
         
        $user=User::where('email',$email)->first();
         
         if(!empty($user))
         {
             if(Hash::check($password,$user->password))
             {
             return response()->json(['message'=>'valid user','data'=>$user],200);
             }else
             {
               return response()->json(['message'=>'invalid user','data'=>[]],500);  
             }
         }else
         {
             return response()->json(['message'=>'invalid user','data'=>[]],500);
         }
        
    }
    
    
    
    
//     public function newchecking(Request $request)
//     {
//       $date=$request->check_in_date; 
//       $todayDate = Carbon::now();
// $checkoutdatatime=$todayDate->addDays($request->duration_of_stay);
// $time = date("H:i:s");
// $datetime = $date ." ". $time;
// Carbon::useStrictMode(false);
// $to_date = Carbon::parse($checkoutdatatime);
// $from_date = Carbon::parse($datetime);
// $room = $request->per_room_price;
// $booking = $request->booking_payment;
// if(isset($request->document_upload)){
//         $documentPath = $request->document_upload->store('public/files');
       
//     }else{
//         $documentPath = $request->document_id ?? '';
//     }
// if($request->customer_id){
//     $customer_id=$request->customer_id;
//     $custData = Customer::whereId($customer_id)->first();
//     $custName = $custData->name;
//     $customerId = $customer_id;
// }else
// {
//         $dateOfBirth = dateConvert($request->age, 'Y-m-d');
//         $years = Carbon::parse($dateOfBirth)->age;
//         $custName = $request->name;
//         $customerData = [
//         "Booking_id" => $request->name.rand(0000,9999),
//         "name" => $request->name,
//         "father_name" => $request->father_name,
//         "email" => $request->email,
//         "mobile" => $request->mobile,
//         "address" => $request->Address,
//         "nationality" => $request->nationality,
//         "country" => $request->country,
//         "state" => $request->state,
//         "city" => $request->city,
//         "gender" => $request->gender,
//         "dob" => dateConvert($request->age, 'Y-m-d'),
//         "age" => $years,
//         "password" => Hash::make($request->mobile),
//         "document" =>   $documentPath,
//       ];
//       $customerId = Customer::insertGetId($customerData);
// }


//       $reservationData = [
//         "customer_id" => $customerId,
//          "booking_payment" => $booking,
//          "room_qty" => 1,
//          "unique_id"=>uniqid(),
//          "per_room_price" => $request->booking_payment,
//          "guest_type" => $request->guest_type,
//          "check_in" => $datetime,
//          "infant" => $request->infant ?? 0,
//          "user_checkout" => $checkoutdatatime,
//          "duration_of_stay" => $request->duration_of_stay,
//          "Booking_Reason" => $request->Booking_Reason,
//          "room_type_id" => $request->room_type_id,
//          "room_num" => $request->room_num,
//          "adult" => $request->adult,
//          "kids" => $request->kids,
//          "booked_by" => $request->booked_by,
//          "vehicle_number" => $request->vehicle_number,
//          "reason_visit_stay" => $request->reason_visit_stay,
//          "advance_payment" => $request->advance_payment,
//          "idcard_type" => $request->idcard_type,
//          "idcard_no" => $request->idcard_no,
//          "idcard_image" => $documentPath,
//          "payment_mode" =>$request->payment_mode,
//          "meal_plan" => $request->meal_plan,
//          "corporates" => $request->corporate,
//          "tas" => $request->ta,
//          "ota" => $request->ota,
//          "referred_by_name" => $request->referred_by_name,
//          "referred_by" => '',
//          "remark_amount" => $request->remark_amount,
//          "remark" => $request->remark,
//          "package_id" => $request->package_id ?? '',
//          "checkin_type" => 'single',
//          "Employee_Check_In_name" => ''
         
//      ];
//      if(!$request->id){
//         $reservationData["created_at_checkin"] = date('Y-m-d H:i:s');
//     }
//     $res = Reservation::updateOrCreate(['id'=>$request->id],$reservationData);
//         $paytmParams["body"] = array(
//         "clientId"             => "8e5a3jsp0hh7",
//         "clientSecret"        => "xsW3fI3q45",
//     );
    
   
//         $history['payment']=$request->advance_payment ?? 0;
//         $history['mode']=$request->payment_mode;
//         $history['payment_date']=date('Y-m-d');
//         $history['reservations_id']=$res->id;
//     DB::table("payment_history")->insert($history);
//  return response()->json(['status'=>200,'message'=>'Booking Confirm','data'=>$request->all()],200);
//     }
    //new code start 8-2-2022
    public function newchecking(Request $request)
    {   
        if($request->room_qty){
            $room_qty= $request->room_qty;
        }
        else{
            $room_qty=1;
        }
        
       // $date=$request->check_in_date; 
        $date = dateConvert($request->check_in_date, 'Y-m-d');
        
        $todayDate = Carbon::now();
        $checkoutdatatime=$todayDate->addDays($request->duration_of_stay);
        $time = date("H:i:s");
        $datetime = $date ." ". $time;
        Carbon::useStrictMode(false);
        $to_date = Carbon::parse($checkoutdatatime);
        $from_date = Carbon::parse($datetime);
        $room = $request->per_room_price;
        $booking = $request->booking_payment;
            if(isset($request->document_upload)){
                $documentPath = $request->document_upload->store('public/files');
            }else{
                $documentPath = $request->document_id ?? '';
            }
            if($request->customer_id){
                $customer_id=$request->customer_id;
                $custData = Customer::whereId($customer_id)->first();
                $custName = $custData->name;
                $customerId = $customer_id;
            }
            else
            {
                $dateOfBirth = dateConvert($request->age, 'Y-m-d');
                $years = Carbon::parse($dateOfBirth)->age;
                $custName = $request->name;
                $customerData = [
                "Booking_id" => $request->name.rand(0000,9999),
                "name" => $request->name,
                "father_name" => $request->father_name,
                "email" => $request->email,
                "mobile" => $request->mobile,
                "address" => $request->Address,
                "nationality" => $request->nationality,
                "country" => $request->country,
                "state" => $request->state,
                "city" => $request->city,
                "gender" => $request->gender,
                "dob" => dateConvert($request->age, 'Y-m-d'),
                "age" => $years,
                "password" => Hash::make($request->mobile),
                "document" =>   $documentPath,
               ];
                $customerId = Customer::insertGetId($customerData);
            }

        $hello = $request->room_num;
        $jc = explode(",",$hello);
        $room_count = count($jc);
       
        if($room_count == 1){
            $reservationData = [
            "customer_id" => $customerId,
            "booking_payment" => $booking,
            "room_qty" => 1,
            "unique_id"=>uniqid(),
            "per_room_price" => $request->booking_payment,
            'total_amount'=>$request->total_amount,
            "guest_type" => $request->guest_type,
            "check_in" => $datetime,
            "infant" => $request->infant ?? 0,
            "user_checkout" => $checkoutdatatime,
            "duration_of_stay" => $request->duration_of_stay,
            "Booking_Reason" => $request->Booking_Reason,
            "room_type_id" => $request->room_type_id,
            "room_num" => $request->room_num,
            "adult" => $request->adult,
            "kids" => $request->kids,
            "booked_by" => $request->booked_by,
            "vehicle_number" => $request->vehicle_number,
            "reason_visit_stay" => $request->reason_visit_stay,
            "advance_payment" => $request->advance_payment,
            "idcard_type" => $request->idcard_type,
            "idcard_no" => $request->idcard_no,
            "idcard_image" => $documentPath,
            "payment_mode" =>$request->payment_mode,
            "meal_plan" => $request->meal_plan,
            "corporates" => $request->corporate,
            "tas" => $request->ta,
            "ota" => $request->ota,
            "referred_by_name" => $request->referred_by_name,
            "referred_by" => '',
            "remark_amount" => $request->remark_amount,
            "remark" => $request->remark,
            "package_id" => $request->package_id ?? '',
            "checkin_type" => 'single',
            "Employee_Check_In_name" => ''
        ];
        
            if(!$request->id){
                $reservationData["created_at_checkin"] = date('Y-m-d H:i:s');
            }
            $res = Reservation::updateOrCreate(['id'=>$request->id],$reservationData);
            $paytmParams["body"] = array(
                "clientId"             => "8e5a3jsp0hh7",
                "clientSecret"        => "xsW3fI3q45",
            );
    
            $history['payment']=$request->advance_payment ?? 0;
            $history['mode']=$request->payment_mode;
            $history['payment_date']=date('Y-m-d');
            $history['reservations_id']=$res->id;
            $history['remark']='Advance';
            DB::table("payment_history")->insert($history);
        }
        elseif($room_count > 1)
        {
            $unique_id=uniqid();
            foreach($jc as $rm_num)
            {
                //print_r($rm_num);die;
                $reservationData = [
                    "customer_id" => $customerId,
                    'unique_id'=>$unique_id,
                    "booking_payment" => $booking/$room_count,
                    'total_amount'=>$request->total_amount,
                    "per_room_price" => $request->booking_payment,
                    "room_qty" => $room_qty/$room_count,//$request->room_qty,
                    "guest_type" => $request->guest_type,
                    "check_in" => $datetime,
                    "Booking_Reason" => $request->Booking_Reason,
                    "user_checkout" => $checkoutdatatime,
                    "duration_of_stay" => $request->duration_of_stay,
                    "room_type_id" => $request->room_type_id,
                    "room_num" => $rm_num,
                    "adult" => $request->adult,
                    "infant" => $request->infant,
                    "kids" => $request->kids,
                    "booked_by" => $request->booked_by,
                    "vehicle_number" => $request->vehicle_number,
                    "reason_visit_stay" => $request->reason_visit_stay,
                    "advance_payment" => $request->advance_payment,
                    "sec_advance_payment" => $request->sec_advance_payment,
                    "sec_payment_mode" => $request->sec_payment_mode,
                    "idcard_type" => $request->idcard_type,
                    "idcard_no" => $request->idcard_no,
                    "idcard_image" => $documentPath,
                    "payment_mode" =>$request->payment_mode,
                    "meal_plan" => $request->meal_plan,
                    "corporates" => $request->corporate,
                    "tas" => $request->ta,
                    "ota" => $request->ota,
                    "referred_by_name" => $request->referred_by_name,
                    "referred_by" => '',
                    "remark_amount" => $request->remark_amount,
                    "remark" => $request->remark,
                    "package_id" => $request->package_id,
                    "checkin_type" => 'multiple',
                    "Employee_Check_In_name" => '',
                ];
                if(!$request->id){
                    $reservationData["created_at_checkin"] = date('Y-m-d H:i:s');
                }
                $res = Reservation::updateOrCreate(['id'=>$request->id],$reservationData);
                $paytmParams["body"] = array(
                    "clientId"             => "8e5a3jsp0hh7",
                    "clientSecret"        => "xsW3fI3q45",
                );
                //Setting::where('name', 'mid')->update(['value'=>$mid]);
                
                $history['payment']=$request->advance_payment/$room_count ?? 0;
                $history['mode']=$request->payment_mode;
                $history['payment_date']=date('Y-m-d');
                $history['reservations_id']=$res->id;
                $history['remark']='Advance';
                
                DB::table("payment_history")->insert($history);
            
            }
        }
        return response()->json(['status'=>200,'message'=>'Booking Confirm','data'=>$request->all()],200);
    }
    //new code end 8-2-2022
    
    public function roomInfo($hotel_id,$room_num)
    {
       
        $res['room_info']=DB::table('rooms')
        ->join('room_types','room_types.id','=','rooms.room_type_id')
        ->select('rooms.room_no','rooms.floor','rooms.reason','rooms.maintinance','room_types.title','room_types.short_code','room_types.adult_capacity','room_types.kids_capacity','room_types.base_price')
        ->where('room_types.is_deleted','0')
        ->where('rooms.room_no',$room_num)
        ->get();
        
          $res['booking_info']=DB::table('rooms')
         ->join('reservations','reservations.room_num','rooms.room_no')
         ->join('customers','customers.id','reservations.customer_id')
         ->select('reservations.id as bookingId','customers.name','customers.email','customers.mobile','customers.address','reservations.room_num','reservations.per_room_price','reservations.check_in','reservations.check_out','reservations.duration_of_stay','reservations.adult','reservations.kids')
         ->where('rooms.room_no',$room_num)
         ->get();
        
        if(count($res)>0)
        {
            return response()->json(['status'=>200,'data'=>$res],200);
        }else
        {
           return response()->json(['status'=>500,'data'=>[]],500); 
        }
        
        
    }

    public function billInfo($bookingId)
    {
       
        $res=DB::table('order_items')
        ->where('reservation_id',$bookingId)
        ->select('order_items.reservation_id as bookingId','order_items.item_name','order_items.item_price','order_items.item_qty','order_items.created_at')
        ->get();
        
        if(count($res)>0)
        {
            return response()->json(['status'=>200,'data'=>$res],200);
        }else
        {
           return response()->json(['status'=>500,'data'=>[]],500); 
        }
        
        
    }
   
      public function billsave(Request $request)
      {
         
         //dd($request->all());
         $data['reservation_id']=$request->bookingId;
         $data['total_amount']=$request->total_amount;
         $data['gst_apply']=$request->gst_apply;
         $data['gst_perc']=$request->gst_perc;
         $data['gst_amount']=$request->gst_amount;
         $data['cgst_perc']=$request->cgst_perc;
         $data['cgst_amount']=$request->sgst_perc;
         $data['discount']=$request->discount;
         DB::table('orders')->insert($data);
         foreach($request->item as $i)
         {
             $insert['item_name']=$i['item_name'];
             $insert['item_price']=$i['item_price'];
             $insert['item_price']=$i['item_qty'];
             $insert['status']=$i['status'];
             DB::table('order_items')->insert($insert);
         }
       
        return response()->json(['status'=>200,'message'=>'Successfully Saved'],200);
        
        
        
          
      }
      
    public function search(Request $request)
    {
        $data=Customer::select(DB::raw("CONCAT(customers.name,'-',customers.mobile,'-',customers.id) as name",""),"mobile","id")
        ->where("name","LIKE","%{$request->input('query')}%")
        ->orWhere( "mobile","LIKE","%{$request->input('query')}%")
        ->get();
        
        if(count($data)>0)
        {
            return response()->json(['status'=>200,'data'=>$data],200);
        }else
        {
           return response()->json(['status'=>500,'data'=>[]],500); 
        }
    }
    
    
    public function paymentMode(Request $request)
    {
        $data=DB::table('payment_mode')->get();
        if(count($data)>0)
        {
            return response()->json(['status'=>200,'data'=>$data],200);
        }else
        {
           return response()->json(['status'=>500,'data'=>[]],500); 
        }
    }
    
    public function roomType(Request $request)
    {
        $data=DB::table('room_types')->get();
        if(count($data)>0)
        {
            return response()->json(['status'=>200,'data'=>$data],200);
        }else
        {
           return response()->json(['status'=>500,'data'=>[]],500); 
        }
    }
    
    
    public function getRoom(Request $request)
    {
       
        $checkin_date = date('Y-m-d');//$request->checkin_date;
        $bookedRooms = [];
        $undermaintinance = [];
        $dirtyRooms= [];
        $reservationData = Reservation::whereStatus(1)->whereIsDeleted(0)->orderBy('room_num','ASC')->select('room_num','check_in','check_out','referred_by_name','user_checkout','id')->get();

        if($reservationData->count()>0){
            foreach($reservationData as $val){
                $exp = explode(',', $val->room_num);
                $count = count($exp);
                if( date('Y-m-d',strtotime($val->user_checkout)) == $checkin_date || ($val->check_out == null))
                {
                    if($val->check_out == null)
                    {
                        for($i=0; $i<$count; $i++)
                        {
                            $bookedRooms[$exp[$i]] = $exp[$i];
                        }
                    }
                    else
                    {
                        for($i=0; $i<$count; $i++)
                        {
                            $dirtyRooms[$exp[$i]] = $exp[$i];
                        }
                    }
                }
                if($val->check_out != null)
                {
                    for($i=0; $i<$count; $i++)
                        {
                            $dirtyRooms[$exp[$i]] = $exp[$i];
                        }
                }
               
            }
        }
        
        $this->data['booked_rooms'] = $bookedRooms;
        $this->data['dirty_rooms'] = $dirtyRooms;
        $resrvation = Room::whereStatus(1)->whereIsDeleted(0)->where(['room_type_id'=>$request->id])->orderBy('room_no','ASC')->get();
      
        $k=1;
        $newRoomd=[];
        foreach($resrvation as $r)
        {
            $newRoomd[$k]=$r['room_no'];
            $k++;
        }
      
       
        
        $reservationData2=Room::whereStatus(1)->whereIsDeleted(0)->where(['maintinance' =>1])->orderBy('room_no','ASC')->pluck('room_no','id');
        if($reservationData2->count()>0){
            foreach($reservationData2 as $val){
                $exp = explode(',', $val);
                $count = count($exp);
                for($i=0; $i<$count; $i++){
                    $undermaintinance[$exp[$i]] = $exp[$i];
                }
            }
        }
        $this->data['undermaintinance'] =$undermaintinance;
        $newList=array_diff($newRoomd,$bookedRooms);
        $this->data['rooms']=array_diff($newList,$undermaintinance);
         return response()->json(['status'=>200,'data'=>$this->data],200);
    }
    
      public function referred_by(Request $request)
      {
         $constants[]=config('constants.REFERRED_BY_NAME');
           return response()->json(['status'=>200,'data'=>$constants],200);
      }
      
    public function referred_by_data(Request $request)
    {
        $data=[];
        if($request->key=='OTA')
        {
            $data=DB::table('ota')->get();
              
        }
        if($request->key=='TA')
        {
            $data=DB::table('tas')->get();
        }
          
        if($request->key=='Corporate')
        {
            $data=DB::table('corporates')->get();
        }

           return response()->json(['status'=>200,'data'=>$data],200);
      }
    
    public function f9alist(Request $request){
        
          date_default_timezone_set("Asia/Kolkata");
          $amount = $request->amount;
          $bookingdetailurl=$request->invoice;
          $ptypenew = $request->payment_mode;
          $customerData = [
                "name" => $request->firstname,
                "Booking_id" => $request->booking_id,
                "father_name" => "",
                "email" => $request->email,
                "mobile" => $request->mobile,
                "address" => "",
                "nationality" => "",
                "country" => "",
                "state" => "",
                "city" => "",
                "gender" => "",
                "dob" => "",
                "age" => "",
                "password" => Hash::make($request->mobile),
            ];
           
         
            $customerId =DB::table('customers')->insertGetId($customerData);
            
           
            $date=$request->checkin;
            $checkoutdate=$request->checkout;
            $time = date("H:i:s");
            $datetime = $date ." ". $time;
            $checkoutdatatime = $checkoutdate ." ". $time;
            Carbon::useStrictMode(false);
            $to_date = Carbon::parse($checkoutdatatime);
            $from_date = Carbon::parse($datetime);
            $answer_in_days = $to_date->diffInDays($from_date);
            $check_in_day = date('l', strtotime($date));
            if($check_in_day == 'Saturday' &&  $check_in_day == 'Sunday')
            {
                $is_weekend = '1';
            }
            else{
                $is_weekend = '0';
            }
         $data= DB::table('arrivals')->insert([
                "customer_id" => $customerId,
                "check_in" => $datetime,
                "check_out" => $checkoutdatatime,
                "duration_of_stay" => $answer_in_days,
                "adult" => "",
                "kids" => "",
                "infant" => "",
                "Booking_Reason" => "",
                "vehicle_number" => "",
                "corporates" => "",
                "tas" => "",
                "ota" => "",
                "referred_by_name" =>"F9hotels",
                "booked_by" => $request->booked_by ?? "app",
                "room_type_id" => $request->room_id,
                "room_num" => "",
                "room_qty" => "",
                "package_id" => "",
                "check_in_day" => $check_in_day,
                "is_weekend" => $is_weekend,
                "payment" =>$amount,
                "payment_mode"=>"cash",
                "paymenttype" => $ptypenew,
                "bookingdetailurl"=>$bookingdetailurl,
                "bookingstatus" =>"BOOKED"
              ]);
              return response()->json(['status'=>200,'data'=>$data],200);
    }
    
    public function getrooom(Request $request){
        // print_r('hello112');die;
        $roomno = $request->roomno;
        $roomnouserdata = Reservation::whereNull('check_out')->where('room_num',$roomno)->orderBy('created_at','DESC')->distinct('room_num')->get();
        return response()->json([
            'roomnouserdata'=>$roomnouserdata,
        ]);
    }
   
    public function roomfood() { 
        $foodcategory=FoodCategory::with('food_items')->whereStatus(1)->whereIsDeleted(0)->orderBy('name','ASC')->get();
        $this->data['skills']= Room::whereStatus(1)->whereIsDeleted(0)->orderBy('room_no','ASC')->get();
        $this->data['skillsnew'] = Reservation::whereNull('check_out')->orderBy('created_at','DESC')->get('room_num');
        // return view('backend/room_food_order_page',$this->data);
        return response()->json([
            'roomfood'=>$foodcategory,
        ]);
    }
    
public function get_checkin_room()
{
    $room = Reservation::whereNull('check_out')->orderBy('created_at','DESC')->get('room_num');
    return response()->json([
        'room'=>$room,
    ]);
}

public function saveFoodOrder(Request $request){

    // print_r('hello');die;
    // $order = $request->input('orderArr');
    $order = $request->orderArr;

    $order_info = $request->input('orderInfo');
    $tb = $request->input('table_num');
    // print_r($tb);die;
    // print_r($order_info[0]['reservation_id']);die;
    // $reservationId = $order_info['reservation_id'];

    // print_r($request->input('orderInfo.reservation_id'));die;
    
    $orderArr = json_decode($order, true); 
    // $orderId = $orderArr[0]['order_id'];
    // print_r($orderId);die;
            $insertRec = true;
            $insertRecOrderHistorty = true;
            $orderHistoryResId = null;
    
            $invoiceDate = date('Y-m-d');
            $settings = getSettings();
            $orderArr = [];
            $itemsArr = [];
            // print_r('hello12');die;
            // $itemsArr = array_filter($request->item_qty);
            if(count($order_info)>0){
                // print_r('hello');die;
                $orderData = [];
                $gstPerc = $cgstPerc = $gstAmount = $cgstAmount = 0;
                if($request->food_gst_apply==1){
                    $gstPerc = $request->gst_perc;
                    $cgstPerc = $request->cgst_perc;
                    $gstAmount = $request->gst_amount;
                    $cgstAmount = $request->cgst_amount;
                }
    
                $room_no = $request->table_num; 
               
              $reservationid = DB::table('reservations')->where('room_num',$room_no) ->whereNull('check_out')->get();
              $reservation_id = $reservationid[0]->id;
            //   print_r($reservation_id);die;
            // print_r($room_no);die;
                $orderInfo= [
                    'reservation_id'=>$reservation_id,
                    'invoice_num'=>($request->food_invoice_apply=="on") ? getNextInvoiceNo('orders') : null,
                    'invoice_date'=>$order_info['invoice_date'],
                    'table_num'=>$order_info['table_num'],
                    'gst_apply'=>1,
                    'gst_perc'=>1,
                    'gst_amount'=>1,
                    'cgst_perc'=>1,
                    'cgst_amount'=>1,
                    'discount'=>$order_info['discount'],
                    'total_amount'=>$order_info['total_amount'],
                    'name' => $order_info['name'],
                    'email' => $order_info['email'],
                    'mobile' => $order_info['mobile'],
                    'address' => $order_info['address'],
                    'gender' =>$order_info['gender'],
                    'num_of_person' => $order_info['num_of_person'],
                    'waiter_name' => $order_info['waiter_name'],
                    'payment_mode' =>$order_info['payment_mode'],
    
                ];
                // print_r($orderInfo);die;
                if($request->page=='ff_order'){
                    $orderInfo['original_date'] = date('Y-m-d H:i:s');
                    $orderRes = Order::where('id',$orderId)->update($orderInfo);
                   
                    if($orderRes){
                        // print_r($orderRes);die;
                        OrderHistory::where('order_id',$orderId)->update(['is_book'=>0]);
                        //send sms
                        if($order_info['mobile']){
                            $this->core->sendSms(3,$order_info['mobile'],['name'=>$order_info['name']]);
                        }
    
    
                        // $markp['closeorder'] = 1;
                        $id = $orderId;
                        // print_r($id);die;
    
                        DB::table('orders')->where('id', $id)->update(['closeorder' => '1']);
    
                        // $close = Customerfoodorder::where('order_id',$id)->update($markp);
                        // return redirect()->route('order-invoice-final',[$request->order_id])->with(['success' => 'Orders Successfully submitted']);
                        return redirect()->route('latest-orders')->with(['success' => 'Payment done Successfully']);
                        
                    } else {
                        return redirect()->back()->with(['error' => 'Order placed failed.Try again']);
                    }
    
                } else {
                    
                    if($reservation_id>0){
                        $insertRecOrderHistorty = false;
                        // print_r($insertRecOrderHistorty);die;
                    } else {
                        // check table num is booked or not (if table num booked , no new orders row added, added in orderHistory table)
                        $isTableBooked = isTableBook($tb);
                        if($isTableBooked){
                            $insertRec = false;
                            $orderResId = $isTableBooked->order_id;
                        }
                    }
    
                    if($insertRec){
                        

                        // $orderinfoo = json_decode($orderInfo, true); 
                        $jsonData = json_encode($orderInfo);
                        // print_r($orderInfo);die;
                        // $orderResId = Order::insertGetId($jsonData);
                        $orderResId = Order::insertGetId($orderInfo);
                        // print_r($orderResId);die;
                    }
                    // print_r($insertRecOrderHistorty);die;
                  
                        $orderHistoryResId = OrderHistory::insertGetId(['order_id'=>$orderResId, 'table_num'=>$tb]);
                    
    
                    $lastOrderId = $orderResId; // $orderRes->id;

                //    print_r($order);die;
                
                $orders = json_decode($order, true);
                    foreach ($orders as $data) {
                        // $orderID = $data['order_id'];
                        // $orderHistoryID = $data['order_history_id'];
                        // $reservationID = $data['reservation_id'];
                        // $itemName = $data['item_name'];
                        // $itemPrice = $data['item_price'];
                        // $itemQty = $data['item_qty'];
                        // $jsonData = json_decode($data['json_data'], true);
                        // $status = $data['status'];
                        $data2 = DB::table('food_items')
                         ->join('food_categories', 'food_items.category_id', '=', 'food_categories.id')
                         ->where('food_items.name', $data['item_name'])
                         ->select('food_items.category_id','food_categories.name as category_name','food_items.name as item_name','food_items.id as item_id')
                         ->get();

// print_r($data2);die;
                        //  $jsonData2 = json_encode($data2);
                        if ($data2 !== null) {
                            $jsonData2 = json_encode([
                                'category_id' => $data2[0]->category_id,
                                'category_name' => $data2[0]->category_name,
                                'item_name' => $data2[0]->item_name,
                                'item_id' => $data2[0]->item_id
                            ]);
                        //  $jsonData2 = json_encode([
                        //     'category_id' => $data2->category_id,
                        //     'category_name' => $data2->category_name,
                        //     'item_name' => $data2->item_name,
                        //     'item_id' => $data2->item_id
                        // ]);
                        
                        
                        // print_r($jsonData2);die;
                        $orderArr[] = [
                                    'order_id'=>$lastOrderId,
                                    'order_history_id'=>$orderHistoryResId,
                                    'reservation_id'=>$order_info['reservation_id'],
                                    'item_name'=>$data['item_name'],
                                    'item_price'=>$data['item_price'],
                                    'item_qty'=>$data['item_qty'],
                                    'json_data'=>$jsonData2,
                                    'status'=>3
                                ];
                                if (isset($data['item_qty'])) {
                                    $itemsArr[] = $data['item_qty'];
                                }
                            }else{}
                        // Process the data as needed
                    }

                    // foreach($itemsArr as $k=>$val){
                    //     $exp = explode('~', $request->items[$k]);
                    //     $jsonData = ['category_id'=>$exp[0], 'category_name'=>$exp[1], 'item_name'=>$exp[2], 'item_id'=>$k];
                    //      $orderArr[] = [
                    //         'order_id'=>$lastOrderId,
                    //         'order_history_id'=>$orderHistoryResId,
                    //         'reservation_id'=>$reservation_id,
                    //         'item_name'=>$exp[2],
                    //         'item_price'=>$exp[3],
                    //         'item_qty'=>$val,
                    //         'json_data'=>json_encode($jsonData),
                    //         'status'=>3
                    //     ];
                    // }
                    // $jsonData = json_encode($orderArr);
                    // print_r($orderArr);die;
                    $res = OrderItem::insert($orderArr);
    
    
    

                    // print_r($close);die;
                    if($res){
                        if($request->$reservation_id>0) {
    
                           
                            return response()->json(['message'=>'Data saved successfully'],200);
                            // return redirect()->route('room-kitchen-invoice',['order_id'=>$lastOrderId,'order_type'=>'room-order'])->with(['success' => 'Orders Successfully submitted']);
                        }
                        return response()->json(['message'=>'valid user'],200);
                        // return redirect()->route('room-kitchen-invoice',['order_id'=>$orderHistoryResId,'order_type'=>'room-order'])->with(['success' => 'Orders Successfully submitted']);
                    } else {
                        return response()->json(['message'=>'Data saved successfully'],200);
                        // return redirect()->back()->with(['error' => 'Order placed failed.Try again']);
                    }
                }
    
    
            }
            return response()->json(['message'=>'Data Not saved successfully'],200);
            // return redirect()->back()->with(['error' => 'Please item quantity']);
        }
    

public function checkout_users()
{
   
    $yesterday = now()->subDay(); 
    $todayDate = Carbon::now(); 
    $yesterday = $todayDate->subDay();
    $yesterdayDate = $yesterday->format('Y-m-d');

     DB::table('reservations')
     ->whereDate('check_in', $yesterdayDate)
     ->update(['check_out' => now()]);

    // $data = DB::table('reservations')->where('check_in')->update(['check_out',]);
}

public function oyo_data(Request $request)
{
   
    
    if (!$request->header('Authorization')) {
        return response()->json(['error' => 'Unauthorized'], 401);
    }
    $authorization = $request->header('Authorization');
    $credentials = base64_decode(substr($authorization, 6));
    list($username, $password) = explode(':', $credentials);
    if ($username !== 'oyo' || $password !== 'passerine@123') {
        return response()->json(['error' => 'Unauthorized'], 401);
    }
   // Capture incoming booking data from the request
$bookingData = $request->json()->all();



    $firstName = $bookingData['guest']['firstName'];
    $lastName = $bookingData['guest']['lastName'];
    $email = $bookingData['guest']['email'];
    $phone = $bookingData['guest']['phone'];
    $Booking_id = $bookingData['cmBookingId'];
    $hotel_code = $bookingData['hotelCode'];

    $customerData = [
        "Booking_id" => $Booking_id,
        "name" => $firstName,
        "email" => $email,
        "mobile" => $phone
    ];
    $customer = Customer::updateOrCreate(
        ['Booking_id' => $Booking_id],
        $customerData
    );
    $customerId = $customer->id;

       date_default_timezone_set("Asia/Kolkata");
        $check_in_d = $bookingData['checkin'];
        $check_in_date = date("Y-m-d", strtotime($check_in_d));
        $check_out_d = $bookingData['checkout'];
        $check_out_date = date("Y-m-d", strtotime($check_out_d));

        $from_date = \Carbon\Carbon::parse($check_in_date);
        $to_date = \Carbon\Carbon::parse($check_out_date);

        $answer_in_days = $to_date->diffInDays($from_date);

        $date = $check_in_date;
        $checkoutdate = $check_out_date;
        $time = date("H:i:s");
        $datetime = $date . " " . $time;
        $checkoutdatatime = $checkoutdate;

    $check_in_day = date('l', strtotime($date));
    if ($check_in_day == 'Saturday' &&  $check_in_day == 'Sunday') {
        $is_weekend = '1';
    } else {
        $is_weekend = '0';
    }

    $payment = $bookingData['amount']['amountAfterTax'];
    $adult = $bookingData['rooms'][0]['occupancy']['adults'];
    $kids = $bookingData['rooms'][0]['occupancy']['children'];
    $refbyname = $bookingData['channel'];
    $room_code = $bookingData['rooms'][0]['roomCode'];
    $roomqty = 1;
 $rateplanCode = $bookingData['rooms'][0]['rateplanCode'];
    $PaymentType = ($bookingData['pah'] == false) ? 'PrePaid' : 'PayAtHotel';
    $BookingDetailUrl = 0;
    $BookingStatus = $bookingData['action'];

    if ($answer_in_days == 0) {
        $answer_in_days = 0;
    } else {
        $answer_in_days = $answer_in_days;
    }

    $res = DB::table('arrivals')
        ->updateOrInsert(
            ['customer_id' => $customerId],
            [
                "check_in" => $datetime,
                "check_out" => $checkoutdatatime,
                "duration_of_stay" => $answer_in_days,
                "adult" => $adult,
                "kids" => $kids,
                "referred_by_name" => $refbyname,
                "room_type_id" => $room_code,
                "room_qty" => $roomqty,
                "check_in_day" => $check_in_day,
                "is_weekend" => $is_weekend,
                "payment" => $payment,
                "paymenttype" => $PaymentType,
                "bookingdetailurl" => $BookingDetailUrl,
                "bookingstatus" => $BookingStatus,
                "oyo_roomcode" =>$room_code,
                "oyo_rateplanCode" => $rateplanCode,
                "hotel_code" => $hotel_code,
            ]
        );


       //data send in pms db for availability
       $today_checkin_timenew = date('Y-m-d');
    
     
       $data2 = DB::table('oyo_avail_datewise')->where('date',$check_in_d)->latest()->first();
      if($BookingStatus=='cancel'){

        $data =DB::table('rooms')->where('is_deleted',0)->get();
        $data_count = ($data->count() - $roomqty);

        // $data_count = ($data2->availability + $roomqty);

        $room_type = DB::table('room_types')->where('id', $room_code)->where('is_deleted',0)->value('title');
       
        $existingRecord_new = DB::table('oyo_avail_datewise')
        ->where('booking_id', $Booking_id)
        ->where('cancel',1)
        ->first();
       
    if ($existingRecord_new) {
        // print_r('sd');die;
        // Update only the booking_id
        $apihit = DB::table('oyo_avail_datewise')
            ->where('booking_id', $Booking_id)
            ->update(['booking_id' => $Booking_id]);
    } else {
        
        // Insert all data
        $apihit = DB::table('oyo_avail_datewise')->insert([
            'room_type' => $room_type,
            'date' => $check_in_d,
            'availability' => $data_count,
            'booking_id' => $Booking_id,
            'cancel' => 1,
        ]);
        // print_r($apihit);die;
        $post_fields4 = array(
            "hotelCode" => "2381521",
            "updates" => array(
                array(
                    "startDate" => $check_in_d,
                    "endDate" => $check_out_d,
                    "rooms" => array(
                        array(
                            "available" => $data_count,
                            "roomCode" => $room_code
                        )
                    )
                )
                )
            );  
          $post_fields_json4 = json_encode($post_fields4);
                $curl = curl_init();
    
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.oyoos.com/third_party/api/update_inventory?qid=2381521',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $post_fields_json4,
            CURLOPT_HTTPHEADER => array(
                'HTTPXOYOLANG: en',
                'Content-Type: application/json',
                'Cookie: SESSION=YmU5MjQ1MmUtYTZlZS00YzY2LTlkOTYtYTY2MmZlOWZiMGM4; SESSION=Nzk4NDBmZjItZDA1Yy00NmVmLWE5YzUtMDYxNmZmYWRjYWEw',
                'Authorization: Basic cGFzc2VyaW5lOnBhc3NlcmluLXRlY2hub2xvZ3k='
            ),
            ));
    
            $response = curl_exec($curl);
    
            curl_close($curl);
           
        //  echo $response;
    
    }

      }else{
        // print_r($room_type);die;
        $data =DB::table('rooms')->where('is_deleted',0)->get();
       $data_count = ($data->count() - $roomqty);

       $room_type = DB::table('room_types')->where('id', $room_code)->where('is_deleted',0)->value('title');

       $existingRecord = DB::table('oyo_avail_datewise')
       ->where('booking_id', $Booking_id)
       ->first();
   
   if ($existingRecord) {
       // Update only the booking_id
       $apihit = DB::table('oyo_avail_datewise')
           ->where('booking_id', $Booking_id)
           ->update(['booking_id' => $Booking_id]);
   } else {
       // Insert all data
       $apihit = DB::table('oyo_avail_datewise')->insert([
           'room_type' => $room_type,
           'date' => $check_in_d,
           'availability' => $data_count,
           'booking_id' => $Booking_id,
       ]);
   
       $post_fields4 = array(
           "hotelCode" => "2381521",
           "updates" => array(
               array(
                   "startDate" => $check_in_d,
                   "endDate" => $check_out_d,
                   "rooms" => array(
                       array(
                           "available" => $data_count,
                           "roomCode" => $room_code
                       )
                   )
               )
               )
           );  
         $post_fields_json4 = json_encode($post_fields4);
               $curl = curl_init();
   
           curl_setopt_array($curl, array(
           CURLOPT_URL => 'https://api.oyoos.com/third_party/api/update_inventory?qid=2381521',
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => '',
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 0,
           CURLOPT_FOLLOWLOCATION => true,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => 'POST',
           CURLOPT_POSTFIELDS => $post_fields_json4,
           CURLOPT_HTTPHEADER => array(
               'HTTPXOYOLANG: en',
               'Content-Type: application/json',
               'Cookie: SESSION=YmU5MjQ1MmUtYTZlZS00YzY2LTlkOTYtYTY2MmZlOWZiMGM4; SESSION=Nzk4NDBmZjItZDA1Yy00NmVmLWE5YzUtMDYxNmZmYWRjYWEw',
               'Authorization: Basic cGFzc2VyaW5lOnBhc3NlcmluLXRlY2hub2xvZ3k='
           ),
           ));
   
           $response = curl_exec($curl);
   
           curl_close($curl);
          
        // echo $response;
   
   }

      }
     


        // Encode booking data to JSON format
$bookingJsonData = json_encode([
    'action' => $bookingData['action'],
    'hotelCode' => $bookingData['hotelCode'],
    'channel' => 'OYO',
    'bookingId' => $bookingData['bookingId'],
    'cmBookingId' => $bookingData['cmBookingId'],
    'bookedOn' => $bookingData['bookedOn'],
    'checkin' => $bookingData['checkin'],
    'checkout' => $bookingData['checkout'],
    'segment' => $bookingData['segment'],
    'pah' => $bookingData['pah'],
    'amount' => [
        'amountAfterTax' => $bookingData['amount']['amountAfterTax'],
        'amountBeforeTax' => $bookingData['amount']['amountBeforeTax'],
        'tax' => $bookingData['amount']['tax'],
        'currency' => $bookingData['amount']['currency']
    ],
    'guest' => [
        'firstName' => $bookingData['guest']['firstName'],
        'lastName' => $bookingData['guest']['lastName'],
        'email' => $bookingData['guest']['email'],
        'phone' => $bookingData['guest']['phone']
    ],
    'rooms' => [
        [
            'roomCode' => $bookingData['rooms'][0]['roomCode'],
            'rateplanCode' => $bookingData['rooms'][0]['rateplanCode'],
            'guestName' => $bookingData['rooms'][0]['guestName'],
            'occupancy' => [
                'adults' => $bookingData['rooms'][0]['occupancy']['adults'],
                'children' => $bookingData['rooms'][0]['occupancy']['children']
            ]
        ]
    ]
]);

$curl04 = curl_init();

curl_setopt_array($curl04, array(
    CURLOPT_URL => 'https://amarinn.f9hotels.com/api/oyo_data',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => $bookingJsonData, // Use dynamically created JSON data
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Basic b3lvOnBhc3NlcmluZUAxMjM='
    ),
));

$response04 = curl_exec($curl04);
$response04 = json_decode($response04);
curl_close($curl04);


return $response;
}

// public function oyo_data(Request $request)
// {
   
//     if (!$request->header('Authorization')) {
//         return response()->json(['error' => 'Unauthorized'], 401);
//     }
//     $authorization = $request->header('Authorization');
//     $credentials = base64_decode(substr($authorization, 6));
//     list($username, $password) = explode(':', $credentials);
//     if ($username !== 'oyo' || $password !== 'passerine@123') {
//         return response()->json(['error' => 'Unauthorized'], 401);
//     }
//     $bookingData = $request->json()->all();

//     $bookingJsonData = json_encode([
//         'action' => $bookingData['action'],
//         'hotelCode' => $bookingData['hotelCode'],
//         'channel' => 'OYO',
//         'bookingId' => $bookingData['bookingId'],
//         'cmBookingId' => $bookingData['cmBookingId'],
//         'bookedOn' => $bookingData['bookedOn'],
//         'checkin' => $bookingData['checkin'],
//         'checkout' => $bookingData['checkout'],
//         'segment' => $bookingData['segment'],
//         'pah' => $bookingData['pah'],
//         'amount' => [
//             'amountAfterTax' => $bookingData['amount']['amountAfterTax'],
//             'amountBeforeTax' => $bookingData['amount']['amountBeforeTax'],
//             'tax' => $bookingData['amount']['tax'],
//             'currency' => $bookingData['amount']['currency']
//         ],
//         'guest' => [
//             'firstName' => $bookingData['guest']['firstName'],
//             'lastName' => $bookingData['guest']['lastName'],
//             'email' => $bookingData['guest']['email'],
//             'phone' => $bookingData['guest']['phone']
//         ],
//         'rooms' => [
//             [
//                 'roomCode' => $bookingData['rooms'][0]['roomCode'],
//                 'rateplanCode' => $bookingData['rooms'][0]['rateplanCode'],
//                 'guestName' => $bookingData['rooms'][0]['guestName'],
//                 'occupancy' => [
//                     'adults' => $bookingData['rooms'][0]['occupancy']['adults'],
//                     'children' => $bookingData['rooms'][0]['occupancy']['children']
//                 ]
//             ]
//         ]
//     ]);

//     if($bookingData['channel']){
//     $url = env('HOTEL_NAME');
    
    
//     $curl = curl_init();
    
//     curl_setopt_array($curl, array(
//       CURLOPT_URL =>$url,
//       CURLOPT_RETURNTRANSFER => true,
//       CURLOPT_ENCODING => '',
//       CURLOPT_MAXREDIRS => 10,
//       CURLOPT_TIMEOUT => 0,
//       CURLOPT_FOLLOWLOCATION => true,
//       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//       CURLOPT_CUSTOMREQUEST => 'POST',
//       CURLOPT_POSTFIELDS => $bookingJsonData,
//       CURLOPT_HTTPHEADER => array(
//         'Content-Type: application/json',
//         'Authorization: Basic b3lvOnBhc3NlcmluZUAxMjM='
//       ),
//     ));
    
//     $response = curl_exec($curl);
    
//     curl_close($curl);
//     echo $response;
// }
//     return response()->json(['message' => 'Data received successfully'], 200);
// }



}
