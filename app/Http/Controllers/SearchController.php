<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Auth,DB,Hash;
use App\User,App\Customer,App\Role;
use App\Room,App\RoomType;
use App\Amenities;
use App\FoodCategory,App\FoodItem;
use App\ExpenseCategory,App\Expense;
use App\Product,App\StockHistory;
use App\Reservation;
use App\Order,App\OrderItem,App\OrderHistory;
use App\Setting;
use App\PersonList;
use App\MediaFile;
use App\Permission;
use Mail;
use App\Mail\InvoiceEmail;
use Illuminate\Support\Str;
use App\UserLog;
use Excel;
use App\Http\Controllers\ExcelExport;
use Session;
use Validator;

use Carbon\Carbon;
use App\Paytm\PaytmChecksum;
use GuzzleHttp\Client;
use App\MealPlan;
use App\PackageMaster;
use App\DatePriceRange;
use App\PaymentMode;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        private $core;
     public function __construct()
    {
        $this->core=app(\App\Http\Controllers\CoreController::class);
        $this->middleware('auth');
    }
    
    
    public function index()
    {
        return view('backend.search');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
      public function newcheckin(Request $request, $mobile) {
          
          
       $this->data['data_row']=[];

        $this->data['roomtypes_list']=RoomType::select('id','title','is_base_price','base_price')->whereStatus(1)->whereIsDeleted(0)->orderBy('title','ASC')->pluck('title','id');
        $this->data['customer_list']=[];//getCustomerList();
        $this->data['getmid'] = Setting::where('name', 'mid')->select('value')->first();
        $this->data['corporates']=DB::table('corporates')->pluck('name');
        $this->data['tas']=DB::table('tas')->pluck('name');
        $this->data['ota']=DB::table('ota')->pluck('name');
        //   $this->data['ota']=DB::table('ota')->select('id','name')->get();
        $this->data['package_list']=PackageMaster::select('id','title', 'package_price', 'room_type_id')->whereStatus(1)->orderBy('id','DESC')->get();
        $this->data['mealplan_list']=MealPlan::select('id', 'name')->whereStatus(1)->orderBy('id','ASC')->pluck('name', 'id');
        $this->data['payment_mode_list']=PaymentMode::select('id', 'payment_mode')->whereStatus(1)->orderBy('id','ASC')->pluck('payment_mode', 'id');
        $this->data['arrivals_id'] = $request->id;
        $this->data['mobile'] = $mobile;
        $direct_name = DB::table('settings')->where('name','direct_name')->get();
        $this->data['direct_namee'] = $direct_name[0]->value;
        return view('backend/rooms/newcheckin',$this->data);
    }
    
    public function newexistscheckin(Request $request) { 
          
      
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://dashboard.f9hotels.com/api/getCustomerById',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id'=>$request->id),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    
      
        $c=json_decode($response);
        $this->data['data_row']=$c->customer; 
        $this->data['booking']=$c->booking;
         
        $this->data['roomtypes_list']=RoomType::select('id','title','is_base_price','base_price')->whereStatus(1)->whereIsDeleted(0)->orderBy('title','ASC')->pluck('title','id');
        $this->data['customer_list']=[];//getCustomerList();

        $this->data['getmid'] = Setting::where('name', 'mid')->select('value')->first();
        $this->data['corporates']=DB::table('corporates')->pluck('name');
        $this->data['tas']=DB::table('tas')->pluck('name');
        $this->data['ota']=DB::table('ota')->pluck('name');
        //  $this->data['ota']=DB::table('ota')->select('id','name')->get();
        $this->data['package_list']=PackageMaster::select('id','title', 'package_price', 'room_type_id')->whereStatus(1)->orderBy('id','DESC')->get();
        $this->data['mealplan_list']=MealPlan::select('id', 'name')->whereStatus(1)->orderBy('id','ASC')->pluck('name', 'id');
        $this->data['payment_mode_list']=PaymentMode::select('id', 'payment_mode')->whereStatus(1)->orderBy('id','ASC')->pluck('payment_mode', 'id');
        $this->data['arrivals_id'] = $request->id;
        $direct_name = DB::table('settings')->where('name','direct_name')->get();
        $this->data['direct_namee'] = $direct_name[0]->value;
        return view('backend/rooms/newcheckin',$this->data);
    }
     
     public function myotaalldata(Request $request){
        $id = $request->id;
        //$selctoption = PaymentMode::where('otarelation',$id)->get();
        $selctoption = PaymentMode::whereIn('otarelation', [$id,1000, 2000, 3000, 4000])->get();
        return $selctoption;
    }
    
    // public function myotaalldata2(Request $request){
    //     $id2 = $request->id;
    //     $selctoption2 = PaymentMode::whereIn('otarelation', [$id2,1000, 2000, 3000, 4000])->get();
    //     return $selctoption2;
    // }
    
    public function myotaalldata3(Request $request){
        $id3 = $request->id;
        // $selctoption3 = PaymentMode::where('status', 1)->get();
        $selctoption3 = PaymentMode::whereIn('otarelation', [1000, 2000, 3000, 4000])->get();
        return $selctoption3;
    }
    
    
    public function mobotpnewchekin(Request $request)
    {
        date_default_timezone_set("Asia/Kolkata");
        $mobile = $request->mobile;
        $otp = $request->otp;
        $fieldsnew = array(
            "variables_values" => $otp,
            "route" => "otp",
            "numbers" => $mobile,
        );
        
        $curl3 = curl_init();

curl_setopt_array($curl3, array(
  CURLOPT_URL => 'https://cerf.cerfgs.com/runway/api/auth/login',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "username": "passerine_trans1",
    "password": "Admin@123"
}',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXNzZXJpbmUiLCJpc3MiOiJ0cC1taWNyb3NlcnZpY2UiLCJhdXRob3JpdGllcyI6WyJST0xFXzU4Il0sImlhdCI6MTcwODU3ODcyMiwiZXhwIjoxNzA4NTgyMzIyfQ.G0RYCDfu2cEoeaQTZQ7lBuShwb-FtHn9WOiDZM30vJWyWV4SmwUE2qLh9BqtKQuw1k1RT4UXBWDpUPkMjlLIJg',
    'Content-Type: application/json'
  ),
));

$response3 = curl_exec($curl3);

curl_close($curl3);
// echo $response;

$responseData3 = json_decode($response3, true);

// Check if decoding was successful
if (json_last_error() === JSON_ERROR_NONE) {
    // Access the token from the response
    $token = $responseData3['data']['token'];

    // Use the $token variable as needed
    // echo "Token: " . $token;
} 


$curl2 = curl_init();

curl_setopt_array($curl2, array(
  CURLOPT_URL => "https://cerf.cerfgs.com/cpaas?unicode=false&from=PGPMS&to=" . $mobile . "&dltContentId=1707171274131005469&text=Your%20PMS%20OTP%20to%20submit%20your%20entry%20in%20the%20system%20is%20". $otp .".%20Please%20do%20not%20share%20this%20OTP%20with%20anyone%20else.%20Powered%20by%20Passerine%20Group&token=".$token."",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
));

$response2 = curl_exec($curl2);

curl_close($curl2);


$hotel_n = DB::table('settings')->where('name','hotel_name')->get();
$hotel_name = $hotel_n[0]->value;
$campaign_id = "1dcf83c3-6a7e-4269-a80f-e07fccaf5154";
$mob_number = $mobile; // Dynamic mobile number
$name = $request->name; // Dynamic name
// $hotel_name = "Hotel Name";
$code = $otp; // Dynamic code
$company = "Occumax"; // Dynamic company name

// Create the JSON payload dynamically
$postFields = json_encode(array(
    "campaign_id" => $campaign_id,
    "phone" => $mob_number,
    "params" => array(
        $name,
        $hotel_name,
        $code,
        $company
    )
));
$curl4 = curl_init();

curl_setopt_array($curl4, array(
  CURLOPT_URL => 'https://passerinegrp.cerfsolutions.com/api/wa/send-msg',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>$postFields,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
    'Authorization: Bearer 2|7nsIydoWEo8gNb76A1kpZY93bNkMgLMgKHgbJbA3'
  ),
));

$response4 = curl_exec($curl4);

curl_close($curl4);



//end otp get in whatsapp

        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://www.fast2sms.com/dev/bulkV2",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_SSL_VERIFYHOST => 0,
        //   CURLOPT_SSL_VERIFYPEER => 0,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "POST",
        //   CURLOPT_POSTFIELDS => json_encode($fieldsnew),
        //   CURLOPT_HTTPHEADER => array(
        //     "authorization: ndP9f2cztvZrow6QS0GHOI8jsmxaFplg7RyDB1VqMeXNK34LkYqxCSzR6w8hgbrk4QDlp91aiYcAjVoT",
        //     "accept: */*",
        //     "cache-control: no-cache",
        //     "content-type: application/json"
        //   ),
        // ));
        
        // $response = json_decode(curl_exec($curl));
        // return $response;
        
        $customerData = [
            "mobile" => $request->mobile,
            "otp" => $request->otp
        ];
        
        $customerId = Customer::updateOrCreate(['mobile'=>$request->mobile],$customerData);
        
        if($customerId){
            return $customerId;
        }else{
            return $response;
        }
        
        // if($response->request_id != ""){
        //     $res = new Customer;
        //     $res->mobile = $mobile;
        //     $res->otp = $otp;
        //     $res->save();
        //     return $response->request_id;
        // }else{
        //     return $response;
        // }
        $err = curl_error($curl);
        curl_close($curl);
        // return $response->status_code;
    }
    
    public function verifyotpchekin(Request $request){
        $userotp = $request->enterotp;
        $usermob = $request->mobileno;
        // return $usermob;
        $data = Customer::where('mobile',$usermob)->where('otp',$userotp)->first();
        if($userotp == $data->otp){
            Customer::where('mobile', $usermob)->where('otp',$userotp)->update(['verifystatus'=> 1]);
            return true;
        }else{
            return false;
        }
    }
    
    public function skipotpcheckin(Request $request){
        $enterreason = $request->enterreason;
        $usermob = $request->mobileno;
        // return $usermob;
        
        $customerData = [
            "mobile" => $usermob,
            "skipreason" => $enterreason
        ];
        
        $dataskip = Customer::updateOrCreate(['mobile'=>$usermob],$customerData);
        

        // $dataskip = Customer::where('mobile', $usermob)->updateOrCreate(['skipreason'=> $enterreason]);
        if($dataskip){
            return true;
        }else{
            return false;
        }
    }
    
     public function savecheckin(Request $request) {
          

        // print_r($request->room_num);die;
        $roomNumbers = $request->input('room_num');

        foreach ($roomNumbers as $roomNumber) {
           $data = DB::table('hotels_switches_roomid')->where('room_no',$roomNumber)->first();
           
           if($data){
            $room_id = $data->room_switch_id;
            $status = 1;
            $postData = [
                "room_id" => $room_id,
                "status" => $status,
            ];
            $jsonData = json_encode($postData);
          
            $curl_light = curl_init();

            curl_setopt_array($curl_light, array(
            CURLOPT_URL => 'https://dyfolite.dyfolabs.com/api/room/room-device-status/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$jsonData,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Basic ZHlmb2xhYnMyNEBnbWFpbC5jb206YWRtaW4xMjM0NQ=='
            ),
            ));

            $response_light = curl_exec($curl_light);

            curl_close($curl_light);
            // echo $response_light;die;

        }
        }

//extra code
$hotel_n = DB::table('settings')->where('name','hotel_name')->get();
        $hotel_name = $hotel_n[0]->value;
        $campaign_id = "1f1a311f-b6a1-4cb8-bf65-99831c51bc78";
        $mob_number = $request->mobile; // Dynamic mobile number
        $name = $request->name; // Dynamic name GOOGLE_REVIEW
        $company = env('GOOGLE_REVIEW'); // Dynamic company name

        // Create the JSON payload dynamically
        $postFields = json_encode(array(
            "campaign_id" => $campaign_id,
            "phone" => $mob_number,
            "params" => array(
                $name,
                $hotel_name,
                $company
            )
        ));

     
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://passerinegrp.cerfsolutions.com/api/wa/send-msg',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>$postFields,
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Bearer 2|7nsIydoWEo8gNb76A1kpZY93bNkMgLMgKHgbJbA3'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        

//end extra code



 // data send to oyo available room ---------------
 $room_type_id =$request->room_type_id;
 $today_checkin_timenew = date('Y-m-d');
 $queryResult = DB::select(DB::raw("
     SELECT room_num
     FROM reservations
     WHERE
         `check_out` IS NULL
         AND is_deleted = '0'
         AND status = '1'
         AND room_num != ''
        
 "));
 $queryResult2 = DB::select(DB::raw("
 SELECT COUNT(*) AS room_count
 FROM rooms
 WHERE status = 1
 AND is_deleted = '0'
 "));
 $room_type = DB::table('room_types')->where('id',$room_type_id)->first();
 $data = DB::table('oyo_avail_datewise')
 ->where('date', date("Y-m-d"))
 ->where('room_type', $room_type->title)
 ->latest() 
 ->first();
 if($data){
 $countdata = ($data->availability)-($request->no_of_rooms);
 $checkin_date = date("Y-m-d");
 $checkout_date = date("Y-m-d", strtotime("+1 day"));

//data send in pms db for availability

$insert = [
    'room_type' =>$room_type->title,
    'date' => $checkin_date,
    'availability' => $countdata
];

$in = DB::table('oyo_avail_datewise')->insertGetId($insert);
//end data send in pms db for availability


 $post_fields1 = array(
 "hotelCode" => env('HOTEL_CODE'),
 "updates" => array(
     array(
         "startDate" => $checkin_date,
         "endDate" => $checkout_date,
         "rooms" => array(
             array(
                 "available" => $countdata,
                 "roomCode" => $room_type_id
             )
         )
     )
     )
 );  
$post_fields_json1 = json_encode($post_fields1);
     $curl = curl_init();

 curl_setopt_array($curl, array(
 CURLOPT_URL => "https://api.oyoos.com/third_party/api/update_inventory?qid=env('HOTEL_CODE')",
 CURLOPT_RETURNTRANSFER => true,
 CURLOPT_ENCODING => '',
 CURLOPT_MAXREDIRS => 10,
 CURLOPT_TIMEOUT => 0,
 CURLOPT_FOLLOWLOCATION => true,
 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
 CURLOPT_CUSTOMREQUEST => 'POST',
 CURLOPT_POSTFIELDS => $post_fields_json1,
 CURLOPT_HTTPHEADER => array(
     'HTTPXOYOLANG: en',
     'Content-Type: application/json',
     'Cookie: SESSION=YmU5MjQ1MmUtYTZlZS00YzY2LTlkOTYtYTY2MmZlOWZiMGM4; SESSION=Nzk4NDBmZjItZDA1Yy00NmVmLWE5YzUtMDYxNmZmYWRjYWEw',
     'Authorization: Basic cGFzc2VyaW5lOnBhc3NlcmluLXRlY2hub2xvZ3k='
 ),
 ));

 $response = curl_exec($curl);

 curl_close($curl);
}
 //end data send to oyo available
        
        $countrycode = $request->country_code;
        $url = url('/');
        $cleanUrl = str_ireplace(['http://', 'https://'], '', $url);
       $hotel_name1 = DB::table('settings')->where('id',3)->get();
       $hotel_name = $hotel_name1[0]->value;
        $data = $request->all();
        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile;
        $checkin = $request->check_in_date;
        $duration_of_stays = $request->duration_of_stay;
        $price = $request->total_amount;
        $hotel_id = 11;
        $roomNum = isset($request->room_num[0]) ? $request->room_num[0] : null;
         $data = $request->all();
        
            date_default_timezone_set('Asia/Kolkata');
            if($request->room_qty){
            $room_qty= $request->room_qty;
            }
            else{
            $room_qty=1;
            }
                // $getmid = Setting::where('name', 'mid')->select('value')->first();
                $getmid = Setting::where('name', 'mid')->select('value')->first();
                if ($getmid) {
                    $mid = $getmid->value + 1;
                    Setting::where('name', 'mid')->update(['value' => $mid]);
                    // return $mid;
                }

                $mid=$getmid->value+1;

    if(isset($request->document_upload)){
        $documentPath = $request->document_upload->store('public/files');
       
    }else{
        $documentPath = $request->document_id ?? '';
    }
   

if($request->id>0){
    if($this->core->checkWebPortal()==0){
        return redirect()->back()->with(['info' => config('constants.FLASH_NOT_ALLOW_FOR_DEMO')]);
    }
    $success = config('constants.FLASH_REC_UPDATE_1');
    $error = config('constants.FLASH_REC_UPDATE_0');
}
else {
    $success = config('constants.FLASH_REC_ADD_1');
    $error = config('constants.FLASH_REC_ADD_0');
}
$starttime=date('Y-m-d H:i:s',strtotime("12:00:00"));
$starttime=date('H:i:s',strtotime($starttime));
$endtime=date('Y-m-d H:i:s',strtotime("06:00:00"));
$endtime=date('H:i:s',strtotime($endtime));
$time = date("H:i:s");

if($starttime > $time && $endtime > $time)
{
    
    $todayDate=date('Y-m-d');
    $date=date('Y-m-d',strtotime($todayDate.'-1 days'));
    $datetime = $date ." ". $time;
    $checkoutdatatime=date('Y-m-d',strtotime($date.'+ '.$request->duration_of_stay.' days'));
    $paymentdate = $date;
    
}else
{
    $date=$request->check_in_date;
    $todayDate = Carbon::now();
    $datetime = $date ." ". $time;
    $checkoutdatatime=$todayDate->addDays($request->duration_of_stay); 
    $paymentdate=date('Y-m-d');
}
Carbon::useStrictMode(false);
$to_date = Carbon::parse($checkoutdatatime);
$from_date = Carbon::parse($datetime);
$room = $request->per_room_price;
$booking = $request->booking_payment;
$reservationData = [];
$customerData = [];

        if($request->referred_by_name == "F9" || $request->referred_by_name == "Management")
        {
            $spl = str_split($request->name);
            $Booking_id = $spl[0].$spl[1].rand(0000,9999);  
        }
        else
        {
            $Booking_id = "";
        }
   
        $dateOfBirth = dateConvert($request->age, 'Y-m-d');
        $years = Carbon::parse($dateOfBirth)->age;
        $custName = $request->name;
        $customerData = [
        "Booking_id" => $request->Booking_id ?? $Booking_id,
        "and_number" => $mid,
        "name" => $request->name,
        "father_name" => $request->father_name,
        "email" => $request->email,
        "mobile" => $request->mobile,
        "address" => $request->Address,
        "nationality" => $request->nationality,
        "country" => $request->country,
        "state" => $request->state,
        "city" => $request->city,
        "gender" => $request->gender,
        "dob" => dateConvert($request->age, 'Y-m-d'),
        "age" => $years,
        "password" => Hash::make($request->mobile),
        "document" =>   $documentPath,
       ];
      $customerId = Customer::updateOrCreate(['mobile'=>$request->mobile],$customerData);
    //   $savedId = $customerId->id;
    //   $savedName = $customerId->and_number;

    //   $otherTableData = [
    //     'customer_id' => $customerId->id,
    //     'and_number' => $customerId->and_number,
    //     'name' => $customerId->name,
    //     'email' => $customerId->email,
    //     'mobile' => $customerId->mobile,
    // ];
    // DB::table('customer_and_number')->insert($otherTableData);

   
    
    // Retrieve the saved record
    // $savedRecord = DB::table('customers')->where('mobile', $request->mobile)->first();
    
  
    
        $otherTableData = [
            'customer_id' => $customerId->id,
            'and_number' => $customerId->and_number,
            'name' => $customerId->name,
            'email' => $customerId->email,
            'mobile' => $customerId->mobile,
        ];
    
       $data11 =  DB::table('customer_and_number')->insert($otherTableData);
    

    //   print_r($savedName);
    //   print_r($customerId);
    //   die;
                           
      $hit['mobile']=$request->mobile;
      $hit['name']=$request->name;
      $hit['email']=$request->email;
      $hit['address']=$request->Address;
      $hit['dob']=dateConvert($request->age, 'Y-m-d');
      

if($room == 'NaN')
{
      $perroom = 0.00;

}else
{
  $perroom =  $room;

}
$room_count = count($request->room_num);

if($request->ta)
{
    $referred_by = $request->ta;
}
else if($request->ota)
{
    $referred_by = $request->ota;
}
else if($request->corporate)
{
    $referred_by = $request->corporate;
}
else{
    $referred_by = '';
}
if($room_count == 1)
{
  
    $reservationData = [
         "customer_id" => $customerId->id,
         "booking_payment" => $booking,
         'total_amount'=>$request->total_amount,
         "room_qty" => $room_qty,
         "unique_id"=>uniqid(),
         "per_room_price" => $request->booking_payment,
         "guest_type" => $request->guest_type,
         "check_in" => $datetime,
         "infant" => $request->infant,
         "user_checkout" => $checkoutdatatime,
         "duration_of_stay" => $request->duration_of_stay,
         "Booking_Reason" => $request->Booking_Reason,
         "room_type_id" => $request->room_type_id,
         "room_num" => ($request->room_num) ? join(',',$request->room_num) : null,
         "adult" => $request->adult,
         "kids" => $request->kids,
         "booked_by" => $request->booked_by,
         "vehicle_number" => $request->vehicle_number,
         "reason_visit_stay" => $request->reason_visit_stay,
         "advance_payment" => $request->advance_payment,
         "idcard_type" => $request->idcard_type,
         "idcard_no" => $request->idcard_no,
         "idcard_image" => $documentPath,
         "payment_mode" =>$request->payment_mode,
         "meal_plan" => $request->meal_plan,
         "corporates" => $request->corporate,
         "tas" => $request->ta,
         "ota" => $request->ota,
         "referred_by_name" => $request->referred_by_name,
         "referred_by" => $referred_by,
         "remark_amount" => $request->remark_amount,
         "remark" => $request->remark,
         "package_id" => $request->package_id,
         "checkin_type" => 'single',
         "Employee_Check_In_name" => Auth::user()->id,
         'mid'=>$mid
         
     ];
     if(!$request->id){
        $reservationData["created_at_checkin"] = $datetime;
    }
    $res = Reservation::updateOrCreate(['id'=>$request->id],$reservationData);
        $paytmParams["body"] = array(
        "clientId"             => "8e5a3jsp0hh7",
        "clientSecret"        => "xsW3fI3q45",
    );
        Setting::where('name', 'mid')->update(['value'=>$mid]);
        $currentTime = Carbon::now(new \DateTimeZone('Asia/Kolkata'));
        $dateTimeString = $currentTime->toDateTimeString();
        $history['created_at']=$dateTimeString;

        $history['payment']=$request->advance_payment ?? 0;
        $history['mode']=$request->payment_mode;
        $history['payment_date']=$paymentdate;
        $history['reservations_id']=$res->id;
        $history['remark']='Advance';
        DB::table("payment_history")->insert($history);
        //print_r($request->payment);die;
        
         if($request->payment != null){
            foreach($request->payment as $key => $value){
                 if($value > 0){
                    $r_id = $res->id;
                    $value1 = $value;
                    $today = $paymentdate;
                    $mode = $request->mode[$key];
                    $remark = $request->payment_remark[$key];
                    DB::table("payment_history")->insert(['reservations_id' => $r_id, 'payment' => $value1, 'payment_date' => $today, 'mode' => $mode,  'remark' => $remark]);
                 }
            }
         } 
        // 
    
        $hit['checkin']=$datetime;
        $hit['checkout']=$checkoutdatatime;
        $hit['payment']=$booking;
        $hit['adult']=$request->adult;
        $hit['source']=$referred_by;
        $hit['payment_mode']=$request->payment_mode;
        $se=Setting::where('name','hotel_name')->first();
        $hit['hotel']=$se->value;
        $hit['room_number']=($request->room_num) ? join(',',$request->room_num) : null;
        $hit['nights']=$request->duration_of_stay;
        $hit['invoice_number']=$reservationData['unique_id'];
       // $f=$this->foreca($reservationData['room_num'],$custName,$datetime,$checkoutdatatime,$res->id,$request->adult,$request->kids);
        $re=centralDataInsert($hit);
        // $hit['booking_id'] = $request->Booking_id;
        $re1=centralDataInsertf9($hit);
      // dd($re);
}
elseif($room_count > 1)
{
    $unique_id=uniqid();
  foreach($request->room_num as $rm_num)
  {
    // echo $customerId;die();
    $reservationData = [
        "customer_id" => $customerId->id,
        'unique_id'=>$unique_id,
         "booking_payment" => $booking,
         'total_amount'=>$request->booking_payment,
         "per_room_price" => $request->booking_payment,
         "room_qty" => $room_qty/$room_count,//$request->room_qty,
         "guest_type" => $request->guest_type,
         "check_in" => $datetime,
         "Booking_Reason" => $request->Booking_Reason,
         "user_checkout" => $checkoutdatatime,
         "duration_of_stay" => $request->duration_of_stay,
         "room_type_id" => $request->room_type_id,
         "room_num" => $rm_num,
         "adult" => $request->adult,
         "infant" => $request->infant,
         "kids" => $request->kids,
         "booked_by" => $request->booked_by,
         "vehicle_number" => $request->vehicle_number,
         "reason_visit_stay" => $request->reason_visit_stay,
         "advance_payment" => $request->advance_payment,
         "sec_advance_payment" => $request->sec_advance_payment,
         "sec_payment_mode" => $request->sec_payment_mode,
         "idcard_type" => $request->idcard_type,
         "idcard_no" => $request->idcard_no,
         "idcard_image" => $documentPath,
         "payment_mode" =>$request->payment_mode,
         "meal_plan" => $request->meal_plan,
         "corporates" => $request->corporate,
         "tas" => $request->ta,
         "ota" => $request->ota,
         "referred_by_name" => $request->referred_by_name,
         "referred_by" => $referred_by,
         "remark_amount" => $request->remark_amount,
         "remark" => $request->remark,
         "package_id" => $request->package_id,
         "checkin_type" => 'multiple',
         "Employee_Check_In_name" => Auth::user()->id,
         'mid'=>$mid
    ];
    if(!$request->id){
        $reservationData["created_at_checkin"] = $datetime;
    }
    $res = Reservation::updateOrCreate(['id'=>$request->id],$reservationData);
            $paytmParams["body"] = array(
        "clientId"             => "8e5a3jsp0hh7",
        "clientSecret"        => "xsW3fI3q45",
    );
    Setting::where('name', 'mid')->update(['value'=>$mid]);
    $currentTime = Carbon::now(new \DateTimeZone('Asia/Kolkata'));
    $dateTimeString = $currentTime->toDateTimeString();
    $history['created_at']=$dateTimeString;

    $history['payment']=$request->advance_payment/$room_count ?? 0;
    $history['mode']=$request->payment_mode;
    $history['payment_date']=$paymentdate;
    $history['reservations_id']=$res->id;
    $history['remark']='Advance';
    
    DB::table("payment_history")->insert($history);
    
    if($request->payment != null){
        foreach($request->payment as $key => $value){
             if($value > 0){
                $r_id = $res->id;
                $value1 = $value/$room_count;
                $today = $paymentdate;
                $mode = $request->mode[$key];
                $remark = $request->payment_remark[$key];
                DB::table("payment_history")->insert(['reservations_id' => $r_id, 'payment' => $value1, 'payment_date' => $today, 'mode' => $mode,  'remark' => $remark]);
            }
        }
    } 
         
    $hit['checkin']=$datetime;
    $hit['checkout']=$checkoutdatatime;
    $hit['payment']=$booking;
    $hit['adult']=$request->adult;
    $hit['source']=$referred_by;
    $hit['payment_mode']=$request->payment_mode;
    $se=Setting::where('name','hotel_name')->first();
    $hit['hotel']=$se->value;
    $hit['room_number']=($request->room_num) ? join(',',$request->room_num) : null;
    $hit['nights']=$request->duration_of_stay;
    $hit['invoice_number']=$reservationData['unique_id'];
    $re=centralDataInsert($hit);
    $re1=centralDataInsertf9($hit);

  }
}

if($room_count !=1)
{
    $idd = $res->id -1;
}
else
{
    $idd = $res->id;
}
if($idd){

    $img = $request->id_cardno;
    for($c=0; $c < count($img); $c++){

    $img = $request->id_cardno;

$folderPath = "public/uploads/id_cards/";

$image_parts = explode(";base64,", $img[$c]);
$image_type_aux = explode("image/", $image_parts[0] ?? '');
$image_type = $image_type_aux[1] ?? '';

$image_base64 = base64_decode($image_parts[1] ?? '');
$fileName = uniqid() . '.png';

$file = $folderPath . $fileName;
file_put_contents($file, $image_base64);
$idImages = [];

$idImages[] = ['tbl_id'=>$res->id, 'file'=>$fileName];
        //}
        if(count($idImages)>0){
            MediaFile::insert($idImages);
        }
    }




$img = $request->id_cardno;
$folderPath = "public/uploads/id_cards/";
$image_parts = explode(";base64,", $img[0] ?? '');
$image_type_aux = explode("image/", $image_parts[0] ?? '');
$image_type = $image_type_aux[1] ?? '';
$image_base64 = base64_decode($image_parts[1] ?? '');
$fileNameFront = uniqid() . '.png';
$file = $folderPath . $fileNameFront;
file_put_contents($file, $image_base64);



$folderPath = "public/uploads/id_cards/";
$image_parts = explode(";base64,", $img[1] ?? '');
$image_type_aux = explode("image/", $image_parts[0] ?? '');
$image_type = $image_type_aux[1] ?? '';
$image_base64 = base64_decode($image_parts[1] ?? '');
$fileNameBack = uniqid() . '.png';
$back = $folderPath . $fileNameBack;
file_put_contents($back, $image_base64);

$mediaData=array(
'tbl_id'=>$res->id,
'file'=>$fileNameFront,
'cnic_back'=>$fileNameBack,

);

DB::table('media_files')->InsertGetId($mediaData);
    if(isset($request->persons_info['name'])){

         $personReqData = $request->persons_info;

        $personsData = [];
        
        foreach($personReqData['name'] as $k=>$val){
           
            if (!empty($personReqData['age'][$k]) && !empty($personReqData['gender'][$k]) )
            {
                if(isset($request->document_upload) && !empty($personReqData['document_upload1'][$k]) ){
                    $documentPath = $personReqData['document_upload1'][$k]->store('public/files');
                }else{
                    $documentPath = '';
                }

                if($val!=''){
                $temp_id = session()->get('temp_id');
                $image= DB::table('guest_cnic_images')->where('is_fetch',$temp_id)->orderBy('id', 'DESC')->first();

                if(!isset($image->front) == 1)
                {
                    $front = "";
                }
                else{
                    $front = $image->front;
                }

                if(!isset($image->back) == 1)
                {
                    $back = "";
                }
                else
                {
                    $back = $image->back;
                }




                $person_dateOfBirth = dateConvert($personReqData['age'][$k], 'Y-m-d');
                    $person_years = Carbon::parse($person_dateOfBirth)->age;
                $personsData[] = [
                        'reservation_id'=>$idd,
                        'name'=>$val,
                        'gender'=>$personReqData['gender'][$k],
                        'age'=>$person_years,
                        'idcard_type'=>$personReqData['idcard_type'][$k],
                        'document'=>$documentPath,
                        'idcard_no'=>$personReqData['idcard_no'][$k],
                        'cnic_front'=>$front,
                        'cnic_back'=>$back,
                        'dob' => dateConvert($personReqData['age'][$k], 'Y-m-d'),
                    ];

                //  $delete= DB::table('guest_cnic_images')->where('id',$image->id)->delete();
                }
            }
        }

        if(count($personsData)>0){
            PersonList::insert($personsData);
        }

    }

    if(!$request->id && $request->mobile){
        $this->core->sendSms(1,$request->mobile,["name" => $custName]);
    }
                $starttime=date('Y-m-d H:i:s',strtotime("12:00:00"));
                $starttime=date('H:i:s',strtotime($starttime));
                $endtime=date('Y-m-d H:i:s',strtotime("06:00:00"));
                $endtime=date('H:i:s',strtotime($endtime));
                $time = date("H:i:s");
                $today_checkin_timenew = date('Y-m-d');
                if($starttime > $time && $endtime > $time)
                {
                    $today_checkin_timenew = date('Y-m-d',strtotime($today_checkin_timenew.'-1 days'));
                }else{
                    $today_checkin_timenew = $today_checkin_timenew;
                }
                
                
                $controoms = DB::select("SELECT COUNT(*) as noOfContinue FROM reservations WHERE check_out IS NULL and is_deleted='0' and status='1' and room_num != '' and date(check_in) != '$today_checkin_timenew'");
                $noofrooms = $controoms[0]->noOfContinue;
                $controoms_new = DB::select("SELECT COUNT(*) as noOfContinuetodaycheckout FROM reservations WHERE date(check_out) = '$today_checkin_timenew' and is_deleted='0' and status='1' and room_num != '' and date(check_in) != '$today_checkin_timenew'");
                $noofrooms_new = $controoms_new[0]->noOfContinuetodaycheckout;
                $today_room_count_query =  DB::select("SELECT * FROM reservations WHERE DATE(`created_at_checkin`) = '$today_checkin_timenew'");
                $today_room_count_add = count($today_room_count_query);
                $noofrooms = $noofrooms + $noofrooms_new + $today_room_count_add;
                DB::delete("DELETE FROM continue_rooms WHERE DATE(created_at) = DATE('$today_checkin_timenew') ");
                $today_checkin_timenew = date("$today_checkin_timenew H:i:s");
                DB::insert("INSERT INTO continue_rooms(no_of_rooms,created_at)VALUES($noofrooms,'$today_checkin_timenew')"); 
        
                // $controoms = DB::select("SELECT COUNT(*) as noOfContinue FROM reservations WHERE check_out IS NULL and is_deleted='0' and status='1' and room_num != '' and date(check_in) != '$today_checkin_timenew'");
                // $noofrooms = $controoms[0]->noOfContinue;
                // $today_room_count_query =  DB::select("SELECT * FROM reservations WHERE DATE(`created_at_checkin`) = '$today_checkin_timenew'");
                // $today_room_count_add = count($today_room_count_query);
                // $noofrooms = $noofrooms + $today_room_count_add;
                // DB::delete("DELETE FROM continue_rooms WHERE DATE(created_at) = DATE('$today_checkin_timenew') ");
                // DB::insert("INSERT INTO continue_rooms(no_of_rooms)VALUES($noofrooms)");   
    
    //   $now = date('Y-m-d');
    //   $controoms = DB::select("SELECT COUNT(*) as noOfContinue FROM reservations WHERE check_out IS NULL and is_deleted='0' and status='1' and room_num != ''");
    //   $noofrooms = $controoms[0]->noOfContinue;
    //   DB::delete("DELETE FROM continue_rooms WHERE DATE(created_at) = DATE('$now') ");
    //   DB::insert("INSERT INTO continue_rooms(no_of_rooms)VALUES($noofrooms)");
      
           
        // $count = count($queryResult);
        // dd($count);




    return redirect('admin/search')->with(['success' => $success]);
   //return redirect()->back()->with(['success' => $success]);
}
return redirect()->back()->with(['error' => $error]);
}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
