<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Razorpay\Api\Api;

use Redirect,Response;
use App\Customerfoodorder;
use DB;
use Illuminate\Support\Facades\Session;
class RazorpayController extends Controller
{
    public function razorpay()
    {        
        return view('index');
    }
public function thankYou(Request $request){

    $cart = Session::get('cart');

    if ($cart) {
        foreach ($cart as $key => $item) {
            // Check your condition here
            // For example, let's say you want to remove items with price less than 200
          
                // Remove the item from the session array
                Session::forget('cart.'.$key);
            
        }
    }
    return view('thankyou');
}


public function payment(Request $request)
    {        
        // print_r('hello');die;
        date_default_timezone_set("Asia/Kolkata");
        $input = $request->all();
        //return $input;
        $res_id = rand(0000000,9999999);
        
        // $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        // $payment = $api->payment->fetch($input['razorpay_payment_id']);
        
        if(count($input)) 
        {
            try 
            {
               // print_r('hello');die;
                  $mobnum = $input['mobileno'];
                // $enterotp = '1111';
                $customer_name = $input['name'];
                // print_r($customer_name);die;
                // $dataa = DB::table('reservations')->where('room_num',$input['roomnumber'])->latest();
                $dataa = DB::table('reservations')
                    ->where('room_num', $input['roomnumber'])
                    ->latest()
                    ->first();
                // print_r($dataa->id);die;
                $arrayall = [
                      'mobile' => $input['mobileno'],
                    'name' => $input['allfoodname'],
                    'quantity' => $input['foodquantity'],
                    'unitprice' => $input['unitprice'],
                    'roomnumber' => $input['roomnumber'],
                    'order_id' => $res_id,
                    'discount' => $input['discount'],
                    'amount' => $input['amount'],
                    // 'payment_id' => $input['razorpay_payment_id'],
                    // 'payment_done' => 1,
                    'customer_id'=>$dataa->customer_id,
                    'reservation_id' =>$dataa->id,
                   
                     'customer_name' =>$customer_name,
                    'order_date' => date("Y-m-d")
                ];

               
                // $dataupdate = Customerfoodorder::where(['mobile'=>$mobnum])->update($arrayall);
                $dataupdate =   Customerfoodorder::create($arrayall);
                // if($dataupdate == 1){
                //     session()->forget('cart');
                //     // return $roomforextra;
                // }
                // $cart = 0;
                // session()->put('cart', $cart);
               
                $fields = array(
                    "message" => "Thank you for your Order. Your order id is '".$res_id."' and you ordered '".$input['allfoodname']."' ",
                    "language" => "english",
                    "route" => "q",
                    "numbers" => $mobnum,
                );
        
                $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://www.fast2sms.com/dev/bulkV2",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_SSL_VERIFYHOST => 0,
                  CURLOPT_SSL_VERIFYPEER => 0,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => json_encode($fields),
                  CURLOPT_HTTPHEADER => array(
                    "authorization: ndP9f2cztvZrow6QS0GHOI8jsmxaFplg7RyDB1VqMeXNK34LkYqxCSzR6w8hgbrk4QDlp91aiYcAjVoT",
                    "accept: */*",
                    "cache-control: no-cache",
                    "content-type: application/json"
                  ),
                ));
        
                $response = json_decode(curl_exec($curl));
                $err = curl_error($curl);
                curl_close($curl);
                
                // return view('backend.thankyou');
            } 
            catch (\Exception $e) 
            {
                return  $e->getMessage();
                \Session::put('error',$e->getMessage());
                return redirect()->back();
            } 
            return redirect()->back();           
        }
        
        
        
    }
    





    // public function payment(Request $request)
    // {        
        
    //     date_default_timezone_set("Asia/Kolkata");
    //     $input = $request->all();
    //     //return $input;
    //     $res_id = rand(0000000,9999999);
        
    //     $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
    //     $payment = $api->payment->fetch($input['razorpay_payment_id']);
        
    //     if(count($input)  && !empty($input['razorpay_payment_id'])) 
    //     {
    //         try 
    //         {
    //             $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount'=>$payment['amount']));
                
    //             $mobnum = $input['mobileno'];
    //             $enterotp = $input['enterotp'];
             
    //             $arrayall = [
    //                 'name' => $input['allfoodname'],
    //                 'quantity' => $input['foodquantity'],
    //                 'unitprice' => $input['unitprice'],
    //                 'roomnumber' => $input['roomnumber'],
    //                 'order_id' => $res_id,
    //                 'discount' => $input['discount'],
    //                 'amount' => $input['amount'],
    //                 'payment_id' => $input['razorpay_payment_id'],
    //                 'payment_done' => 1,
    //                 'order_date' => date("Y-m-d")
    //             ];
                
    //             $dataupdate = Customerfoodorder::where(['mobile'=>$mobnum,'otp'=>$enterotp])->update($arrayall);
    //             if($dataupdate == 1){
    //                 session()->forget('cart');
    //                 // return $roomforextra;
    //             }
                
    //             $fields = array(
    //                 "message" => "Thank you for your Order. Your order id is '".$res_id."' and you ordered '".$input['allfoodname']."' ",
    //                 "language" => "english",
    //                 "route" => "q",
    //                 "numbers" => $mobnum,
    //             );
        
    //             $curl = curl_init();

    //             curl_setopt_array($curl, array(
    //               CURLOPT_URL => "https://www.fast2sms.com/dev/bulkV2",
    //               CURLOPT_RETURNTRANSFER => true,
    //               CURLOPT_ENCODING => "",
    //               CURLOPT_MAXREDIRS => 10,
    //               CURLOPT_TIMEOUT => 30,
    //               CURLOPT_SSL_VERIFYHOST => 0,
    //               CURLOPT_SSL_VERIFYPEER => 0,
    //               CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //               CURLOPT_CUSTOMREQUEST => "POST",
    //               CURLOPT_POSTFIELDS => json_encode($fields),
    //               CURLOPT_HTTPHEADER => array(
    //                 "authorization: ndP9f2cztvZrow6QS0GHOI8jsmxaFplg7RyDB1VqMeXNK34LkYqxCSzR6w8hgbrk4QDlp91aiYcAjVoT",
    //                 "accept: */*",
    //                 "cache-control: no-cache",
    //                 "content-type: application/json"
    //               ),
    //             ));
        
    //             $response = json_decode(curl_exec($curl));
    //             $err = curl_error($curl);
    //             curl_close($curl);
                
    //         } 
    //         catch (\Exception $e) 
    //         {
    //             return  $e->getMessage();
    //             \Session::put('error',$e->getMessage());
    //             return redirect()->back();
    //         }            
    //     }
        
    //     return redirect()->back();
        
    // }
    
    
    
}
