<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function roomlight(Request $request)
    {
        // print_r('sdsd');die;
        $hotels = DB::table('show_hotel')->select('*')->get();
        $room_no = DB::table('rooms')->select('*')->get();
        // print_r($room_no);die;
        // return view('room_light', compact('hotels', 'room_no'));
        return view('roomlight_new', compact('hotels', 'room_no'));
    }

    // public function roomlight_action(Request $request)
    // {
    //     $insert['room_no'] = $request->room_no;
    //     $insert['light_on'] = $request->oncode;
    //     $insert['light_off'] = $request->offcode;

    //     $insert['ac_on'] = $request->event_onac;
    //     $insert['ac_off'] = $request->event_offac;
    //     $insert['bathroom_on'] = $request->event_on_bathroom;
    //     $insert['bathroom_off'] = $request->event_off_bathroom;
    //     $insert['tv_on'] = $request->event_on_tv;
    //     $insert['tv_off'] = $request->event_off_tv;
    //     $insert['table_lamp_on'] = $request->event_on_lamp;
    //     $insert['table_lamp_off'] = $request->event_off_lamp;
       
    //     $res = DB::table('hotels_room_light')->insert($insert);
    //     if ($res) {
    //         return response()->json(['status' => 'success', 'message' => 'Coupon Code Created']);
    //     } else {
    //         return response()->json(['status' => 'error', 'message' => 'Error !']);
    //     }
    // }
    public function roomlight_action(Request $request)
     {
        // print_r($request->ac_switch);die;
        $roomNo = $request->room_no;
        $roomSwitchId = $request->room_no_switch;
        $acId = $request->ac_switch;
        
        // Define the search criteria
        $searchCriteria = [
            'room_no' => $roomNo
        ];
        
        // Define the values to be inserted or updated
        $values = [
            'room_switch_id' => $roomSwitchId,
            'ac_id' => $acId
        ];
        
        // Perform the update or insert operation
        $res = DB::table('hotels_switches_roomid')->updateOrInsert($searchCriteria, $values);
        
        if ($res) {
            return response()->json(['status' => 'success', 'message' => 'Coupon Code Created']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Error !']);
        }
     }

public function roomlight_data(Request $request)
{
    $room_no =$request->selectedRoom;
    $data = DB::table('hotels_switches_roomid')->where('room_no',$room_no)->first();
    // print_r($data);die;
    // return response()->json($data);
    return response()->json([
        'light_fan' => $data->room_switch_id,
        'ac_switch' => $data->ac_id,
    ]);
}

    public function get_detail()
    {
        $res = DB::table('hotels_switches_roomid')->get();
        $i = 1;
        foreach ($res as $v) {
            echo "<tr>
            <td>" . $i++ . "</td>
           
            <td>" . $v->room_no . "</td>
            
            <td>" . $v->room_switch_id . "</td>

            <td>" . $v->ac_id . "</td>
           
            <td><button class='btn btn-danger' onClick='deleteCoupon(" . $v->id . ")'>Delete</td>
            </tr>";
        }
    }


    public function deleteCoupon(Request $request)
    {
        // print_r('hello');
        $id = $request->id;
        if (DB::table('hotels_switches_roomid')->where('id', $id)->delete()) {
            return response()->json(['status' => 'success', 'message' => 'Coupon Code Deleted']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Error !']);
        }
    }


    public function lighton(Request $request)
    {

        $roon_on = $request->roon_on;

        $detail =  DB::table('hotels_room_light')->select('light_on')->where('room_no', $request->roon_on)->first();

        if ($detail) {
            // https://maker.ifttt.com/trigger/NG121_608_MS_ON/with/key/cCCBs5uLNQJDGkDH-65yXr
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://maker.ifttt.com/trigger/' . $detail->light_on . '/with/key/djA7Jb9Ro-feDYX8rC7esx',
                //   CURLOPT_URL => 'https://maker.ifttt.com/trigger/NG121_608_MS_ON/with/key/cCCBs5uLNQJDGkDH-65yXr',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
            ));

            $response = curl_exec($curl);



            curl_close($curl);
        }
    }


    public function lightoff(Request $request)
    {

        $roon_on = $request->roon_on;



        $detail =  DB::table('hotels_room_light')->select('light_off')->where('room_no', $request->roon_on)->first();

        if ($detail) {
            // https://maker.ifttt.com/trigger/NG121_608_MS_ON/with/key/cCCBs5uLNQJDGkDH-65yXr
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://maker.ifttt.com/trigger/' . $detail->light_off . '/with/key/djA7Jb9Ro-feDYX8rC7esx',
                //   CURLOPT_URL => 'https://maker.ifttt.com/trigger/NG121_608_MS_ON/with/key/cCCBs5uLNQJDGkDH-65yXr',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
            ));

            $response = curl_exec($curl);



            curl_close($curl);
        }
    }

    public function gold_user(Request $request)
    {
        // print_r('hello');die;
        $gold_data = DB::table('gold')->select('*')->first();
    
        $curl = curl_init();
    
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('GOLDBCK_URL') .'api/v1/hotel-passbook/' . env('hotel_id') . '',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
    
        $response = curl_exec($curl);
        $responseData = json_decode($response, true); // Decode the JSON response
    
        // Extract the goldGrms value
        $goldamountprice = isset($responseData['goldQuote']['rates']['gBuy']) ? $responseData['goldQuote']['rates']['gBuy'] : null;

        $goldGrms = isset($responseData['result']['data']['goldGrms']) ? $responseData['result']['data']['goldGrms'] : null;
        $transactions = isset($responseData['transfers']) ? $responseData['transfers'] : [];
        foreach ($transactions as &$transaction) {
            $transaction['executionDateTime'] = Carbon::parse($transaction['executionDateTime'])->format('Y-m-d H:i:s');
        }
        curl_close($curl);
    
        return view('gold.user_transaction', compact('gold_data', 'transactions'));
    }

    public function gold(Request $request)
    {
        // print_r('hell');die;



        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://amarinn.f9hotels.com/api/oyo_data',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
          "action": "book",
          "hotelCode": "2381521",
          "channel": "OYO",
          "bookingId": "402047990",
          "cmBookingId": "40210047990",
          "bookedOn": "2024-06-12 10:25:35",
          "checkin": "2024-06-25",
          "checkout": "2024-06-26",
          "segment": "OTA",
          "pah": false,
          "amount": {
            "amountAfterTax": 1204.0,
            "amountBeforeTax": 1075.0,
            "tax": 129.0,
            "currency": "INR"
          },
          "guest": {
            "firstName": "dddsunil",
            "lastName": "Kumar",
            "email": "sunil.maurya@corewebconnections.com",
            "phone": "8173987453"
          },
          "rooms": [
            {
              "roomCode": "1",
              "rateplanCode": "CLASSIC-SINGLE-EP",
              "guestName": "ddd Maurya",
              "occupancy": {
                "adults": 1,
                "children": 0
              }
            }
          ]
        }
        ',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Authorization: Basic b3lvOnBhc3NlcmluZUAxMjM='
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;die;
        





        $gold_data = DB::table('gold')->select('*')->first();
    
        $curl = curl_init();
    

        curl_setopt_array($curl, array(
            CURLOPT_URL => env('GOLDBCK_URL') .'api/v1/hotel-passbook/' . env('hotel_id') . '',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
    
        $response = curl_exec($curl);
        $responseData = json_decode($response, true); // Decode the JSON response
    
        // Extract the goldGrms value
        $goldamountprice = isset($responseData['goldQuote']['rates']['gBuy']) ? $responseData['goldQuote']['rates']['gBuy'] : null;

        $goldamountsell = isset($responseData['goldQuote']['rates']['gSell']) ? $responseData['goldQuote']['rates']['gSell'] : null;

        $goldGrms = isset($responseData['result']['data']['goldGrms']) ? $responseData['result']['data']['goldGrms'] : null;
        $transactions = isset($responseData['transactions']) ? $responseData['transactions'] : [];
        foreach ($transactions as &$transaction) {
            $transaction['created_at_datetime'] = Carbon::parse($transaction['created_at_datetime'])->format('Y-m-d H:i:s');
        }
        curl_close($curl);
// print_r($goldamountprice);die;
        return view('gold.view', compact('gold_data', 'goldGrms','transactions','goldamountprice','goldamountsell'));
    }
   Public function gold_action(Request $request)
   {
    // print_r('hell');die;
    $qty = $request->input('qty');
    $amount = $request->input('qty');
   
    // Assuming $amount is obtained from the input
    
    // Calculate amount after removing 3% GST
    $amountAfterGST = $amount - ($amount * 0.03);
    
    // Calculate final amount after removing 10% for service charge
    $finalAmount = $amountAfterGST - ($amountAfterGST * 0.10);
    
    
    // print_r(env('GOLDBCK_URL') . 'api/v1/hotel/' . env('hotel_id') . '');die;
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => env('GOLDBCK_URL') . 'api/v1/hotel/' . env('hotel_id') . '',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('gold_amount' => $finalAmount),
        ));

        $response = curl_exec($curl);

        $responseData = json_decode($response);

        if ($responseData && $responseData->status) {
            return redirect()->back()->with('success', $responseData->message);
        } else {
            // Handle the case where the request was not successful
            return redirect()->back()->with('error', 'Failed to add gold.');
        }
    
   }
//eglobe availability
public function room_avail_management_eglobe()
{
    return view('availability.index_eglobe');
}
public function get_room_avail_eglobe()
{
    $roomTypes = DB::table('room_types')->where('is_deleted',0)->get();

   
    $mealPlans = DB::table('meal_plans')->get();
    $oyodata = DB::table('eglobe_avail_datewise')->get();
    $data = [
        
        'roomTypes' => $roomTypes,
        'oyodata' => $oyodata,
    ];


    return response()->json($data);
}
public function update_room_avail_eglobe(Request $request)
{
    $filledData = $request->filledData;
// print_r($filledData);die;
    $groupedUpdates = [];
    // Process the filled data
    foreach ($filledData as $row) {
        $roomId = $row['roomId']; // Get the room ID
        $rowData = $row['data']; // Get the filled data for this room
        
        $room = $row['room']; 
        $room_type = $row['room_type']; 
       
        $roomCode = ($room == 'DELUXE') ? "2" : "1";
          $rateplanCode  =  $room;
       
        foreach ($rowData as $date => $value) {
            $date = trim($date);
            $rate = trim($value);
           

            $insertData = [
                'room_type' => $room_type,
                'date' => $date,
                'availability' => $rate
            ];
            
            $insertedId = DB::table('eglobe_avail_datewise')->insertGetId($insertData);
           
            $checkin_date = $date; // Current date
            $checkout_date = date("Y-m-d", strtotime($checkin_date . " +1 day")); // Next day after check-in date
            $formatted_date = date("d-M-Y", strtotime($checkin_date));
            // echo $formatted_date;
            // print_r($formatted_date);die;
           
            // ' . env('hotel_id') . '',

            $curl12 = curl_init();
            
            curl_setopt_array($curl12, array(
              CURLOPT_URL => 'https://www.eglobe-solutions.com/webapichannelmanager/rooms/' . env('EGLOBE_API_KEY'),
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'Authorization: Basic Og=='
              ),
            ));
            
            $response12 = curl_exec($curl12);
            
            curl_close($curl12);
           
            $responseData = json_decode($response12, true);
            // print_r($responseData);die;
            if ($responseData === null && json_last_error() !== JSON_ERROR_NONE) {
                die('Error decoding JSON response: ' . json_last_error_msg());
            }
            
           
                            $roomCode = null;
                foreach ($responseData as $room) {
                    if (strtolower($room['RoomName']) === strtolower($room_type)) {
                        $roomCode = $room['RoomCode'];
                        break; 
                    }
                }


if ($roomCode) {
    // echo "The room code for is $roomCode.";die;
     $curl = curl_init();
                
                curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://www.eglobe-solutions.com/webapichannelmanager/inventory/' . env('EGLOBE_API_KEY') . '/v2',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                 CURLOPT_POSTFIELDS => json_encode(array(
                    "RoomWiseInventory" => array(
                    array(
                        "DateFrom" => $formatted_date,
                        "DateTill" => $formatted_date, // Dynamic DateTill value
                        "RoomCode" => $roomCode,
                        "Availability" => $rate
                    )
                    )
  )),
                  CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                  ),
                ));
                
                $response = curl_exec($curl);
                
                curl_close($curl);
                echo $response;
}         

        }
        }

    // Return a response if needed
    return response()->json(['message' => $response]);
}
// end eglobe availability


   //oyo room availability
   public function room_avail_management()
   {
       return view('availability.index');
   }
   public function get_room_avail()
   {
       $roomTypes = DB::table('room_types')->where('is_deleted',0)->get();
   
      
       $mealPlans = DB::table('meal_plans')->get();
       $oyodata = DB::table('oyo_avail_datewise')->get();
       $data = [
           
           'roomTypes' => $roomTypes,
           'oyodata' => $oyodata,
       ];

   
       return response()->json($data);
   }
   public function update_room_avail(Request $request)
{
    $filledData = $request->filledData;

    $groupedUpdates = [];
    // Process the filled data
    foreach ($filledData as $row) {
        $roomId = $row['roomId']; // Get the room ID
        $rowData = $row['data']; // Get the filled data for this room
        
        $room = $row['room']; 
        $room_type = $row['room_type']; 
       
        $roomCode = ($room == 'DELUXE') ? "2" : "1";
          $rateplanCode  =  $room;
       
        foreach ($rowData as $date => $value) {
            $date = trim($date);
            $rate = trim($value);
           

            $insertData = [
                'room_type' => $room_type,
                'date' => $date,
                'availability' => $rate
            ];
            
            $insertedId = DB::table('oyo_avail_datewise')->insertGetId($insertData);
           
            $checkin_date = $date; // Current date
            $checkout_date = date("Y-m-d", strtotime($checkin_date . " +1 day")); // Next day after check-in date
            
           

 $post_fields = array(
 "hotelCode" => "2381521",
 "updates" => array(
     array(
         "startDate" => $checkin_date,
         "endDate" => $checkout_date,
         "rooms" => array(
             array(
                 "available" => $rate,
                 "roomCode" => $roomCode
             )
         )
     )
     )
 );  
$post_fields_json = json_encode($post_fields);
// print_r($post_fields_json);die;
     $curl = curl_init();

 curl_setopt_array($curl, array(
 CURLOPT_URL => 'https://api.oyoos.com/third_party/api/update_inventory?qid=2381521',
 CURLOPT_RETURNTRANSFER => true,
 CURLOPT_ENCODING => '',
 CURLOPT_MAXREDIRS => 10,
 CURLOPT_TIMEOUT => 0,
 CURLOPT_FOLLOWLOCATION => true,
 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
 CURLOPT_CUSTOMREQUEST => 'POST',
 CURLOPT_POSTFIELDS => $post_fields_json,
 CURLOPT_HTTPHEADER => array(
     'HTTPXOYOLANG: en',
     'Content-Type: application/json',
     'Cookie: SESSION=YmU5MjQ1MmUtYTZlZS00YzY2LTlkOTYtYTY2MmZlOWZiMGM4; SESSION=Nzk4NDBmZjItZDA1Yy00NmVmLWE5YzUtMDYxNmZmYWRjYWEw',
     'Authorization: Basic cGFzc2VyaW5lOnBhc3NlcmluLXRlY2hub2xvZ3k='
 ),
 ));

 $response = curl_exec($curl);

 curl_close($curl);

        }
        }

    // Return a response if needed
    return response()->json(['message' => $response]);
}

//eglobe

public function revenue_management_eglobe()
{
    return view('revenue.index_eglobe');
}
public function get_room_type_eglobe()
{
    $roomTypes = DB::table('room_types')->where('is_deleted',0)->get();

    // Fetch meal plans
    $mealPlans = DB::table('meal_plans')->get();
    $oyodata = DB::table('eglobe_price_datewise')->get();
    $data = [
        
        'roomTypes' => $roomTypes,
        'mealPlans' => $mealPlans,
        'oyodata' => $oyodata,
    ];


    return response()->json($data);
}

public function update_room_data_eglobe(Request $request)
{
    $filledData = $request->filledData;
    $groupedUpdates = [];
    foreach ($filledData as $row) {
        $roomId = $row['roomId']; 
        $rowData = $row['data']; 
        $occupancy = $row['occupancy']; 
        $mealPlan = $row['mealPlan']; 
        $room = $row['room']; 
        $room_type = $row['room_type']; 
        $room_type = $row['room_type']; 
        $words = explode(' ', $room_type);
        $first_two_words = implode(' ', array_slice($words, 0, 2));
        $last_two_words = implode(' ', array_slice($words, -2));
        
    //    print_r($first_two_words);die;
        $roomCode = ($room == 'DELUXE') ? 2 : 1;
          $rateplanCode  =  $room . ' ' . $occupancy . ' ' . $mealPlan;
        foreach ($rowData as $date => $value) {
            $date = trim($date);
            $rate = trim($value);
            $insertData = [
                'room_type' => $room_type,
                'date' => $date,
                'price' => $rate
            ];        
            $insertedId = DB::table('eglobe_price_datewise')->insertGetId($insertData);
       


             //extra code
            $curl12 = curl_init();
            
            curl_setopt_array($curl12, array(
              CURLOPT_URL => 'https://www.eglobe-solutions.com/webapichannelmanager/rooms/' . env('EGLOBE_API_KEY'),
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'Authorization: Basic Og=='
              ),
            ));
            
            $response12 = curl_exec($curl12);
            
            curl_close($curl12);
           
            $responseData = json_decode($response12, true);
           
            if ($responseData === null && json_last_error() !== JSON_ERROR_NONE) {
                die('Error decoding JSON response: ' . json_last_error_msg());
            }
            
           
                //             $roomCode = null;
                // foreach ($responseData as $room) {
                //     if (strtolower($room['RoomName']) === strtolower($first_two_words)) {
                //         $roomCode = $room['RoomCode'];
                //         // print_r($roomCode);die;
                //         break; 
                //     }
                   
                // }

                // if ($roomCode) {
                    // $roomCode = $roomCode;
                    $last_two_words = str_replace(' ', '', $last_two_words);
                    $rateplanname = $last_two_words; // The rate plan name to match
                    $ratePlanCode = null; // Variable to store the matched RatePlanCode
                    $roomCode = null; // Variable to store the matched RoomCode
                    
                    foreach ($responseData as $room) {
                        if (strtolower($room['RoomName']) === strtolower($first_two_words)) {
                            $roomCode = $room['RoomCode'];
                            foreach ($room['RatePlans'] as $ratePlan) {
                                if (strtolower($ratePlan['RatePlanName']) === strtolower($rateplanname)) {
                                    $ratePlanCode = $ratePlan['RatePlanCode'];
                                    break 2; // Break out of both loops once a match is found
                                }
                            }
                        }
                    }
                    
                    if ($ratePlanCode !== null) {
                        // echo "Room code: " . $roomCode . "<br>";
                        // echo "Matched RatePlanCode: " . $ratePlanCode;
                        
                        $ratePlanWiseRates = [
                            ["RatePlanCode" => $ratePlanCode,
                             "Rate" => $rate], 
                        ];
                        
                        // Construct the data array
                        $data = [
                            "RoomCode" => $roomCode,
                            "DateFrom" => $date,
                            "DateTill" => $date,
                            "RatePlanWiseRates" => $ratePlanWiseRates
                        ];
                    
                        // Convert the data array to JSON
                        $jsonData = json_encode($data);
    
                            $curl = curl_init();
                            
                            curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://www.eglobe-solutions.com/webapichannelmanager/rates/' . env('EGLOBE_API_KEY') . '/bulkupdate',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS =>$jsonData,
                            CURLOPT_HTTPHEADER => array(
                                'Content-Type: application/json'
                            ),
                            ));
                            
                            $response = curl_exec($curl);
                            
                            curl_close($curl);
                            echo $response;
                    }


             


             //end extra code

    }
}

         
    // return response()->json(['message' => $response]);
}


//enf eglobe


public function revenue_management()
{
    return view('revenue.index');
}
public function get_room_type()
{
    $roomTypes = DB::table('room_types')->where('is_deleted',0)->get();

    // Fetch meal plans
    $mealPlans = DB::table('meal_plans')->get();
    $oyodata = DB::table('oyo_price_datewise')->get();
    $data = [
        
        'roomTypes' => $roomTypes,
        'mealPlans' => $mealPlans,
        'oyodata' => $oyodata,
    ];


    return response()->json($data);
}
public function update_room_data(Request $request)
{
    $filledData = $request->filledData;

    $groupedUpdates = [];
    // Process the filled data
    foreach ($filledData as $row) {
        $roomId = $row['roomId']; // Get the room ID
        $rowData = $row['data']; // Get the filled data for this room
        $occupancy = $row['occupancy']; 
        $mealPlan = $row['mealPlan']; 
        $room = $row['room']; 
        $room_type = $row['room_type']; 
       
        $roomCode = ($room == 'DELUXE') ? 2 : 1;
          $rateplanCode  =  $room . ' ' . $occupancy . ' ' . $mealPlan;
        //   print_r($rateplanCode);die;
        foreach ($rowData as $date => $value) {
            $date = trim($date);
            $rate = trim($value);
           

            $insertData = [
                'room_type' => $room_type,
                'date' => $date,
                'price' => $rate
            ];
            
            $insertedId = DB::table('oyo_price_datewise')->insertGetId($insertData);
    


        if (!isset($groupedUpdates[$date])) {
            $groupedUpdates[$date] = [
                'startDate' => $date,
                'endDate' => $date,
                'rates' => []
            ];
        }

        // Add the rate to the rates array for this date
        $groupedUpdates[$date]['rates'][] = [
            'roomCode' => $roomCode,
            'rate' => $rate,
            'rateplanCode' => $rateplanCode
        ];
    }
}
$jsonData = [
    'hotelCode' => '2381521', // Replace with dynamic hotelCode value
    'updates' => array_values($groupedUpdates) // Convert associative array to indexed array
];

// Encode the associative array to JSON
$jsonDataString = json_encode($jsonData);
        // Encode the associative array to JSON
        // $jsonDataString = json_encode($jsonData);
// print_r($jsonData);die;
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.oyoos.com/third_party/api/update_rates',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$jsonDataString,
            CURLOPT_HTTPHEADER => array(
              'HTTPXOYOLANG: en',
              'Content-Type: application/json',
              'Cookie: SESSION=YmU5MjQ1MmUtYTZlZS00YzY2LTlkOTYtYTY2MmZlOWZiMGM4; SESSION=MTFkNGM4YjEtNGMzZi00NTEzLTkxNDMtNjgwMDRlOTEzZmIz',
              'Authorization: Basic cGFzc2VyaW5lOnBhc3NlcmluLXRlY2hub2xvZ3k='
            ),
          ));
          
          $response = curl_exec($curl);
          
          curl_close($curl);
          echo $response;
          //end data send in oyo
    //     }
    // }

    // Return a response if needed
    return response()->json(['message' => $response]);
}


public function no_show()
{
    try {
        $currentDateTime = Carbon::now();
        $thirtyMinutesAgo = $currentDateTime->copy()->subMinutes(30);
        $apiCalled = false; // Flag to track if API was called

        $checkout = DB::table('arrivals')
            ->join('customers', 'customers.id', '=', 'arrivals.customer_id')
            ->where('arrivals.referred_by_name', 'oyo')
            ->select('arrivals.check_in', 'customers.Booking_id')
            ->get();
        
        foreach ($checkout as $item) {
            $checkinDateTime = Carbon::parse($item->check_in);
            $nextDay2PM = $checkinDateTime->copy()->addDay()->setTime(14, 0, 0); // Next day 2 PM
            $nextDay8PM = $checkinDateTime->copy()->addDay()->setTime(20, 0, 0); // Next day 8 PM

            // Check if the current time is between 2 PM to 8 PM on the next day after check-in
            if ($currentDateTime->between($nextDay2PM, $nextDay8PM)) {
                $bookingId = $item->Booking_id;

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://api.oyoos.com/third_party/api/no_show',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>json_encode(array(
                        "hotelCode" => "2381521",
                        "bookingId" => $bookingId // Use the dynamic booking ID here
                    )),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'HTTPXOYOLANG: en',
                        'Authorization: Basic cGFzc2VyaW5lOnBhc3NlcmluLXRlY2hub2xvZ3k='
                    ),
                ));
                
                $response = curl_exec($curl);

                $apiCalled = true; // Set flag to true as API call was made

                curl_close($curl);
            }
        }

        if ($apiCalled) {
            echo "API call was successful.";
        } else {
            echo "No API call was made as the current time is not between 2 PM to 8 PM on the next day after check-in.";
        }
    } catch (\Exception $e) {
        // Log the error
        // \Log::error('Error executing database query: ' . $e->getMessage());
    }
}






}
