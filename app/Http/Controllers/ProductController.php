<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Foodlist;
use App\FoodCategory,App\FoodItem;
use App\Customerfoodorder;
use App\Reservation;
use DB;
// use App\Room,App\RoomType;
  
class ProductController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
     
    public function __construct()
    {
        $this->core=app(\App\Http\Controllers\CoreController::class);
        // $this->middleware('auth');
    }
    public function indexnew()
    {

        // $products  = FoodCategory::with('food_items')->whereStatus(1)->whereIsDeleted(0)->orderBy('name','ASC')->get();
        $products = FoodCategory::with('food_items')
    ->where('name', 'Welcome Snacks')
    ->orWhere(function ($query) {
        $query->whereStatus(1)->whereIsDeleted(0);
    })
    ->orderByRaw("name='Welcome Snacks' DESC")
    ->orderBy('name', 'ASC')
    ->get();

return view('products', compact('products'));
        // return view('products',compact('products'));
        // $this->data['products'] = DB::table('food_items')
        // ->join('food_categories','food_categories.id','=','food_items.category_id')
        // ->where('food_items.status',1)
        // ->where('food_items.is_deleted',0)
        // ->select('food_items.name','food_items.*')
        // ->get();
        // $products = Foodlist::whereStatus(1)->whereIsDeleted(0)->orderBy('name','ASC')->get();
       
    }
    
    public function indexnewtwo(Request $request)
    {
        // $products = FoodCategory::join('food_items','food_items.category_id','=','food_categories.id')
        // ->where('food_categories.status',1)
        // ->where('food_categories.is_deleted',0)
        // ->select('food_categories.*')->with('food_items')
        // ->get();
    
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function cart()
    {
        return view('cart'); 
    }
  

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function addToCart($id)
    {
        
        // var roomnumber = $('#roomnumber').val();
        $product = Foodlist::findOrFail($id);
          
        $cart = session()->get('cart', []);
  
        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
        } else {
            $cart[$id] = [
                "name" => $product->name,
                "quantity" => 1,
                "price" => $product->price,
                "image" => $product->food_image,
                
            ];
        }
          
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function update(Request $request)
    {
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }
    
    public function getmobnumb(Request $request){
        $room = $request->roomvalue;
        $datalistgetuserdata = Reservation::whereStatus(1)->wherePaymentStatus(0)->whereIsDeleted(0)->whereNull('check_out')->orderBy('created_at','DESC')->distinct('room_num')->where('room_num', $room)->first();
        return response()->json([
            'userdatafood'=>$datalistgetuserdata,
        ]);
        
    }
    
    public function mobotpnew(Request $request)
    {
        date_default_timezone_set("Asia/Kolkata");
        $mobile = $request->mobile;
        $otp = $request->otp;

        $curl3 = curl_init();

        curl_setopt_array($curl3, array(
          CURLOPT_URL => 'https://cerf.cerfgs.com/runway/api/auth/login',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "username": "passerine_trans1",
            "password": "Admin@123"
        }',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXNzZXJpbmUiLCJpc3MiOiJ0cC1taWNyb3NlcnZpY2UiLCJhdXRob3JpdGllcyI6WyJST0xFXzU4Il0sImlhdCI6MTcwODU3ODcyMiwiZXhwIjoxNzA4NTgyMzIyfQ.G0RYCDfu2cEoeaQTZQ7lBuShwb-FtHn9WOiDZM30vJWyWV4SmwUE2qLh9BqtKQuw1k1RT4UXBWDpUPkMjlLIJg',
            'Content-Type: application/json'
          ),
        ));
        
        $response3 = curl_exec($curl3);
        
        curl_close($curl3);
        // echo $response;
        
        $responseData3 = json_decode($response3, true);
        
        // Check if decoding was successful
        if (json_last_error() === JSON_ERROR_NONE) {
            // Access the token from the response
            $token = $responseData3['data']['token'];
        
            // Use the $token variable as needed
            // echo "Token: " . $token;
        } 
        
        
        $curl2 = curl_init();
        
        curl_setopt_array($curl2, array(
          CURLOPT_URL => "https://cerf.cerfgs.com/cpaas?unicode=false&from=F9HTS&to=" . $mobile . "&dltContentId=1707170912806973975&text=Your%20F9Hotels%20Booking%20Confirmation%20OTP%20is%20". $otp .".%20Please%20do%20not%20share%20this%20OTP%20with%20anyone%20else.%20Powered%20by%20Passerine%20Group&token=".$token."",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        
        $response2 = curl_exec($curl2);
        
        curl_close($curl2);
           $existingCustomer = DB::table('customers')->where('mobile', $mobile)->first();

            if ($existingCustomer) {
                // Mobile number exists, update the OTP for the existing customer
                DB::table('customers')
                    ->where('id', $existingCustomer->id)
                    ->update(['otp' => $otp]);
            } else {
                // Mobile number doesn't exist, insert a new record
                DB::table('customers')->insert([
                    'mobile' => $mobile,
                    'otp' => $otp,
                ]);
            }


        $err = curl_error($curl2);
        curl_close($curl2);
        // return $response->status_code;
    }
    
    public function verifyotp(Request $request){
        $userotp = $request->enterotp;
        $usermob = $request->mobileno;
        // return $usermob;
        $data = DB::table('customers')->where('mobile',$usermob)->first();
        // $data = Customerfoodorder::where('mobile',$usermob)->where('otp',$userotp)->first();
        if($userotp == $data->otp){
            return true;
        }else{
            return false;
        }
    }
    public function RazorThankYou()
    {
        // session()->flash('cart', null);
      
        // session()->forget('cart');
        return view('ordermenu/thankyou');
    }
}