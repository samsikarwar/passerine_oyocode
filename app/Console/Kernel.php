<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\EmailJob;
use DB;
use Session;
use App\Reservation;
use Mail;
use App\Mail\ReportEmail;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Oyonoshow::class,
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('oyonoshow:run')->everyThirtyMinutes();
        // $email=DB::table('emails')->first();
        // $time=date('h:i',strtotime($email->time));
        // $schedule->job(new EmailJob);
       
        // $schedule->command('your:user_checkout')
        //          ->dailyAt('11:00');
               
                 
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
   
    



}
