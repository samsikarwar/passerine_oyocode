<?php

namespace App\Console\Commands;
use Auth, DB, Hash;
use Illuminate\Console\Command;

class user_checkout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $yesterday = now()->subDay(); 
        $todayDate = Carbon::now(); 
        $yesterday = $todayDate->subDay();
        $yesterdayDate = $yesterday->format('Y-m-d');
    
         DB::table('reservations')
         ->whereDate('check_in', $yesterdayDate)
         ->update(['check_out' => now()]);
        // return 0;
    }
}
