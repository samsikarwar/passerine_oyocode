<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class Oyonoshow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'oyonoshow:run';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $checkout = DB::table('arrivals')
        ->join('customers', 'customers.id', '=', 'arrivals.customer_id')
        ->where('arrivals.referred_by_name', 'oyo')
        ->select('arrivals.check_in', 'customers.Booking_id')
        ->get();
    
            $today = now()->toDateString();

            foreach ($checkout as $item) {
                $checkOutDate = date('Y-m-d', strtotime($item->check_in));

                if ($checkOutDate === $today) {
                    $data = array(
                        "hotelCode" => "2381521",
                        "bookingId" => $item->Booking_id 
                    );
                    
                    $postFields = json_encode($data);
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://api.oyoos.com/third_party/api/no_show',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>$postFields,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'HTTPXOYOLANG: en',
                        'Authorization: Basic dXNlckB1c2VyLmNvbTpzb21lc2VjdXJlcGFzc3dvcmQ='
                    ),
                    ));

                    $response = curl_exec($curl);

                    curl_close($curl);
                    echo $response;



                    echo "Today's check_out date matches Booking ID: " . $item->Booking_id;
 
                }
            }
    
        $this->info('This is the Oyonoshow command!');
    }
}
