@extends('layouts.master_backend')
@section('content')

<style>
    .form-container{
        margin-top: 61px;
        margin-left: 261px;
    }
        .gold-info {
            display: flex; 
          
        }
        table {
        border-collapse: collapse; 
        width: 100%;
    }

    th {
        padding: 10px;
        background-color: #f2f2f2;
    }

    td {
        padding: 5px;
    }
</style>



<div class="form-container">
    <form action="{{ route('gold_action') }}" method="post">
        @csrf 
        <div class="form-group">
            <label for="field1">Buy Gold</label>
            <input type="text" class="form-control" id="gold_amount" name="qty" required placeholder="Enter amount">
        </div>

        <div class="form-group">
            <label for="field1">You get Gold in Grams</label>
            <input type="text" class="form-control" id="gold_grams" name="message" required placeholder="Enter message"readonly>
        </div>
        <input type="hidden"id="getgram"value="{{$goldamountprice}}">
       

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Buy">
        </div>
    </form>
</div>
<br><br>


    
<div style="margin-left:261px;">
<a href="{{ route('gold_user') }}"class="btn btn-primary">See user Transaction</a>
<div class="gold-info">
    <div>
    <h2 id="availableGold">Available Gold: {{$goldGrms}}</h2>
</div>
<div style="margin-left: 116px;">
<h2 id="goldAmount">Gold Amount: {{$goldGrms*$goldamountprice}}</h2>
</div><br>
</div>
    <h2>Transaction History</h2>
    <table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Quantity</th>
            <th>Amount</th>
            <th>Customer Reference No</th>
            <th>Execution Date Time</th>
        </tr>
    </thead>
    <tbody>
            @foreach($transactions as $transaction)
                <tr>
                    <td>{{ $transaction['id'] }}</td>
                    <td>{{ $transaction['qty'] }}</td>
                    <!-- <td>{{ $transaction['gold_amount'] }}</td> -->
                   
                    <td>
    <?php
       $goldAmount = $transaction['gold_amount'];

       // Calculate the amount after adding 10% for service charge
       $amountAfterServiceCharge = $goldAmount / (1 - 0.10);
       $serviceCharge = $amountAfterServiceCharge - $goldAmount;
       
       // Calculate the final amount after adding 3% for GST
       $originalAmount = $amountAfterServiceCharge / (1 - 0.03);
       $gstAmount = $originalAmount - $amountAfterServiceCharge;
       
       // Display breakdown
       echo "Gold Amount: $goldAmount <br>";
       echo "Service Charge (10%): $serviceCharge <br>";
      
       echo "GST (3%): $gstAmount <br>";
       echo "Final Amount: $originalAmount";
    ?>
</td>
                    
                    <td>{{ $transaction['hotel_id'] }}</td>
                    <td>{{ $transaction['created_at_datetime'] }}</td>
                    <!-- Add more fields as needed -->
                </tr>
            @endforeach
        </tbody>
</table>
 </div>
                    
                   
                
                 


@endsection
@section('jquery')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<scrit>

</script>
<script>
    $(document).ready(function() {
        var settings = {
  "url": "http://dev.goldbck.com/api/v1/mmtc-portfolio/IMPGOLDBCK1695634474",
  "method": "GET",
  "timeout": 0,
};


$.ajax(settings).done(function (response) {
  console.log(response);

  var availableGold = response.qty;
  var goldAmount = response.totalAmount;

 
  $('#availableGold').text('Available Gold: ' + availableGold);
  $('#goldAmount').text('Gold Amount:  Rs  ' + goldAmount);

 var tableBody = document.getElementById("goldTransferTableBody");


var mmtcGoldTransfer = response.mmtcGoldTransfer;


var increment = 1;


for (var i = 0; i < mmtcGoldTransfer.length; i++) {
    var transfer = mmtcGoldTransfer[i];

   
    var newRow = document.createElement("tr");

   
    var idCell = document.createElement("td");
    idCell.textContent = increment++; 

   
    var qtyCell = document.createElement("td");
    qtyCell.textContent = transfer.qty;

    var orderIdCell = document.createElement("td");
    orderIdCell.textContent = transfer.orderId;
    
    var customer_ref_noCell = document.createElement("td");
    customer_ref_noCell.textContent = transfer.customer_ref_no;

    var executionDateTimeCell = document.createElement("td");
    executionDateTimeCell.textContent = transfer.executionDateTime;

   
    newRow.appendChild(idCell);
    newRow.appendChild(qtyCell);
    newRow.appendChild(orderIdCell);
    newRow.appendChild(customer_ref_noCell);
    newRow.appendChild(executionDateTimeCell);

    
    tableBody.appendChild(newRow);
}
});



    $('#datatable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
            
        ]
    } );
} );
</script>

<script>



</script>

<script>
        $(document).ready(function() {
            // Capture keyup event on gold_amount input
            $('#gold_amount').on('keyup', function() {
                // Get the value from gold_amount input
                var amount = $(this).val();
                var gram = $('#getgram').val();
                var originalAmount = amount / (1 + 0.03); // Calculate original amount before GST
                var amountAfterServiceCharge = originalAmount / (1 + 0.10); // Calculate amount after removing service charge
                var grams = amountAfterServiceCharge / gram;
                        //   alert(amountAfterServiceCharge);return false;
                // Perform your calculation here, for example, let's assume 1 unit of gold for every 10 units of amount
                // var grams = (amount / gram);
// alert(grams);
                // Update the value of gold_grams input
                $('#gold_grams').val(grams.toFixed(5)); // Adjust the decimal places as needed
            });
        });
    </script>

@endsection