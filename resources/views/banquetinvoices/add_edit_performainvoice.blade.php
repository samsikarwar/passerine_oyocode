@extends('layouts.master_backend')
@section('content')
<style type="text/css">
   #results {
      border: 1px solid;
      background: #ccc;
   }

   .results_guest {
      border: 1px solid;
      background: #ccc;
   }
</style>

<script>
$(document).on('focus',".date", function(){ //bind to all instances of class "date".
   $(this).datepicker({
      dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true,
      yearRange: "-50:+0"
   });
});
   $(function() {
      $(".datePickerDefault").datepicker({
         dateFormat: 'yy-mm-dd',
         minDate: 0

      });
      $(".datePickerDefault1").datepicker({
         dateFormat: 'dd-mm-yy',
         changeMonth: true,
          changeYear: true,
          yearRange: "-90:+0"
      });
   });
</script>
<div class="">

   {{ Form::open(array('url'=>route('save-banquet'),'class'=>"form-horizontal form-label-left",'files'=>true)) }}

   <div class="row hide_elem" id="existing_guest_section">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <script type="text/javascript">
               var path = "{{ route('autocomplete') }}";
               $('input.typeahead').typeahead({
                  source: function(query, process) {
                     return $.get(path, {
                        query: query
                     }, function(data) {
                        console.log(data);
                        console.log(process.id);
                        console.log(data.name);
                        return process(data);
                     });
                  }
               });
            </script>
         </div>
      </div>
   </div>
   <div class="row" id="new_guest_section">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>Banquet Invoice</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Booking ID <span ></span></label>-->
                    <!--    {{--Form::text('Booking_id',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"Booking_id", "placeholder"=>"Booking ID"])--}}-->
                    <!--</div>-->
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> And Number </label>-->
                    <!--    {{--Form::text('and_number',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"and_number", "placeholder"=>"And Number"])--}}-->
                    <!--</div>-->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Name <span class="required">*</span></label>
                        {{Form::text('name',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"name", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_fullname'), 'required'])}}
                    </div>
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Address <span class="required">*</span></label>
                        <input type="text" id="address" class="form-control col-md-6 col-xs-12" placeholder="Address" required name="address">
                        <!--{{Form::text('address',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"address", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_address'), 'required'])}}-->
                    </div>
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> {{lang_trans('txt_email')}} </label>-->
                    <!--    {{--Form::email('email',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"email", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_email')])--}}-->
                    <!--</div>-->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> {{lang_trans('txt_mobile_num')}} <span class="required">*</span></label>
                        {{Form::text('mobile',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"mobile","maxlength"=>"10", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_mobile_num'), 'required'])}}
                    </div>

                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> {{lang_trans('txt_gender')}} <span class="required">*</span></label>-->
                    <!--    {{-- Form::select('gender',config('constants.GENDER'),null,['class'=>'form-control col-md-6 col-xs-12','placeholder'=>lang_trans('ph_select'), 'required']) --}}-->
                    <!--</div>-->
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> {{lang_trans('txt_age')}} </label>-->
                    <!--    {{--Form::text('age',null,['class'=>"form-control datePickerDefault1 col-md-6 col-xs-12", "id"=>"age", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_age'), "autocomplete"=>"off"])--}}-->
                    <!--</div>-->
                </div>
                <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    {{Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])}}-->
                    <!--</div>-->
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Date of Party <span class="required">*</span></label>
                        <!--<input type="datetime-local" id="meeting-time" class="form-control col-md-6 col-xs-12" name="date_time">-->
                        {{Form::date('date',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"date", "placeholder"=>"Date", 'required'])}}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Time <span class="required">*</span></label>
                        <input type="time" id="time" class="form-control col-md-6 col-xs-12" required name="time">
                        <!--{{Form::time('time',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"time", "placeholder"=>"Date", 'required'])}}-->
                    </div>
                </div>
                
                 <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    {{Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])}}-->
                    <!--</div>-->
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Type of Party <span class="required">*</span></label>
                        <input type="text" id="type_of_party" class="form-control col-md-6 col-xs-12" placeholder="Type of Party"required name="type_of_party">
                        <!--{{Form::text('party',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"type_of_party", "placeholder"=>"Date", 'required'])}}-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Food Pax <span class="required">*</span></label>
                        <input type="text" id="food_pax" class="form-control col-md-6 col-xs-12"Placeholder="Food Pax" required name="food_pax">
                        <!--{{Form::text('time',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"food_pax", "placeholder"=>"Date", 'required'])}}-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Rate <span class="required">*</span></label>
                        <input type="text" id="rate" class="form-control col-md-6 col-xs-12" Placeholder="rate" name="rate">
                        <!--{{Form::text('rate',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"rate", "placeholder"=>"Rate", 'required'])}}-->
                    </div>
                </div>
                
                <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    {{Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])}}-->
                    <!--</div>-->
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Banquet Hall Charges <span class="required">*</span></label>
                        <input type="text" id="hall_charges" class="form-control col-md-6 col-xs-12"placeholder="Hall Charges" required name="hall_charges">
                        <!--{{Form::text('hall_charges',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"hall_charges", "placeholder"=>"Date", 'required'])}}-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Room Charges <span class="required">*</span></label>
                        <input type="text" id="room_charges" class="form-control col-md-6 col-xs-12" Placeholder="Room Charges" required name="room_charges">
                        <!--{{Form::text('room_charges',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"room_charges", "placeholder"=>"Date", 'required'])}}-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Other charges <span class="required">*</span></label>
                        <input type="text" id="other_charges" class="form-control col-md-6 col-xs-12"Placeholder="other_charges" name="other_charges">
                        <!--{{Form::text('other_charges',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"other_charges", "placeholder"=>"Rate", 'required'])}}-->
                    </div>
                </div>
                
                 <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    {{Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])}}-->
                    <!--</div>-->
                  

                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Total Amount <span class="required">*</span></label>
                        <input type="text" id="total_amount" class="form-control col-md-6 col-xs-12" placeholder="Total" required name="total_amount">
                        <input type="hidden"id="total"val="">
                        <!-- <input type="hidden"id="gst1"val="{{$banquet_charges->value}}"> -->
                        <!--{{Form::text('total_amount',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"total_amount", "placeholder"=>"Date", 'required'])}}-->
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12">
                <label class="control-label">GST</label>
                <select class="form-control col-md-6 col-xs-12" id="gst_option" required>
               <option value="1">Included GST</option>
               <option value="2">Excluded GST</option>
               </select>
               
               </div>              
                     <!-- <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Total Amount <span class="required">*</span></label>
                        <input type="text" id="total_amount" class="form-control col-md-6 col-xs-12" placeholder="Total" required name="total_amount">
                        <input type="hidden"id="total"val="">
                       
                    </div> -->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Advance <span class="required">*</span></label>
                        <!--<input type="datetime-local" id="meeting-time" class="form-control col-md-6 col-xs-12" name="date_time">-->
                        <input type="text" id="advance" class="form-control col-md-6 col-xs-12" placeholder="advance" required name="advance">
                        <!--{{Form::text('advance',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"advance", "placeholder"=>"Date", 'required'])}}-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Balance <span class="required">*</span></label>
                         <input type="text" id="balance" class="form-control col-md-6 col-xs-12" placeholder="balance" required name="balance">
                        <!--{{Form::text('balance',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"balance", "placeholder"=>"Rate", 'required'])}}-->
                    </div>
                </div>
                
               
                
                
            </div>
         </div>
      </div>
   </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">
                    <div class="row">
                       
                    </div>
                </div>
                <div class="col-md-10 col-sm-12 col-xs-12">
                    <button class="btn btn-success btn-submit-form2" type="submit"  id="arrivals_btn" style="float:right;">{{lang_trans('btn_submit')}}</button>
                </div>
            </div>
        </div>
        
    </div>
   </div>

    {{ Form::close() }}
</div>



<script>

  
  $('body').on('keyup', '#total_amount', function() {
         var amount =  $(this).val();
         // $("#total_amount").val(hall_charges);
         $("#total").val(amount);
        //  alert(room_charges);
  // code to execute when the key is released
});   
// $('body').on('keyup', '#room_charges', function() {
//          var room_charges =  $(this).val();
//       var total =  $("#total").val();
//       var sum = (parseInt(room_charges)+(parseInt(total)));
//         $("#total_amount").val(sum);
//     //   alert(sum);
       
//         //  alert(total);
//   // code to execute when the key is released
// });
  

    $(document).ready(function () {
      
      $("#gst_option").change(function () {
            var selectedValue = $(this).val();
            var totalAmount = parseFloat($("#total_amount").val());
            //  var gst = $("#gst1").val();
           
             // alert(new_value);
            if (selectedValue === "2") {
               var gstValue = "{{$banquet_charges->value}}";
              // var gstValue = $("#gst1").val();
          
              var new_value= (totalAmount*gstValue)/100;
               //  alert("GST is Excluded");
                var totalExcludedGST = totalAmount + new_value; // Assuming 18% GST rate
               //  $("#total").val(totalExcludedGST.toFixed(2));
                $("#total_amount").val(totalExcludedGST.toFixed(2));
               // alert(selectedValue);
               //  alert("GST is Excluded");
               
               
            }
            else{
               var amount= $('#total').val();
               // alert(amount);
               document.getElementById("total_amount").value = amount;
               //  $("#total_amount").html(amount.toFixed(2));
            }
        });
    });

    globalVar.page = 'room_reservation_add';
</script>
<script type="text/javascript" src="{{URL::asset('public/js/page_js/page.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/js/custom.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

@endsection
