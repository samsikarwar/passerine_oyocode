@extends('layouts.master_backend')
@section('content')
<style>
    div.box {
    height: auto;
    padding: 10px;
    overflow: auto;
    border: 1px solid #8080FF;
    background-color: #fcfcff;
}
    </style>
 <div class="padding">
        <div class="box">
            <div class="box-header dker">
                <h3>Smart Device Management</h3>
                <small>
                    <a href="">Home</a> /
                    <a>Manage Device</a>
                </small>
            </div>
        </div>
       
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                       
                        <div class="box-body">
                             <div class="form-group">
                             <div class="col-md-2">
                              <h5>Room No</h5>
                               <select class="form-select" id="room_no"style="height: 41px;">
                                    <option>Select Room No</option>
                                @foreach($room_no as $room_no)
                                    <option value="{{ $room_no->room_no }}"><tr>
                                <td>{{ $room_no->room_no }}</td>  
                                    </tr></option>
                                @endforeach
                                </select>
                                 
                                  </div>
          <div class="col-md-2"> 
            <h5>Light/Fan</h5>
            <input type="text" class="form-control" name="room_no_switch"id="room_no_switch" />
          </div>
          <div class="col-md-2"> 
            <h5>AC</h5>
            <input type="text" class="form-control" name="ac_switch"id="ac_switch" />
          </div>
                                  
                                
                 <div class="col-md-2"style="margin-top: 35px;">
                                <button class="btn btn-success" id="add"style="">Submit</button><br>
                              </div><br>
               
                
                <div class="col-md-12">
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <!--<th>Hotel Id</th>-->
                                    <th>Room No</th>
                                    <th>Light Fan id</th>
                                    <th>AC</th>
                                  
                                </tr>
                            </thead>
                            <tbody id="show">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                
               
              
            </div>
       
    </div>
@endsection
@section('jquery')
<script>
    
    function autoload()
                {
                 
                     $.ajax({
                            method : 'get',
                            url : '{{url("get_detail")}}',
                            success : function(res)
                            {
                                $("#show").html(res);
                            }
                        })
                }
                 autoload();
          function deleteCoupon(id)
          {
             var result = confirm("Want to delete?");
if (result) {
          $.ajax({
                            method : 'get',
                            data : {id:id},
                            url : '{{url("deleteCoupon")}}',
                            success : function(res)
                            {
                               autoload();
                            }
                        })  
    
}
            
               
          }
               
                $(document).ready(function(){
                    // alert('hello');
                   
                    //   alert(hotel);
                    
                    
                    $("#add").click(function(){
                         var room_no=  $('#room_no').val();
                         var room_no_switch=  $('#room_no_switch').val();
                        //  alert(room_no_switch);return false;
                       var ac_switch = $('#ac_switch').val();
                       
                        if(room_no=='')
                        {
                            alert('All Field Required !');
                            return false;
                        }
                        // alert('hello');
                        $.ajax({
                            method : 'get',
                            // alert('hello');
                            data : {room_no:room_no,room_no_switch:room_no_switch,ac_switch:ac_switch},
                            url : '{{url("/roomlight_action")}}',
                            // alert(data);
                            success : function(res)
                            {
                                if(res.status=='success')
                                {
                                    
                                 autoload();
                                }else
                                {
                                    alert(res.message);
                                }
                            }
                        })
                        })
                    })
               
                
    </script>

<script>
    $(document).ready(function() {
        $("#room_no").change(function(){
            var selectedRoom = $(this).val();
            $.ajax({
                            method : 'get',
                            data : {selectedRoom:selectedRoom},
                            url : '{{url("/roomlight_data")}}',
                            success: function(data) {
                                console.log(data);
                                    $('#room_no_switch').val(data.light_fan);
                                    $('#ac_switch').val(data.ac_switch);
                                },
                                error: function(xhr, status, error) {
                                    console.error('Error fetching room data:', error);
                                }
                        })
          
                        });

    $('#main').hide();
            $('#smart').hide();
    $("#light1").change(function(){
        var value = $(this).val();
        if(value==1){
            $('#main').show();
             $('#smart').hide();
        }
        if(value==2){
              $('#main').hide();
            $('#smart').show();
        }
    //   alert(value);
    } );
} );
</script>

<script>
   
    </script>
@endsection