@extends('layouts.master_backend')

@section('content')
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Food Table</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="post" action="{{route('add_food_table')}}">
                    @csrf
                        <div class="row">
                        
                            <div class="col-md-3">
                               <label>Add Table</label>
                               <input type="number" class="form-control" required name="table_num">
                            </div>
                            <div class="col-md-3"><button class="btn btn-info"style="margin-top:25px;">Submit</button></div>
                        </div>
                    </form>
                </div> 
                <div class="row"> 
                </div>
                    
            </div>
        </div>
    </div>
</div>
@endsection
@section('jquery')

@endsection