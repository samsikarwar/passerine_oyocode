@extends('layouts.master_backend')
@section('content')
<div class="">
 

  
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Pending Amount List</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <br/>
                    <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>{{lang_trans('txt_sno')}}</th>
                        <th>{{lang_trans('txt_guest_name')}}</th>
                        <th>{{lang_trans('txt_mobile_num')}}</th>
                        <th>{{lang_trans('txt_email')}}</th>
                        <th>{{lang_trans('txt_room')}}</th>
                        <th>{{lang_trans('txt_checkin')}}</th>
                        <th>{{lang_trans('txt_booking_amount')}}</th>
                        <th>{{lang_trans('txt_action')}}</th>
                      </tr>
                    </thead>
                    <tbody>
<?php 
$i = 1;
use App\Reservation;
// $reservations = Reservation::where('is_deleted', 0)->orderBy('created_at', 'DESC')->get();


$reservations = Reservation::select('reservations.*')
    ->join('payment_history', 'reservations.id', '=', 'payment_history.reservations_id')
    ->where('reservations.is_deleted', 0)
    ->groupBy('reservations.id')
    ->havingRaw('SUM(payment_history.payment) != reservations.total_amount')
    ->orderBy('reservations.created_at', 'DESC')
    ->get();

// $reservations = Reservation::select('reservations.*')
//     ->Join('payment_history', 'reservations.id', '=', 'payment_history.reservations_id')
//     ->where('reservations.is_deleted', 0)
//     ->groupBy('reservations.id')
//     ->havingRaw('SUM(CASE WHEN payment_history.payment > 0 THEN payment_history.payment ELSE 0 END) > 0')
//     ->orderBy('reservations.created_at', 'DESC')
//     ->get();

foreach($reservations as $reservation) {
?>
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo ($reservation->customer) ? $reservation->customer->name : 'NA'; ?></td>
        <td><?php echo ($reservation->customer) ? $reservation->customer->mobile : 'NA'; ?></td>
        <td><?php echo ($reservation->customer) ? $reservation->customer->email : 'NA'; ?></td>
        <td>
            <?php if($reservation->room_type): ?>
                <?php echo $reservation->room_type->title; ?><br/>
                (<?php echo lang_trans('txt_room_num'); ?>: <?php echo $reservation->room_num; ?>)
            <?php endif; ?>
        </td>
        <td><?php echo dateConvert($reservation->check_in, 'd-m-Y H:i'); ?></td>
        <td>
            <?php
            if(isset($count_room)) {
                $room = explode(',', $reservation->room_num);
                $total_room = count($room);
                $booking_payment = $reservation->per_room_price;
                $total_room = $count_room;
            } else {
                $booking_payment = $reservation->booking_payment;
            }
            ?>
            {{getCurrencySymbol()}} {{ numberFormat($booking_payment) }}
            <?php $unique_id = $reservation->unique_id; ?>
        </td>
        <td>
            <a class="btn btn-sm btn-warning" href="{{route('food-order',[$reservation->id])}}">{{lang_trans('btn_food_order')}}</i></a>
            <a class="btn btn-sm btn-success" href="{{route('view-reservation',[$reservation->id])}}">{{lang_trans('btn_view')}}</i></a>
            <a class="btn btn-sm btn-info" href="{{route('check-out-room',[$reservation->unique_id])}}">{{lang_trans('btn_checkout')}}</i></a>
            <a class="btn btn-sm btn-info" href="{{url('admin/edit-check-in/'.$reservation->unique_id)}}">{{lang_trans('btn_checkin_edit')}}</i></a>

            <a class="btn btn-sm btn-info"id="pending_close">Close Order</i></a>
        </td>
    </tr>
<?php 
    $i++;
}
?>
</tbody>

                  </table>

                </div>
            </div>
        </div>
    </div>


</div>
@endsection
