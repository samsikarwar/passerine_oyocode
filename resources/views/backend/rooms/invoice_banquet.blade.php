@php 

    $settings = getSettings();
    $gst_0 = $settings['gst_0'];
    $cgst_0 = $settings['cgst_0'];

    $gst = $settings['gst'];
    $cgst = $settings['cgst'];

    $gst_1 = $settings['gst_1'];
    $cgst_1 = $settings['cgst_1'];
    
    date_default_timezone_set("Asia/Kolkata");
    $currentdate = date('d-m-Y h:i:A');
    
@endphp
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <title>Invoice</title>
        <link href="{{URL::asset('public/assets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('public/css/invoice_style.css')}}" rel="stylesheet">
        <style>
          .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
            {
                padding:4px;
                font-size:12px;
            }
            .dsfht{
                position: absolute;
                right: 10%;
                top: 5%;
            }
        </style>
    </head>
    <body>
      
        <button onClick="printpage()" id="printpagebutton" class="dsfht">Print this page</button>
        <script type="text/javascript">
    function printpage() {
        var printButton = document.getElementById("printpagebutton");
        printButton.style.visibility = 'hidden';
        window.print();
        printButton.style.visibility = 'visible';
    }
</script>
        <div class="container">
            @if (isset($settings['invoice_logo']))
                <center><img src="{{URL::asset('/public/uploads/settings')}}/{{ $settings['invoice_logo'] }}" class="img-fluid" width="150"></center>
            @else
                {{-- <center><img src="/public/images/passerine.png"  class="img-fluid" width="150"></center> --}}
            @endif

            @isset ($settings['invoice_tagline'])
                <h3 style="margin-top:1px;"><center>{{$settings['invoice_tagline']}}</center></h3>
            @endisset
            
            @isset ($settings['invoice_name'])
                <h4><center>{{$settings['invoice_name']}}</center></h4>
            @endisset

            @isset ($settings['invoice_cin'])
                <h5><center>CIN: {{$settings['invoice_cin']}}</center></h5>
            @endisset

            @isset ($settings['invoice_address'])
                <h6><center>{{$settings['invoice_address']}}</center></h6>
            @endisset
            
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-11">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <strong>
                            GSTIN: {{$settings['gst_num']}}
                        </strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                        <strong>
                            Ph. {{$settings['hotel_phone']}}
                        </strong>
                        
                        <strong>
                            (M) {{$settings['hotel_mobile']}}
                        </strong>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="class-inv-12">
                        {{$settings['hotel_name']}}
                    </span>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="class-inv-13">
                        {{$settings['hotel_tagline']}}
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="class-inv-14">
                        {{$settings['hotel_address']}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="class-inv-15">
                        <span>
                            {{$settings['hotel_website']}}
                        </span>
                        |
                        <span>
                            E-mail:-
                        </span>
                        <span>
                           {{$settings['hotel_email']}}
                        </span>
                    </div>
                    <div class="class-inv-14">
                        <b>Performa Invoice</b>
                    </div>
                </div>
            </div>
            @php 
                $hello = $settings['banquit_invoice'];
                $acronym = "";
                $words = explode(" ", "$hello");
            @endphp

            @foreach($words as $w) 
                @php $acronym .= mb_substr($w, 0, 1); @endphp
            @endforeach
                
            <div class="row">
                <br/>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <strong class="fsize-label">
                            No.:
                            <span class="class-inv-19">
                                {{$hello}}{{$data_row[0]->invoice_id}}
                            </span>
                        </strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                        
                        <strong class="fsize-label">
                            Dated : 
                        </strong>
                        <span class-inv-16n="">
                            {{$currentdate}}
                        </span>
                        <br/>
                    </div>
                    
                </div>
                <br/>
                
            </div>
            
            <br/>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <strong class="fsize-label">
                            Customer Name:
                        </strong>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 class-inv-16">
                        {{$data_row[0]->name}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <strong class="fsize-label">
                            Mobile:
                        </strong>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 class-inv-16">
                        <span>
                           {{$data_row[0]->mobile}}
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="5%">
                        S.no
                    </th>
                    <th class="text-center" width="30%">
                        Particulars
                    </th>
                    <th class="text-center" width="9%">
                        Invoice Id
                    </th>
                      <th class="text-center" width="9%">
                        Address
                    </th>
                    
                     <th class="text-center" width="9%">
                        Date/Time
                    </th>
                     <th class="text-center" width="9%">
                      Food Pax
                    </th>
                    
                    
                   
                 
                    <th class="text-center" width="10%" colspan="2">
                        Amount ({{getCurrencySymbol()}})
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">
                        1.
                    </td>
                    <td class="text-center">
                        {{$data_row[0]->type_of_party}}
                    </td>
                    <td class="text-center">
                        {{$data_row[0]->invoice_id}}
                        
                    </td>
                      <td class="text-center">
                        {{$data_row[0]->address}}
                        
                    </td>
                    
                     </td>
                      <td class="text-center">
                        {{$data_row[0]->date}} {{$data_row[0]->time}}
                        
                    </td>
                    
                     <td class="text-center">
                        {{$data_row[0]->food_pax}}
                        
                    </td>
                    
                    
                    
                   
                    <td class="text-center" colspan="2">
                         {{$data_row[0]->total_amount}}
                    </td>
                </tr>
                
                <tr>
                    <th class="text-right" colspan="7">
                         Date :
                    </th>
                    <td class="text-right">
                        {{dateConvert($data_row[0]->date,'d-m-Y')}}
                    </td>
                </tr>
            
            <tr>
                   
                    <th class="text-right" colspan="7">
                       Rate
                    </th>
                   
                    
                    <td class="text-right">
                        {{numberFormat($data_row[0]->rate)}}
                    </td>
                  
                </tr>
                <tr>
                   
                    <th class="text-right" colspan="7">
                        Hall Charges
                    </th>
                   
                    
                    <td class="text-right">
                        {{numberFormat($data_row[0]->hall_charges)}}
                    </td>
                  
                </tr>
                <tr>
                    
                    <th class="text-right" colspan="7">
                        room_charges
                    </th>
                   
                    <td class="text-right">
                        {{numberFormat($data_row[0]->room_charges)}}
                    </td>
                   
                </tr>
                <tr>
                    
                    <th class="text-right" colspan="7">
                      Other Charges
                    </th>
                    
                    <td class="text-right">
                        {{numberFormat($data_row[0]->other_charges)}}
                    </td>
                 
                </tr>
               
          

                
                <tr>
                    <th class="text-right" colspan="7">
                       Total Amount
                    </th>
                    <td class="text-right">
                        {{numberFormat($data_row[0]->total_amount)}}
                    </td>
                </tr>
              
                
                <tr>
                    @if($data_row[0]->advance)
                    <th class="text-right" colspan="7">
                        Advance Paid ({{dateConvert($data_row[0]->created_at,'d-m-Y')}})
                        
                    </th>
                    <td class="text-right">
                       {{numberFormat($data_row[0]->advance)}}
                    </td>
                    @endif
                </tr>
                
              
                
                <tr>
                    <th class="text-right" colspan="7">
                        Total Amount Payable 
                    </th>
                    <td class="text-right">
                        {{numberFormat($data_row[0]->total_amount - $data_row[0]->advance)}}    
                    </td>
                </tr>
                
                <tr>
                    <th class="text-right" colspan="4">
                        Amount in Words:-
                    </th>
                    <td class="class-inv-17" colspan="5">
                        {{ ucwords(getIndianCurrency(numberFormat($data_row[0]->total_amount))) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="class-inv-20" style="padding-top:5px;">
                            Customer Signature
                        </div>
                    </td>
                    <td class="text-right" colspan="5">
                        <div class="class-inv-20" style="padding-top:5px !important;">
                              <!-- <img src="{{asset('public/sign.jpeg')}}" style="width:150px;height:60px"> -->
                              <img src="{{URL::asset('/public/uploads/settings')}}/{{ $settings['manager_signature'] }}" style="width:150px;height:60px">
                              <br>
                            Manager Signature
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
        
        </div>
    </body>
</html>
