@php 
  $settings = getSettings();
  $gst_0 = $settings['gst_0'];
  $cgst_0 = $settings['cgst_0'];

  $gst = $settings['gst'];
  $cgst = $settings['cgst'];

  $gst_1 = $settings['gst_1'];
  $cgst_1 = $settings['cgst_1'];
@endphp
<!DOCTYPE html>
<html lang="en">
     <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <title>{{$settings['site_page_title']}}: Invoice</title>
        <link href="{{URL::asset('public/assets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('public/css/invoice_style.css')}}" rel="stylesheet">
        <style>
          .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
            {
                padding:4px;
                font-size:12px;
            }
            .dsfht{
                position: absolute;
                right: 10%;
                top: 5%;
            }
        </style>
    </head>
    <body>
        @php 
        $jsonDecode = json_decode($data_row->amount_json);
$roomAmountDiscount = (isset($jsonDecode->room_amount_discount)) ? $jsonDecode->room_amount_discount : 0;
$advancePayment = ($data_row->advance_payment>0 ) ? $data_row->advance_payment : 0;
$secadvancePayment = ($data_row->sec_advance_payment>0 ) ? $data_row->sec_advance_payment : 0;
$thirdadvancePayment = ($data_row->third_advance_payment>0 ) ? $data_row->third_advance_payment : 0;
$fourthadvancePayment = ($data_row->fourth_advance_payment>0 ) ? $data_row->fourth_advance_payment : 0;
$fifthadvancePayment = ($data_row->fifth_advance_payment>0 ) ? $data_row->fifth_advance_payment : 0;
$sixthadvancePayment = ($data_row->sixth_advance_payment>0 ) ? $data_row->sixth_advance_payment : 0;
$totalAdvance = $advancePayment + $secadvancePayment + $thirdadvancePayment + $fourthadvancePayment + $fifthadvancePayment + $sixthadvancePayment;
$total_pay_amount=0;

if($data_row->checkin_type=='single')
{
$multi_room=$data_row->room_num;
$durOfStay = $data_row->duration_of_stay;
$bookingAmount = $data_row->per_room_price;
$roomQty = $data_row->room_qty == 0 ? 1 : $data_row->room_qty;
$totalRoomAmount = $data_row->total_amount;
$amount=$bookingAmount;
}

if($data_row->checkin_type=='multiple')
{
    $multi_room=array();
    foreach($room_numbers as $room_dt)
    {
     array_push($multi_room,$room_dt->room_num);
    }
    $multi_room=implode(',',$multi_room);
    $roomQty = count($room_numbers);
    $room_data=$room_numbers[0];
    $bookingAmount=$room_data->per_room_price;
    $totalRoomAmount = $data_row->total_amount;
    $amount=$bookingAmount;
 }
 
 //if($totalamountwithextrastay > 0){
   // $amount+=$totalamountwithextrastay;
 //}else{
   // $totalamountwithextrastay = 0;
 //}
 

    if(($amount>=0) && ($amount<=1000))
    {
        $gstPerc= $gst_0;
        $cgstPerc= $cgst_0;
        $applyGst= ($gstPerc + $cgstPerc)/100;
        $multiplyGst= $gstPerc/100;
        $multiplyCGst=  $cgstPerc/100;
        
        $applyGstnew= 1 + $applyGst;
        $multiplyGstnew= 1 + $multiplyGst ;
        $multiplyCGstnew=  1 + $multiplyCGst;
    }
    else if(($amount>=1001) && ($amount<=7500))
    {
        $gstPerc= $gst;
        $cgstPerc= $cgst;
        $applyGst= ($gstPerc + $cgstPerc)/100;
        $multiplyGst= $gstPerc/100;
        $multiplyCGst=  $cgstPerc/100;
        
        $applyGstnew= 1 + $applyGst;
        $multiplyGstnew= 1 + $multiplyGst ;
        $multiplyCGstnew=  1 + $multiplyCGst;
        
    }
    else if(($amount>=7501))
    {
        $gstPerc= $gst_1;
        $cgstPerc= $cgst_1;
        $applyGst= ($gstPerc + $cgstPerc)/100;
        $multiplyGst= $gstPerc/100;
        $multiplyCGst=  $cgstPerc/100;
        
        $applyGstnew= 1 + $applyGst;
        $multiplyGstnew= 1 + $multiplyGst ;
        $multiplyCGstnew=  1 + $multiplyCGst;
    }
        
$mode = $data_row->payment_mode;
$gstCal = gstCalc($amount,'room_gst',$gstPerc,$cgstPerc);


//print_r($totalRoomAmount);
$totalRoomAmount = numberFormat($data_row->duration_of_stay*$data_row->per_room_price*$roomQty);

$totalRoomAmountGst= numberFormat($totalRoomAmount/$applyGstnew);
$gstamounttotal= $totalRoomAmount-($totalRoomAmount/$applyGstnew);
$roomAmountGst = numberFormat($gstamounttotal/2);
$roomAmountCGst = numberFormat($gstamounttotal/2);

//$totalRoomAmountGst= round($totalRoomAmount - ($totalRoomAmount * $applyGst));
//$roomAmountGst = numberFormat($totalRoomAmount*$multiplyGst);
//$roomAmountCGst = numberFormat($totalRoomAmount*$multiplyCGst);


$finalRoomAmount = $totalRoomAmountGst+$roomAmountGst+$roomAmountCGst-$advancePayment-$secadvancePayment-$thirdadvancePayment-$fourthadvancePayment-$fifthadvancePayment-$sixthadvancePayment-$roomAmountDiscount;
$checkout = date_create($data_row->user_checkout);
$date = date_format($checkout,"Y-m-d H:i");
if($data_row->orders_info == null || $data_row->payment_status==0){
$gstPercFood = $settings['food_gst'];
$cgstPercFood = $settings['food_cgst'];
$foodOrderAmountDiscount = 0;
$gstFoodApply = 1;
} else {
$gstPercFood = $data_row->orders_info->gst_perc;
$cgstPercFood = $data_row->orders_info->cgst_perc;
$foodOrderAmountDiscount = ($data_row->orders_info->discount>0) ? $data_row->orders_info->discount : 0;
$gstFoodApply = ($data_row->orders_info->gst_apply==1) ? 1 : 0;
}
$totalOrdersAmount = $finalFoodAmount = $finalAmount = 0;

$invoiceNum = $data_row->invoice_num;
  if($type==2){
    $invoiceNum = ($data_row->orders_info!=null) ? $data_row->orders_info->invoice_num : '';
  }
  $discount = (isset($jsonDecode->room_amount_discount)) ? $jsonDecode->room_amount_discount : 0;
@endphp
<button onClick="printpage()" id="printpagebutton" class="dsfht">Print this page</button>
<!--<input id="printpagebutton" type="button" value="Print this page"  önclick="printpage()"/>-->
<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
         var printButton = document.getElementById("printpagebutton");
         printButton.style.visibility = 'hidden';
        //Print the page content
        window.print();
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
    }
</script>
    
        <div class="container">
            <div style="text-align: center;"><b>Tax Invoice</b></div>
            @if (isset($settings['invoice_logo']))
                <center><img src="{{URL::asset('/public/uploads/settings')}}/{{ $settings['invoice_logo'] }}" class="img-fluid" width="150"></center>
            @else
                {{-- <center><img src="/public/images/passerine.png"  class="img-fluid" width="150"></center> --}}
            @endif

            @isset ($settings['invoice_tagline'])
                <h3 style="margin-top:1px;"><center>{{$settings['invoice_tagline']}}</center></h3>
            @endisset
            
            @isset ($settings['invoice_name'])
                <h4><center>{{$settings['invoice_name']}}</center></h4>
            @endisset

            @if(isset($settings['invoice_cin']) && !empty($settings['invoice_cin']))
                  <h5><center>CIN: {{$settings['invoice_cin']}}</center></h5>
            @endif

            @isset ($settings['invoice_address'])
                <h6><center>{{$settings['invoice_address']}}</center></h6>
            @endisset
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-11">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <strong>
                            GSTIN: {{$settings['gst_num']}}
                        </strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                        <strong>
                            Ph. {{$settings['hotel_phone']}}
                        </strong>
                        
                        <strong>
                            (M) {{$settings['hotel_mobile']}}
                        </strong>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="class-inv-12">
                        {{$settings['hotel_name']}}
                    </span>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="class-inv-13">
                        {{$settings['hotel_tagline']}}
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="class-inv-14">
                        {{$settings['hotel_address']}}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="class-inv-15">
                        <span>
                            {{$settings['hotel_website']}}
                        </span>
                        |
                        <span>
                            E-mail:-
                        </span>
                        <span>
                            {{$settings['hotel_email']}}
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <strong class="fsize-label">
                           Invoice No.:
                            <span class="class-inv-19">
                            @if($type==2)
                                {{$and_number}}
                                @else
                                {{$and_number2}}
                                @endif
                            </span>
                        </strong><br>
                        <strong class="fsize">
                           Date:
                            <span class="class-inv">
                            {{dateConvert($data_row->created_at_checkout,'d-m-Y')}}
                            </span>
                        </strong>
                    </div>
                    <!-- <div class=""> -->
                        
                    <!-- </div> -->
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                        <br/>
                        <strong class="fsize-label">
                            Check In Date :
                        </strong>
                        <spa class-inv-16n="">
                            {{dateConvert($data_row->check_in,'d-m-Y h:i:A')}}
                        </spa>
                        <br/>
                        <strong class="fsize-label">
                            Check Out Date :
                        </strong>
                        <spa class-inv-16n="">
                            {{dateConvert($data_row->created_at_checkout,'d-m-Y h:i:A')}}
                        </spa>
                        
                    </div>
                </div>
            </div>
     
    
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-2">
            <strong class="fsize-label">
                Customer Name:
            </strong>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <span style="font-size: 15px;">
                {{$data_row->customer->name}}
            </span>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
            <strong class="fsize-label">
                Mobile No:
            </strong>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <span style="font-size: 15px;">
                {{$data_row->customer->mobile}}
            </span>
        </div>
    </div>
    
    @if(!empty($data_row->company_gst_num))
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <strong class="fsize-label">
                GST:
            </strong>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 class-inv-16">
            <span>
                {{$data_row->company_gst_num}}
            </span>
        </div>
    </div>
    @endif
    
    @if(!empty($data_row->gst_company))
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <strong class="fsize-label">
                Company Name:
            </strong>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 class-inv-16">
            <span>
                {{$data_row->gst_company}}
            </span>
        </div>
    </div>
    @endif
    
    @if(!empty($data_row->gst_address))
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 class-inv-6">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <strong class="fsize-label">
                Company Address:
            </strong>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 class-inv-16">
            <span>
                {{$data_row->gst_address}}
            </span>
        </div>
    </div>
    @endif
    
    
</div>
@if($type==1)
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="5%">
                        {{lang_trans('txt_sno')}}.
                    </th>
                    <th class="text-center" width="30%">
                        Particulars
                    </th>
                    <th class="text-center" width="9%">
                        HSN
                    </th>
                    <th class="text-center" width="10%">
                        Room Qty
                    </th>
                    
                     <th class="text-center" width="10%">
                        Room Number
                    </th>
                    
                    <th class="text-center" width="10%">
                        Room Type
                    </th>
                    
                    <th class="text-center" width="10%">
                        Room Rent ({{getCurrencySymbol()}})
                    </th>
                    <th class="text-center" width="10%">
                        Total Days
                    </th>
                    <th class="text-center" width="10%">
                        Amount ({{getCurrencySymbol()}})
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">
                        1.
                    </td>
                    <td class="text-center">
                        Room Charges With {{$data_row->meal_plan}}
                       
                    </td>
                    <td class="text-center">
                        996311
                    </td>
                    <td class="text-center">
                        {{$roomQty}}
                    </td>
                    
                     <td class="text-center">
                        {{$multi_room}}
                    </td>
                    
                    <td class="text-center">
                        {{$data_row->room_type->title}}
                    </td>
                    
                    <td class="text-center">
                       
                        {{ numberFormat($bookingAmount) }}
                    </td>
                    <td class="text-center">
                        {{$data_row->duration_of_stay}}
                    </td>
                    <td class="text-center">
                        {{-- numberFormat(($data_row->duration_of_stay*$data_row->per_room_price*$roomQty)+ $totalamountwithextrastay ) --}}
                        {{ numberFormat(($data_row->duration_of_stay*$data_row->per_room_price*$roomQty) ) }}
                    </td>
                </tr>
               {{-- @php $extraamount=0;  @endphp
                @foreach($payment as $extra)
                @if($extra->remark=='Early Check In')
                <tr>
                    <th class="text-right" colspan="8">
                       {{$extra->remark}}
                    </th>
                    <td class="text-right">
                      {{$extra->payment}}
                    </td>
                </tr>
                @php $extraamount+=$extra->payment @endphp
                @endif
                @endforeach --}}
                 <tr>
                    <th class="text-right" colspan="8">
                        Total
                    </th>
                    <td class="text-right">
                        
                        <?php 
                            echo numberFormat($totalRoomAmountGst);
                        ?>
                    
                    </td>
                </tr>
                
               
                <tr>
                    <th class="text-right" colspan="8">
                        SGST ({{$gstPerc}} %)
                    </th>
                    <td class="text-right">
                        <?php 
                        echo numberFormat($roomAmountGst);
                        ?>
                    </td>
                </tr>
                <tr>
                    <th class="text-right" colspan="8">
                        CGST ({{$cgstPerc}} %)
                    </th>
                    <td class="text-right">
                        <?php 
                        echo numberFormat($roomAmountGst);
                        ?>
                    </td>
                </tr>
                <?php 
                 $total_pay_amount=0;
                 foreach($payment as $key=>$p)
                 {
                     $number=$key+1;
                     if($number==1)
                     {
                         $number='First';
                     }else if($number==2)
                     {
                         $number='Second';
                     }else if($number==3)
                     {
                         $number='Third';
                     } else if($number==4)
                     {
                         $number='Fourth';
                     }else if($number==5)
                     {
                         $number='Fifth';
                     }else 
                     {
                         $numnber=$number;
                     }
                     echo "<tr>
                      <th class='text-right' colspan='8'>".$number." Payment (Payment Date : ".$p->payment_date.") ".$p->remark."</th>
                      <td class='text-right'>".numberFormat($p->payment)."</td>
                     </tr>";
                     
                     $total_pay_amount+=$p->payment;
                 }
                  
                 
                  
                ?>
                
                
                <tr>
                    <th class="text-right" colspan="8">
                       Total Amount
                    </th>
                    <td class="text-right">
                        @if($totalRoomAmount<$total_pay_amount)
                            {{ numberFormat($total_pay_amount) }}
                        @else    
                            {{ numberFormat($data_row->duration_of_stay*$data_row->per_room_price*$roomQty) }}
                        @endif    
                        
                    </td>
                </tr>
                
                  
                <tr>
                    <th class="text-right" colspan="8">
                        Less Paid
                    </th>
                    <td class="text-right">

                        {{ numberFormat($total_pay_amount) }}
                    </td>
                </tr>
             
                    
                
                
                
                <tr>
                    <th class="text-right" colspan="8">
                    {{lang_trans('txt_amount_payable')}}
                    </th>
                    <td class="text-right">
                        @if($totalRoomAmount<$total_pay_amount)
                            {{ numberFormat($total_pay_amount-$total_pay_amount) }}
                        @else    
                            {{ numberFormat($data_row->duration_of_stay*$data_row->per_room_price*$roomQty - $total_pay_amount) }}
                        @endif    
                            
                    </td>
                </tr>
                <tr>
                    <th class="text-right" colspan="5">
                        Amount in Words:-
                    </th>
                    <td class="class-inv-17" colspan="5">
                        @if($totalRoomAmount<$total_pay_amount)
                            {{ ucwords(getIndianCurrency(numberFormat($total_pay_amount))) }}
                        @else    
                            {{ ucwords(getIndianCurrency(numberFormat($data_row->duration_of_stay*$data_row->per_room_price*$roomQty))) }}
                        @endif    
                        
                        <!--@if($totalRoomAmount<$total_pay_amount)-->
                        <!--    {{ getIndianCurrency(numberFormat($total_pay_amount-$total_pay_amount)) }}-->
                        <!--@else    -->
                        <!--    {{ getIndianCurrency(numberFormat($data_row->duration_of_stay*$data_row->per_room_price*$roomQty - $total_pay_amount)) }}-->
                        <!--@endif    -->
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="class-inv-20" style="padding-top:5px;">
                            Customer Signature
                        </div>
                    </td>
                    <td class="text-right" colspan="5">
                        <div class="class-inv-20" style="padding-top:5px !important;">
                              <!-- <img src="{{asset('public/sign.jpeg')}}" style="width:150px;height:60px"> -->
                              <img src="{{URL::asset('/public/uploads/settings')}}/{{ $settings['manager_signature'] }}" style="width:150px;height:60px">
                              <br>
                            Manager Signature
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endif

@if($type==2)
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center" width="2%">
                        {{lang_trans('txt_sno')}}.
                    </th>
                    <th class="text-center" width="20%">
                        Item Details
                    </th>
                    <th class="text-center" width="5%">
                        Date
                    </th>
                    <th class="text-center" width="5%">
                        Item Qty
                    </th>
                    <th class="text-center" width="5%">
                        Item Price ({{getCurrencySymbol()}})
                    </th>
                    <th class="text-center" width="10%">
                        Amount ({{getCurrencySymbol()}})
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse($data_row->orders_items as $k=>$val)
                      @php
                        $totalOrdersAmount = $totalOrdersAmount + ($val->item_qty*$val->item_price);
                        if($val->item_price == 0.00)
                            {
                                $totalOrdersAmount =  $val->booking_payment;
                            }
                      @endphp
                <tr>
                    <td class="text-center">
                        {{$k+1}}
                    </td>
                    <td class="">
                        {{$val->item_name}}
                    </td>
                    <td class="text-center">
                        {{dateConvert($val->check_out,'d-m-Y')}}
                    </td>
                    <td class="text-center">
                        {{$val->item_qty}}
                    </td>
                    <td class="text-center">
                        {{$val->item_price}}
                    </td>
                    <td class="text-center">
                        {{$val->item_qty*$val->item_price}}
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6">
                        No Orders
                    </td>
                </tr>
                @endforelse
                    @php

                      $gstPerc = $cgstPerc = 0;
                      $discount = 0;
                      if($data_row->orders_info){
                        $gstPerc = $data_row->orders_info->gst_perc;
                        $cgstPerc = $data_row->orders_info->cgst_perc;
                        $discount = ($data_row->orders_info->discount>0) ? $data_row->orders_info->discount : 0;
                      }
                      $gst = gstCalc($totalOrdersAmount,'food_amount',$gstPerc,$cgstPerc);
                      $foodAmountGst = $gst['gst'];
                      $foodAmountCGst = $gst['cgst'];
                      $totalfoodamound = $totalOrdersAmount-$foodAmountGst-$foodAmountCGst;
                    @endphp
                <tr>
                    <th class="text-right" colspan="5">
                        Total
                    </th>
                    <td class="text-right">
                        {{ numberFormat($totalfoodamound) }}
                    </td>
                </tr>
                <tr>
                    <th class="text-right" colspan="5">
                        CGST ({{ numberFormat($food_cgst) }}%)
                    </th>
                    <td class="text-right">
                    {{ numberFormat((($totalfoodamound)*($food_cgst))/100) }}
                    </td>
                </tr>
                <tr>
                    <th class="text-right" colspan="5">
                        SGST ({{ numberFormat($food_gst) }}%)
                    </th>
                    <td class="text-right">
                    {{ numberFormat((($totalfoodamound)*($food_gst))/100) }}
                    </td>
                </tr>
              
                  

                    @php 
                      $finalFoodAmount = numberFormat($totalfoodamound);
                    @endphp
                <tr>
                    <th class="text-right" colspan="5">
                        Grand Total
                    </th>
                    <td class="text-right">
                        {{ numberFormat(($finalFoodAmount)+(numberFormat(($totalfoodamound)*($food_gst))/100)*2) }}
                    </td>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">
                        Amount in Words:-
                    </th>
                    <td class="class-inv-17" colspan="4">
                        {{ getIndianCurrency(numberFormat(($finalFoodAmount)+(numberFormat(($totalfoodamound)*($food_gst))/100)*2)) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="class-inv-20">
                            Customer Signature
                        </div>
                    </td>
                    <td class="text-right" colspan="3">
                        <div class="class-inv-20">
                        <img src="{{URL::asset('/public/uploads/settings')}}/{{ $settings['manager_signature'] }}" style="width:150px;height:60px">
                              <br>
                            Manager Signature
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div>
    {!!$settings['invoice_term_condition']!!}
</div>
@endif

