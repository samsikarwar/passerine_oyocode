@php 
  $settings = getSettings();
  $totalOrdersAmount = 0;
 
@endphp
<!DOCTYPE html>
<html lang="en">
     <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <title>{{$settings['site_page_title']}}: {{lang_trans('txt_invoice')}}</title>
        <link href="{{URL::asset('public/assets/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('public/css/invoice_style.css')}}" rel="stylesheet">
    </head>
    <style>
.paysta1{
                background: black;
                color: white;
                padding: 6px;
                font-size: 15px;
            } 
            .paid1cls{
                background: green;
                padding: 6px;
                color: white;
                font-size: 15px;
            } 
            .paysta2{
                background: black;
                color: white;
                padding: 6px;
                font-size: 14px;
            } 
            .notpaid2cls{
                background: green;
                padding: 6px;
                color: white;
                font-size: 14px;
            }
    </style>
    <body>

   

    <div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    {{$settings['hotel_name']}}, {{$settings['hotel_tagline']}}
                </font>
            </label>
        </div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    {{$settings['hotel_address']}}
                </font>
            </label>
        </div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    {{$settings['hotel_email']}}
                </font>
            </label>
        </div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    <!-- {{lang_trans('txt_website')}}: {{$settings['hotel_website']}} -->
                </font>
            </label>
        </div>
    
        <div class="class-inv-15" style="margin-left:741px;">
                       
                    </div>


        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <table border="0" border-style="ridge" class="class-inv-21">
                <tr>
                    <td align="left" width="100px">
                        <div>
                          <h5>  {{lang_trans('txt_gstin')}} </h5>
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                          <h5>  {{$settings['gst_num']}} </h5>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="100px">
                        <div>
                    <h5>  Phone No </h5>
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                       <h5> {{$settings['hotel_mobile']}} </h5>
                        </div>
                    </td>
                </tr>
              
                <tr>
                    <td align="left" width="100px">
                        <div>
                    <h5>Customer Name </h5>
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                       <h5> {{$data[0]->name}} </h5>
                        </div>
                    </td>
                </tr>
               
                <tr>
                    <td align="left" width="100px">
                        <div>
                           
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                            
                        </div>
                    </td>
                </tr>

                <tr>
                    <td align="left" width="100px">
                        <div>
                       <h5> Room -</h5>
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                        {{$data[0]->roomnumber}}
                        </div>
                    </td>
                </tr>
                <tr>
                <td class="txt-left" width="150px">
                        <div>
                        <h5>ORD# {{$data[0]->order_id}} </h5>
                        </div>
                    </td>
                
                <td class="txt-right" width="150px">
                        <div>
                        <h5 style=""> {{$data[0]->created_at}}</h5>
                        </div>
                    </td>
                </tr>

            </table>
            <h5>Original Due</h5>
            <!-- <h5>Due</h5> -->
            

            <table border="0" border-style="ridge" class="class-inv-21">
           
</table>
          

            <table border="1" border-style="ridge" style="font-size: 80%">
            <tbody>
                        <tr>
                            <td colspan="5" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <div class="" style="justify-content: space-between;">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="">Token No</h5>
                                    </div>

                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style=""> Item Details </h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="text-align: center;"> Rate </h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="text-align: center;"> Qty </h5>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: right;"class="gr" > Amount </h5>
                                    </div>
                                </div>
                            </td>
                        </tr>    
                       
                          
                        <tr>
                            <td colspan="5" style="border: none;">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <h5 style="">{{$data[0]->id}}
                                    <?php
// $foodNames = DB::table('order_items')
//                 ->where('order_id', $data_row->id)
//                 ->orderBy('id', 'asc')
//                 ->pluck('id');

// foreach ($foodNames as $foodName) {
//     echo $foodName;
//     echo '<br>';
// }
?>
                                    
                                    

                                </h5>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style=""> {{$data[0]->name}} </h5>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <h5 style="text-align: center;"> {{$data[0]->amount}}</h5>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <h5 style="text-align: center;">{{$data[0]->quantity}} </h5>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style="text-align: right;"> {{$data[0]->amount}} </h5>
                                </div>
                            </td>
                        </tr> 
                       
                        <tr>
                            <td colspan="5">
                              
                            </td>
                        </tr>
                       
                      
                       
                        <tr>
                            <td colspan="5" style="border-top: 1px solid black;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <h5 style=""> Subtotal  </h5>
                                       
                                    </div>
                                    <!-- <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div> -->
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <h5 style="text-align: right;"class="sa"> {{$data[0]->amount}} </h5>
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" style="border: none;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h5 style=""> CGST (2.5%)</h5>
                                       
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style="text-align: right;">{{$data[0]->amount * 0.025}}</h5>
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" style="border: none;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h5 style=""> SGST (2.5%)</h5>
                                       
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                   
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style="text-align: right;">{{$data[0]->amount * 0.025}}</h5>
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" style="border: none;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style=""> S/C({{$food_service_charges}}%)</h5>
                                       
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <h5 style="text-align: right;">{{$data[0]->amount * $food_service_charges/100}}</h5>
                                    </div>
                                </div>
                            </td>
                        </tr>

                       
                        
                       
            
                       
                        
                        <tr>
                            <td colspan="5" style="border-top: 2px solid black;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <h5 style=""> Payble Amount </h5>
                                    </div>
                                    <!-- <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;"> </h5>
                                    </div> -->
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <h5 style="text-align: right;">{{$data[0]->amount + $data[0]->amount * 0.05 + ($data[0]->amount * $food_service_charges/100) }} </h5>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        
                    </tbody>

            </table>
            <!-- <h4> demo </h4> -->
             <button class="btn btn-sm btn-success no-print" onclick="printSlip()">
                {{lang_trans('btn_print')}}
            </button>
         <a class="btn btn-sm btn-danger no-print" href="{{route('new-dashboard')}}" id="back-btn">
                {{lang_trans('btn_go_back')}}
            </a> 
        </div>
    </div>
    <script type="text/javascript" src="{{URL::asset('public/js/page_js/page.js')}}"></script> 
</body>
</html>