@extends('layouts.master_backend')
@section('rightColContent')
@php
$c=1;
$arr = [];
$arr1 = [];

foreach($payment_today as $vals)
{
 if($vals->payment_mode != "")
 {
       $arr[]= $vals->payment_mode;
 }
 if($vals->sec_payment_mode != "")
 {
       $arr[]= $vals->sec_payment_mode;
 }
 if($vals->third_payment_mode != "")
 {
       $arr[]= $vals->third_payment_mode;
 }
 if($vals->fourth_payment_mode != "")
 {
       $arr[]= $vals->fourth_payment_mode;
 }
 if($vals->fifth_payment_mode != "")
 {
       $arr[]= $vals->fifth_payment_mode;
 }
  if($vals->sixth_payment_mode != "")
 {
       $arr[]= $vals->sixth_payment_mode;
 }
 if($vals->sixth_payment_mode != "")
 {
       $arr[]= $vals->sixth_payment_mode;
 }
 
 
 
 
 
}

 $count = array_count_values($arr);

foreach($revenue_today as $val1)
{
  if($val1->referred_by_name == "OTA")
  {
    $arr1[]= $val1->referred_by;
  }
   if($val1->referred_by_name == "TA")
  {
    $arr1[]= $val1->referred_by;
  }
  if($val1->referred_by_name == "Corporate")
  {
    $arr1[]= $val1->referred_by;
  }
  if($val1->referred_by_name == "F9")
  {
    $arr1[]= $val1->referred_by_name;
  } 
  if($val1->referred_by_name == "Management")
  {
    $arr1[]= $val1->referred_by_name;
  } 
}
$count1 = array_count_values($arr1);

@endphp
@endsection
@section('content')
<style>
    .pmain{
        color: red;
        font-size: 18px;
    }
    @media screen and (max-width: 480px) {
        .jnewtab tr{
            display: grid;
            grid-template-columns: repeat(5, 1fr);
        }
        .jnewwidth{
            width: 100%;
        }
    }


       #notification {
    display: none;
    position: fixed;
    top: 80px;
    right: 500px;
    padding: 10px;
    background-color: green;
    color: white;
    border-radius: 5px;
}
/* The modal */
.modal12 {
  display: none; /* Hide the modal by default */
  position: fixed; /* Position it on top of other content */
  z-index: 1; /* Set a high z-index to make it appear above other elements */
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto; /* Enable scrolling if modal content overflows */
  background-color: rgba(0, 0, 0, 0.5); /* Add a semi-transparent background */
}

/* Modal content */
.modal-content12 {
  background-color: #fefefe;
  margin: 15% auto; /* Center vertically and horizontally */
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
  max-width: 870px;
}

/* Close button */
.close12 {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
  text-align: right;
}

.close12:hover,
.close12:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
.modal-box12 {
    DISPLAY: flex;
    justify-content: space-between;
    align-items: center;
}
th{
  width: 100px;
}
td{
  width: 115px;
  cursor: pointer;
}
tr:hover {
  background-color: #d9d6df;
}

.popup {
  z-index:9999;
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
}

.popup-content {
    background-color: #fff;
    width: 50%;
    max-width: 400px;
    margin: 100px auto;
    padding: 20px;
    border-radius: 5px;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
    text-align: center;
}

.popup-content p {
    font-size: 18px;
}

#closeButton {
    margin-top: 20px;
    background-color: #333;
    color: #fff;
    border: none;
    padding: 10px 20px;
    cursor: pointer;
}

#tableData{
  font-size: 40px;
}
</style>

  <h3><b>{{getSettings('hotel_name')}}</b></h3>
<!-- extra code for kitchen -->
  <div class="row kitchen_login">
          <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" type="button" data-toggle="modal" data-target="#myModal1">
                <div class="tile-stats">
                    @if($food_payment_today)
                      <div class="count">{{$food_payment_today}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="javascript:void(0);">{{lang_trans('txt_today_revenue')}}</a></h3>
                    <p>&nbsp;</p>
                    
                </div>
            </div>
                          
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" type="button" data-toggle="modal" data-target="#myModal1">
                <div class="tile-stats">
                    @if($food_payment_this_month)
                      <div class="count">{{$food_payment_this_month}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="javascript:void(0);">Month Revenue</a></h3>
                    <p>&nbsp;</p> 
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" type="button" data-toggle="modal" data-target="#myModal1">
                <div class="tile-stats">
                    @if($food_payment_upi)
                      <div class="count">{{$food_payment_upi}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="javascript:void(0);">UPI</a></h3>
                    <p>&nbsp;</p> 
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" type="button" data-toggle="modal" data-target="#myModal1">
                <div class="tile-stats">
                    @if($food_payment_cash)
                      <div class="count">{{$food_payment_cash}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="javascript:void(0);">Cash</a></h3>
                    <p>&nbsp;</p> 
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" type="button" data-toggle="modal" data-target="#myModal1">
                <div class="tile-stats">
                    @if($food_payment_phonepe)
                      <div class="count">{{$food_payment_phonepe}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="javascript:void(0);">PhonePe</a></h3>
                    <p>&nbsp;</p> 
                </div>
            </div>

    </div>
<!-- end extra code for kitchen -->


        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">
                <div class="tile-stats">
                    <div class="icon">
                        <i class="fa fa-caret-square-o-right"></i>
                    </div>
                    <div class="count">{{$counts[0]->today_check_ins}}</div>
                    <h3><a href="{{route('list-reservation')}}">{{lang_trans('txt_today_checkin')}}</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon">
                        <i class="fa fa-comments-o"></i>
                    </div>
                    <div class="count">{{$counts[0]->today_check_outs}}</div>
                    <h3><a href="{{route('list-check-outs')}}">{{lang_trans('txt_today_checkout')}}</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    @if(isset($perc_room))
                      <div class="count">{{$perc_room}} % <span style="font-size: 20px;color: darkgrey;">({{ $occupied_rooms}}/{{ $total_rooms}})</span></div>
                      @else
                      <div class="count">0 %</div>
                      @endif

                    <h3><a href="javascript:void(0);">Today Occupancy</a></h3>

                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal1">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    @if($payment_today1)
                      <div class="count">{{$payment_today1}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="javascript:void(0);">{{lang_trans('txt_today_revenue')}}</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    @if($total_expense)
                      <div class="count">{{$total_expense}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="{{route('today-expenses')}}">{{lang_trans('txt_today_expenses')}}</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    @if($month_expense)
                      <div class="count">{{$month_expense}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="{{route('list-expense')}}">Monthly Expense</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    @if($counts[0]->continue_rooms)
                      <div class="count">{{$counts[0]->continue_rooms}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                      <h3><a href="{{route('continuerooms-list')}}">Occupied Rooms</a></h3>
                    <!--<h3><a href="javascript:void(0);">{{--lang_trans('txt_continue_rooms')--}}</a></h3>-->
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats" style="background: aqua;">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    <!--@if($counts[0]->continue_rooms)-->
                      <!--<div class="count">{{--$counts[0]->continue_rooms - $counts[0]->today_check_ins--}} </div>-->
                    <!--  @else-->
                      <!--<div class="count">0</div>-->
                    <!--  @endif-->
                      @if($counts[0]->continue_rooms_new)
                      <div class="count">{{$counts[0]->continue_rooms_new}} + {{$counts[0]->continue_rooms_todaycheckout}} <span style="font-size:20px;color:red;">(tco)</span></div>
                      @else
                      <div class="count">0</div>
                      @endif
                      <h3><a href="{{route('continue-rooms')}}">Continue Rooms</a></h3>
                    <!--<h3><a href="javascript:void(0);">{{--lang_trans('txt_continue_rooms')--}}</a></h3>-->
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    @if($counts[0]->same_day_checkout)
                      <div class="count">{{$counts[0]->same_day_checkout}} </div>
                      @else
                      <div class="count">0</div>
                      @endif
                    <h3><a href="javascript:void(0);">Same Day Checkout</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                 
                    @if(isset($today_perc_room))
                      <div class="count">{{$today_perc_room}} % <span style="font-size: 20px;color: darkgrey;">({{ $today_occupied_rooms}}/{{ $total_rooms}})</span></div>
                      @else
                      <div class="count">0 %</div>
                      @endif

                    <h3><a href="javascript:void(0);">Today Check In  {{lang_trans('txt_room_occupancy')}}</a></h3>

                    
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    @if(isset($monthly_perc_room))
                      <div class="count">{{$monthly_perc_room}} % </div>
                      @else
                      <div class="count">0 %</div>
                      @endif

                    <h3><a href="javascript:void(0);">Monthly {{lang_trans('txt_room_occupancy')}}</a></h3>

                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    <div class="count">{{$counts[0]->only_today_arrivals}}</div>
                    <h3><a href="{{route('todays-upcoming')}}">Today's Upcoming</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            
        </div>    
        <div class="row top_tiles">   
        
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon">
                        <i class="fa fa-caret-square-o-right"></i>
                    </div>
                    <div class="count">{{$counts[0]->today_arrivals}}</div>
                    <h3><a href="{{route('list-arrival-reservation')}}">Reservation</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon">
                        <i class="fa fa-caret-square-o-right"></i>
                    </div>
                    <div class="count">{{$counts[0]->noShow_arrivals}}</div>
                    <h3><a href="{{route('list-arrival-reservation')}}">NoShow</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    <div class="count">{{$counts[0]->today_orders}}</div>
                    <h3><a href="{{route('orders-list')}}">{{lang_trans('txt_today_orders')}}</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="count">{{$pending_amount}} INR</div>
                    <h3><a href="{{route('pending_amount')}}">Pending Amount</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    <div class="count">{{abs(round($cashinhand))}} INR</div>
                    <h3><a href="#">Cash In Handss</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>

            
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    <div class="count">{{ isset($food_revenue) ? $food_revenue . ' INR' : 'No Revenue' }}</div>
                    <h3><a href="#">FNB Revenue</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    <div class="count">{{ isset($food_expense_today) ? $food_expense_today . ' INR' : 'No Expenses' }}</div>
                    <h3><a href="#">Today FNB Expenses</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <!--<div class="icon">-->
                    <!--    <i class="fa fa-sort-amount-desc"></i>-->
                    <!--</div>-->
                    <div class="count">{{ isset($food_expense) ? $food_expense . ' INR' : 'No Expenses' }}</div>
                    <h3><a href="#">Total FNB Expenses</a></h3>
                    <p>&nbsp;</p>
                </div>
            </div>

        </div>
       
        


            <div class="row justify-content-center">
                <br>
                <div class="col-md-12 jnewwidth">
                    <form method="post" action="{{route('new-dashboard')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <label>From Date</label>
                                    <input type="date" class="form-control" name="start" required value="{{$start}}">
                                </div>
                                <div class="col-md-3">
                                    <label>To Date</label>
                                    <input type="date" class="form-control" name="end" required value="{{$end}}">
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-info" style="margin-top:25px;">Search</button>
                                </div>
                            </div>    
                        </form>
        
                    <div class="card">
                        <div class="card-header">Collection Break-up Table</div>
                        <div class="card-body">
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <!--<th>Hotel</th>-->
                                        <!--<th>Location</th>-->
                                        <th>Totals</th>
                                        <?php
                                            $payment= DB::select("select*from payment_mode");
                                            foreach($payment as $p1)
                                            {
                                                echo "<th>".$p1->payment_mode."</th>";
                                            }
                                        ?>
                                        <th>Total OTA</th>
                                    </tr>
                                <?php
                                echo "<tr>";
                                
                                $t= DB::select("SELECT sum(payment_history.payment) as total FROM reservations inner join payment_history on payment_history.reservations_id=reservations.id and payment_history.payment_date between '$start' and '$end'");
                                $ter= DB::select("select sum(payment) as totaler from payment_history where extra_revenue = 'er' and date(payment_date) between '$start' and '$end'");
                                
                                $tbreak= DB::select("select sum(payment) as totalbreak from payment_history where extra_revenue = 'break' and date(payment_date) between '$start' and '$end'");
                                $advance_pay=DB::select("select sum(advance) as total_advance from arrivals where referred_by_name = 'F9' and date(created_at) between '$start' and '$end'");
                                // print_r($tbreak);
                                echo "<td>";
                                //   $tot = $ter[0]->totaler + $tbreak[0]->totalbreak;
                                  $tot2 = $ter[0]->totaler - $tbreak[0]->totalbreak;
                                //   echo $t[0]->total - $tot + $tot2;
                                   echo $t[0]->total + $tot2 + $advance_pay[0]->total_advance;
                                "</td>";
                                // print_r($tot2);
                                foreach($payment as $p)
                                {
                                    $data = DB::select("SELECT sum(payment_history.payment) as total FROM reservations inner join payment_history on payment_history.reservations_id=reservations.id and payment_history.payment_date between '$start' and '$end' and payment_history.mode='$p->id'");
                                    $advance_pay=DB::select("select sum(advance) as total_advance from arrivals where referred_by_name ='F9' and payment_mode='$p->id' and date(created_at) between '$start' and '$end'");
                                    // $data= DB::select("select sum(payment) as total from payment_history where payment!='' and mode='$p->id' and payment_date between '$start' and '$end'");
                                    $dataerer= DB::select("select sum(payment) as totaler from payment_history where extra_revenue = 'er' and mode='$p->id' and date(payment_date) between '$start' and '$end'");
                                    $databreak= DB::select("select sum(payment) as totalbreak from payment_history where extra_revenue = 'break' and mode='$p->id' and date(payment_date) between '$start' and '$end'");
                                    echo "<td>";
                                    if(!empty($data[0]->total)){
                                        // $tot = $dataerer[0]->totaler + $databreak[0]->totalbreak;
                                        $tot2 = $dataerer[0]->totaler - $databreak[0]->totalbreak;
                                        // echo $data[0]->total - $tot + $tot2;
                                        echo $data[0]->total + $advance_pay[0]->total_advance + $tot2;
                                    }else
                                    {
                                        echo 0;
                                        echo $advance_pay[0]->total_advance + $tot2;
                                    }
                                      
                                    "</td>";
                                    
                                }
                                 
                                $d= DB::select("select sum(payment) as total from payment_history where reservations_id!='0' and payment!='' and mode!='1' and mode!='2' and mode!='6' and date(payment_date) between '$start' and '$end'");
                                $advance_pay=DB::select("select sum(advance) as total_advance from arrivals where referred_by_name = 'F9' and payment_mode!='1' and payment_mode!='2' and payment_mode!='6' and date(created_at) between '$start' and '$end'");
                                $otatotal = $d[0]->total + $advance_pay[0]->total_advance;
                                echo "<td>".$otatotal."</td>";
                                
                                // echo "<td>".$d[0]->total."</td>";
                                
                                echo "</tr>";
                            ?>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
<anytag data-tippy-content="some tooltip"></anytag>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
google.charts.load('current', {'packages':['line']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', '');
  data.addColumn('number', '');
  
  data.addRows([
    @foreach($graphTotalCheckin as $checkin)
    ['{{$checkin->checkinDate}}',  {{$checkin->totalCheckin}}],
    @endforeach
  ]);

  var options = {
    chart: {
      title: '{{$currentMonthName}} Month Total Checkin',
    },
    // width: 1000,
    height: 300,
    axes: {
      x: {
        0: {side: 'top'}
      }
    }
  };

  var chart = new google.charts.Line(document.getElementById('order_graph_checkin'));
  chart.draw(data, google.charts.Line.convertOptions(options));
}

google.charts.setOnLoadCallback(drawRevenueChart);
function drawRevenueChart() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', '');
  data.addColumn('number', '');
  
  data.addRows([
    @foreach($graphTotalRevenue as $checkin)
    ['{{$checkin->checkinDate}}',  {{$checkin->totalRevenue}}],
    @endforeach
  ]);

  var options = {
    chart: {
      title: '{{$currentMonthName}} Month Total Revenue',
    },
    // width: 1000,
    height: 300,
    axes: {
      x: {
        0: {side: 'top'}
      }
    }
  };

  var chart = new google.charts.Line(document.getElementById('order_graph_revenue'));
  chart.draw(data, google.charts.Line.convertOptions(options));
}

google.charts.setOnLoadCallback(drawChartaRR);
function drawChartaRR() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', '');
  data.addColumn('number', '');
  
  data.addRows([
    @foreach($aRR as $checkin)
    ['{{$checkin->checkinDate}}',  {{$checkin->sumARR}}],
    @endforeach
  ]);

  var options = {
    chart: {
      title: '{{$currentMonthName}} Month Total ARR',
    },
    // width: 1000,
    height: 300,
    axes: {
      x: {
        0: {side: 'top'}
      }
    }
  };

  var chart = new google.charts.Line(document.getElementById('order_aRR'));
  chart.draw(data, google.charts.Line.convertOptions(options));
}

google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawReferredByChart);
function drawReferredByChart() {
var data = google.visualization.arrayToDataTable([
  ['Payment Mode', 'Revenue'],
  @foreach($graphTotalReferredBy as $item)
  ['{{$item->rby}}',     {{$item->totalRevenue}}],
  @endforeach
]);

var options = {
  title: '{{$currentMonthName}} Month Booking Source',
  is3D: true,
  height: 400,
};

var chart = new google.visualization.PieChart(document.getElementById('referred_by_3d'));
chart.draw(data, options);
}


google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawPaymentModeChart);
function drawPaymentModeChart() {
    var data = google.visualization.arrayToDataTable([
      ['Payment Mode', 'Revenue'],
      @foreach($graphTotalPaymentMode as $item)
      ['{{$item->paymentMode}}', {{$item->totalRevenue}}],
      @endforeach
    ]);
    
    var options = {
      title: '{{$currentMonthName}} Month Payment Mode',
      is3D: true,
      height: 400,
    };
    
    var chart = new google.visualization.PieChart(document.getElementById('payment_mode_3d'));
    chart.draw(data, options);
}


google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawPaymentModeChartaaa);
function drawPaymentModeChartaaa() {
    var data = google.visualization.arrayToDataTable([
      ['Payment Mode', 'Revenue'],
      @foreach($graphTotalPaymentMode_all as $item)
      ['{{$item->paymentMode_all}}', {{$item->totalRevenue}}],
      @endforeach
    ]);
    
    var options = {
      title: '{{$currentMonthName}} Month Booking Payment',
      is3D: true,
      height: 400,
    };
    
    var chart = new google.visualization.PieChart(document.getElementById('payment_mode_3d_all'));
    chart.draw(data, options);
}

</script>

<!-- Modal Today Check In -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Today Revenue </h4>
      </div>
      <div class="modal-body">
        
    @foreach($count as $key => $vals)
      {{$key}} : {{$vals}} <br>
    @endforeach
    
   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="ex1" class="modal">
  <a href="#" rel="modal:close">Close</a>
</div>
<!-- End Modal Today Check In -->


<!-- Modal Today Revenue -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Today Check In </h4>
      </div>
      <div class="modal-body">
        
      @foreach($count1 as $key => $vals1)
      {{$key}} : {{$vals1}} <br>
    @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="ex1" class="modal">
  <a href="#" rel="modal:close">Close</a>
</div>
<!-- End Modal Today Revenue -->

<div class="row-sm-12 top_tiles">
  <div class="col-lg-12 ">
    <!-- <h3>Rooms</h3> -->
    <div class="x_content">
      <table id="datatable_" class="table table-striped table-bordered">
        <tbody>

          <table class="table table-striped table-bordered jnewtab">

            <tbody>
   <?php
//                 echo "<pre>";
//                 print_r($inner);
//                 die();
                
//                 ?>
              @foreach($floor as $key=>$basic)
              @foreach($basic as $key=>$inner)
              <tr class="mt-5 ml-5">
                @if($key == 0)
                <button class="btn-sm" style="background:yellow;color:black;">Check Out&ensp;&ensp;({{$checked_rooms_count}})</button>
                @elseif($key == 1)
                <button class="btn-success btn-sm">Ready &ensp;&ensp;({{$totrooms - $checked_rooms_count - $management_rooms_count - $ota_rooms_count - $corporate_rooms_count -$booked_rooms_count - $undermaintinance_count - $fit_rooms_count - $dirty_rooms_count }}) </button>
                @elseif($key == 2)
                <button class="btn-light btn-sm" style="background:black;color:white;">Management&ensp;({{$management_rooms_count}}) </button>
                @elseif($key == 3)
                <button class="btn-sm" style="background:blue;color:white;">OTA &ensp;&ensp;({{($ota_rooms_count-$fab_rooms_count)-$oyo_rooms_count}})</button>
                @elseif($key == 4)
                <button class="btn-dark btn-sm">Corporate &ensp;&ensp;({{$corporate_rooms_count}})</button>
                @elseif($key == 5)
                <button class="btn-info btn-sm">TA &ensp;&ensp;({{$booked_rooms_count}})</button>
                @elseif($key == 6)
                <button class="btn-sm" style="background:red;color:white;">Block/R&M issue &ensp;&ensp;({{$undermaintinance_count}})</button>
                @elseif($key == 7)
                <button class="btn-sm" style="background:#7CFC00;color:black;">{{$settings1[0]->value}} &ensp;&ensp;({{$fit_rooms_count}})</button>
                @elseif($key == 8)
                <button class="btn-sm" style="background:brown;color:white;">In Cleaning &ensp;&ensp;({{$dirty_rooms_count}})</button>
                @elseif($key == 9)
                <button class="btn-sm" style="background:#bf951eeb;color:white;"> Fab&ensp;&ensp;({{$fab_rooms_count}})</button>
                @elseif($key == 10)
                <button class="btn-sm" style="background:#ed1967eb;color:white;"> Oyo&ensp;&ensp;({{$oyo_rooms_count}})</button>
                @endif



                @foreach($inner as $key=>$val)

                @if($loop->first)
                <td>Floor-{{$inner[$key]->floor-1}}</td>
                @endif
                @if($val->count())
                <td>
                  <!--href="{{route('delete-reservation',[$reserv ?? '']-->
                  @if( $checked_rooms_count != 0 && in_array($val->room_no, $checked_rooms))
                  <?php $reserv = $reservation->where('room_num', $val->room_no)->pluck('unique_id')->first();
                  if (!$reserv) {
                    $reserv = 0;
                  }
                  ?>
                  <a class="btn btn-sm" style="background:yellow;color:black;" href="{{route('edit-room-reservation',[$reserv])}}">{{$val->room_no}}</a>

                  @elseif($dirty_rooms_count != 0 && in_array($val->room_no, $dirty_rooms))
                  <?php $reserv = $reservation->where('room_num', $val->room_no)->pluck('id')->first();
                  if (!$reserv) {
                    $reserv = 0;
                  }
                  ?>

                  <a class="btn btn-sm" style="background:brown;color:white;" data-toggle="modal" data-target="#exampleModal{{$reserv}}">{{$val->room_no}}(D)</a>
                  <div class="modal fade" id="exampleModal{{$reserv}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"> X </button>-->
                        <form action="{{route('delete-reservation',[$reserv])}}" method="GET">
                          <div class="modal-body">
                            <div class="container text-center">
                              <div>
                                <img src="{{url('public/images/mark.png')}}" class="img-fluid" width="30%">
                              </div>
                              <h1>Are you sure?</h1>
                              <p class="pmain">You want to change status to ready</p><br>
                              <input type="hidden" id="idstatusroom" name="id" value="{{$reserv}}">
                              <div>
                                <button type="submit" class="btn btn-primary" style="width:15%">Yes</button>
                                <button type="button" class="btn btn-danger" style="width:15%" data-dismiss="modal" aria-label="Close">No</button>
                              </div>

                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  @elseif($fit_rooms_count != 0 && in_array($val->room_no, $fit_rooms))
                  <?php $reserv = $reservation->whereIn('room_num', $val->room_no)->pluck('unique_id')->first();
                  //$reserv=getBookRoomId($val->room_no);
                  //  echo $reserv;die();


                  
                  if (!$reserv) {
                    $reserv = 0;
                  }
                  ?>

                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>

                  <a class="btn btn-sm" style="background:#7CFC00;color:black;" href="{{route('edit-room-reservation',[$reserv])}}">{{$val->room_no}}({{ $titleInitial }})


                  </a>

                  @elseif($booked_rooms_count != 0 && in_array($val->room_no, $booked_rooms))
                  <?php $reserv = $reservation->where('room_num', $val->room_no)->pluck('id')->first();
                  if (!$reserv) {
                    $reserv = 0;
                  } ?>

                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>

                  <a class="btn btn-info btn-sm" href="{{route('check-out-room',[$reserv])}}">{{$val->room_no}}({{ $titleInitial }})</a>

                  @elseif($undermaintinance_count != 0 && in_array($val->room_no, $undermaintinance))
                  <?php $reason = App\Room::where('room_no', $val->room_no)->pluck('reason')->first();
                  if (!$reason) {
                    $reason = 'undefined';
                  } ?>
                  <button type="button" style="background:red;color:white;" {{Popper::arrow()->size('large')->distance(10)->position('bottom')->pop($reason ?? "")}} class="btn  btn-sm">{{$val->room_no}}(D)</button>


                  @elseif($fab_rooms_count != 0 && in_array($val->room_no, $fab_rooms))
                  <?php $reserv = $reservation->where('room_num', $val->room_no)->pluck('unique_id')->first();
                  if (!$reserv) {
                    $reserv = 0;
                  } ?>
                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>
                  <a class="btn btn-sm" style="background:#bf951eeb;color:white;" href="{{route('edit-room-reservation',[$reserv])}}">{{$val->room_no}}({{ $titleInitial }})</a>
                  @elseif($oyo_rooms_count != 0 && in_array($val->room_no, $oyo_rooms))
                  <?php $reserv = $reservation->where('room_num', $val->room_no)->pluck('unique_id')->first();
                  if (!$reserv) {
                    $reserv = 0;
                  } ?>
                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>
                  <a class="btn btn-sm" style="background:#ed1967eb;color:white;" href="{{route('edit-room-reservation',[$reserv])}}">{{$val->room_no}}({{ $titleInitial }})</a>

                  @elseif(($ota_rooms_count-$fab_rooms_count)-$oyo_rooms_count != 0 && in_array($val->room_no, $ota_rooms))
                  <?php $reserv = $reservation->where('room_num', $val->room_no)->pluck('unique_id')->first();
                  if (!$reserv) {
                    $reserv = 0;
                  } ?>
                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>
                  <a class="btn btn-sm" style="background:blue;color:white;" href="{{route('edit-room-reservation',[$reserv])}}">{{$val->room_no}}({{ $titleInitial }})</a>

                  @elseif($corporate_rooms_count != 0 && in_array($val->room_no, $corporate_rooms))
                  <?php $reserv = $reservation->where('room_num', $val->room_no)->pluck('id')->first();
                  if (!$reserv) {
                    $reserv = 0;
                  } ?>
                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>
                  <a class="btn btn-dark btn-sm" href="{{route('check-out-room',[$reserv])}}">{{$val->room_no}}({{ $titleInitial }})</a>

                  @elseif($management_rooms_count != 0 && in_array($val->room_no, $management_rooms))
                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>
                  <button type="button" class="btn btn-sm" style="background:black;color:white;" href="{{--route('check-out-room',[$reserv])--}}">{{$val->room_no}}({{ $titleInitial }})</button>

                  @else
                  <?php
                  $roomTypeId = DB::table('rooms')
                    ->select('rooms.room_type_id', 'room_types.title')
                    ->join('room_types', 'rooms.room_type_id', '=', 'room_types.id')
                    ->where('room_no', $val->room_no)
                    ->first();
                  $titleInitial = substr($roomTypeId->title, 0, 1);
                  ?>
                  <a href="{{route('room-reservation-available',[$val->id])}}" type="button" class="btn btn-success btn-sm">{{$val->room_no}}({{ $titleInitial }})</a>
                  @endif
                </td>
                @else
                {{lang_trans('txt_no_rooms')}}
                <a class="btn btn-sm btn-success" href="{{route('add-room')}}">{{lang_trans('txt_add_new_rooms')}}</a>
                @endif
                @endforeach
              </tr>
              @endforeach
              @endforeach

            </tbody>
          </table>
        </tbody>
      </table>
    </div>

  </div>
</div>

<div class="row">
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <div class="col-sm-12">
              <div class="col-sm-4 col-md-4 col-lg-4 p-left-0">
                <h2>Orders</h2>
              </div>
              <div class="col-sm-4 col-md-4 col-lg-4 text-right">
                <div class="">
                  <nav>

                    <a aria-expanded="false" class="dropdown-toggle btn btn-info" data-toggle="dropdown" href="javascript:;">
                      New Order
                      <!--<span class=" fa fa-angle-down"></span>-->
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                      <li>
                        <!-- <a href="#" class="btn btnmrodrop">DINEIN</a> -->
                        <a href="#" class="btn btnmrodrop" id="dineInButton">DINEIN</a>
                      </li>
                      <li>
                        <a href="#" class="btn btnmrodrop">TAKEAWAY</a>
                      </li>
                      <li>
                        <a href="#" class="btn btnmrodrop">DELIVERY</a>
                      </li>
                      <li>
                        <a href="{{route('room-order')}}" class="btn btnmrodrop">Room</a>
                      </li>
                    </ul>

                  </nav>
                </div>
                <div id="popup" class="popup">
    <div class="popup-content">
        <p>Select Table Number</p>
        <div id="tableData"></div> <!-- Dynamic data will be displayed here -->
        <!-- <button id="closeButton">Close</button> -->
        <a href="{{route('dining-orders')}}" class="btn btn-primary">Search</a>
        
    </div>
</div>
              </div>
              <div class="col-sm-4 col-md-4 col-lg-4 text-right">
                <a href="{{route('latest-orders')}}" class="btn btn-primary">Latest Orders</a>
              </div>
            </div>
            <div class="clearfix"></div>

            

            <!-- <div class="x_content"> -->
            <div class="x_content" style="max-height: 300px; overflow-y: scroll;">
              @foreach($orders as $k=>$val)
              @php
              $totalAmount = 0.00;
              @endphp
              @if($val->order_history)
              @foreach($val->order_history as $key_OH=>$val_OH)
              @if($val_OH->orders_items)
              @foreach($val_OH->orders_items as $key_OI=>$val_OI)
              @php
              $price = $val_OI->item_price*$val_OI->item_qty;
              $totalAmount = $totalAmount+$price;
              $totalAmmountsArr[$val->id] = $totalAmount;
              @endphp
              @endforeach
              @endif
              @endforeach
              @endif
              @endforeach
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>{{lang_trans('txt_sno')}}</th>
                    <th>Name</th>
                    <th>Room No.</th>
                    <th>{{lang_trans('txt_order_amount')}}</th>
                    <th>{{lang_trans('txt_action')}}</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($orders as $k=>$val)
                  <tr>
                    <td>{{$k+1}}</td>
                    <td>
                      @if($val->order_history)
                      @foreach($val->order_history as $key_OH=>$val_OH)
                      @if($val_OH->orders_items)
                      @foreach($val_OH->orders_items as $key_OI=>$val_OI)
                      @php
                      $price = $val_OI->item_price*$val_OI->item_qty;
                      $totalAmount = $totalAmount+$price;
                      @endphp
                      <span class="spcolnj"> {{$val_OI->item_name}} ({{$val_OI->item_qty}}q) </span><br>
                      @endforeach
                      @endif
                      @endforeach
                      @endif
                    </td>
                    <td>{{$val->table_num}}</td>
                    <td>{{getCurrencySymbol()}} {{@$totalAmmountsArr[$val->id]}}</td>
                    <td>
                      <!--<a class="btn btn-sm btn-success" href="{{--route('food-order-table',[$val->id])--}}">{{--lang_trans('btn_repeat_order')--}}</i></a>-->
                      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".view_modal_{{$k}}">{{lang_trans('btn_view_order')}}</button>
                      <a class="btn btn-sm btn-warning" href="{{route('room-order-final',[$val->id])}}" target="_blank">{{lang_trans('btn_pay')}}</i></a>
                      <a class="btn btn-sm btn-danger edit_food_item" id="" data-id="{{$val->id}}">Edit</i></a>
                      <!-- <a class="btn btn-sm btn-danger" href="{{route('room-order-delete',[$val->id])}}">Delete</i></a> -->
                      <a class="btn btn-sm btn-warning" href="{{route('room-order-cancel',[$val->id])}}">Cancel</i></a>

                      <div class="modal fade view_modal_{{$k}}" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                              <h4 class="modal-title" id="myModalLabel">{{lang_trans('txt_order_details')}}: ({{lang_trans('txt_table_num')}}- #{{$val->table_num}})</h4>
                            </div>
                            <div class="modal-body">
                              <table class="table table-striped table-bordered">
                                <tr>
                                  <th>{{lang_trans('txt_sno')}}</th>
                                  <th>{{lang_trans('txt_datetime')}}</th>
                                  <th>{{lang_trans('txt_orderitem_qty')}}</th>
                                </tr>
                                @if($val->order_history)
                                @foreach($val->order_history as $key_OH=>$val_OH)
                                <tr>
                                  <td>{{$key_OH+1}}</td>
                                  <td>{{$val_OH->created_at}}</td>
                                  <td>
                                    @if($val_OH->orders_items)
                                    <table class="table table-bordered">
                                      @foreach($val_OH->orders_items as $key_OI=>$val_OI)
                                      @php
                                      $price = $val_OI->item_price*$val_OI->item_qty;
                                      $totalAmount = $totalAmount+$price;
                                      @endphp
                                      <tr>
                                        <td>{{$val_OI->item_name}}</td>
                                        <td>{{$val_OI->item_qty}}</td>
                                      </tr>
                                      @endforeach
                                    </table>
                                    @endif
                                  </td>
                                </tr>
                                @endforeach
                                @endif
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>

        </div>
      </div>
    </div>

  </div>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>{{lang_trans('txt_product_stocks')}}</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable_" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>{{lang_trans('txt_sno')}}</th>
                  <th>{{lang_trans('txt_product')}}</th>
                  <th>{{lang_trans('txt_current_stocks')}}</th>
                  <th>{{lang_trans('txt_unit')}}</th>
                </tr>
              </thead>
              <tbody>
                @foreach($products as $k=>$val)
                <tr>
                  <td>{{$k+1}}</td>
                  <td>{{$val->name}}</td>
                  <td>{{$val->stock_qty}}</td>
                  <td>{{($val->unit) ? $val->unit->name : ''}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="alertModal" class="modal12 alert modal">
  <div class="modal-content12">
    <span class="close12">&times;</span>
    <div id="alertText">
      <table>
        <thead>
          <tr>
            <th>id</th>
            <th>Item Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="alertTableBody">
          <!-- Table rows will be added here -->
        </tbody>
      </table>
    </div>
  </div>
</div>



<a class="weatherwidget-io" href="https://forecast7.com/en/28d6677d23/delhi/" data-label_1="DELHI" data-label_2="WEATHER" data-theme="original">DELHI WEATHER</a>

<script>
  ! function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (!d.getElementById(id)) {
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://weatherwidget.io/js/widget.min.js';
      fjs.parentNode.insertBefore(js, fjs);
    }
  }(document, 'script', 'weatherwidget-io-js');
</script>


<div class="container">
  <div class="row justify-content-center">
    <br>
    <div class="col-md-12 jnewwidth">
      @if(count($graphTotalCheckin) > 0)
      <div class="card">
        <div class="card-body">
          <br />
          <div class="container" id="order_graph_checkin"></div>
        </div>
      </div>
      @endif

      @if(count($graphTotalRevenue) > 0)
      <div class="card">
        <div class="card-body">
          <br />
          <div class="container" id="order_graph_revenue"></div>
        </div>
      </div>
      @endif

      @if(count($graphTotalRevenue) > 0)
      <div class="card">
        <div class="card-body">
          <br />
          <div class="container" id="order_aRR"></div>
        </div>
      </div>
      @endif

      @if(count($graphTotalReferredBy) > 0)
      <div class="card">
        <div class="card-body">
          <br />
          <div class="container" id="referred_by_3d"></div>
        </div>
      </div>
      @endif

      @if(count($graphTotalPaymentMode) > 0)
      <div class="card">
        <div class="card-body">
          <br />
          <div class="container" id="payment_mode_3d"></div>
        </div>
      </div>
      @endif


      @if(count($graphTotalPaymentMode_all) > 0)
      <div class="card">
                <div class="card-body">
                    <br/>
                    <div class="container" id="payment_mode_3d_all"></div>
                </div>
            </div>
            @endif

    </div>
  </div>
</div>
<?php
$userid = Auth::user()->role_id;

?>

<?php if ($userid != 5) { ?>
  <input type="hidden" id="role" value="0">

<?php } else { ?>

  <input type="hidden" id="role" value="5">
<?php } ?>

<script>
  $(document).ready(function() {
    $(".close12").click(function() {
      $("#alertModal").hide(); // or use .fadeOut() for a fade-out effect
    });
    $("#alertModal").hide();
    var role4 = $('#role').val();
    if (role4 == 5) {
      // alert(role4);
       

      $('.top_tiles').hide();
      $('.justify-content-center').hide();
      $('.weatherwidget-io').hide();
    }

    if (role4 != 5) {
      // alert(role4);
      $('.kitchen_login').hide();
    }
  });
  $(document).on('click', '.edit_food_item', function() {
    // Get the value of "data-id" attribute from the clicked button
    var id = $(this).data('id');

    // alert(id);

    $.ajax({
      type: 'GET',
      url: "{{route('room-order-edit')}}",
      data: {
        id: id,
        _token: "{{ csrf_token() }}"
      },
      success: function(res) {
        console.log(res);
        var tableBody = $("#alertTableBody");
        tableBody.empty();

        for (var i = 0; i < res.length; i++) {
          var rowData = res[i];

          // Create a new table row
          var row = $('<tr>');

          // Add data to the table cells
          var id = $('<td>').text(rowData.id);
          var item_name = $('<td>').text(rowData.item_name);
          var item_qty = $('<td>').text(rowData.item_qty);
          var item_price = $('<td>').text(rowData.item_price);
          // Add more cells as needed

          var editButton = $('<a>').addClass('btn btn-sm btn-primary')
            .attr('href', '{{route("edit-item")}}/' + rowData.id)
            .text('Edit');

          // Create Delete button with link
          var deleteButton = $('<a>').addClass('btn btn-sm btn-danger')
            .attr('href', '{{route("delete-item")}}/' + rowData.id)
            .text('Delete');

          // Create a cell to hold the buttons
          var actionsCell = $('<td>').append(editButton, ' ', deleteButton);

          // Append the cells to the row
          row.append(id, item_name, item_qty, item_price, actionsCell);

          // Append the row to the table body
          $('#alertTableBody').append(row);
        }
        $("#alertModal").show();
      },
    });


    $("#alertModal").show();
    // code to be executed when the element is clicked
  });

  //     function dirty(id)
  //   {
  //             Swal.fire({
  //               title: 'Are you sure?',
  //               text: "You want to change status to ready",
  //               icon: 'warning',
  //               showCancelButton: true,
  //               confirmButtonColor: '#3085d6',
  //               cancelButtonColor: '#d33',
  //               confirmButtonText: 'Yes',
  //               cancelButtonText: 'No'
  //             }).then((result) => {
  //               if (result.isConfirmed) {

  //                   axios.get('delete-reservation/'+id,{
  //                           _method: 'DELETE',
  //                           headers: {
  //     'Content-type': 'application/json'
  //   },
  //                   })
  //                       .then(function (response) {
  //                         window.location.reload();
  //                       })
  //                       .catch(function (error) {
  //                         // handle error
  //                         console.log(error);
  //                       })
  //                       .then(function () {
  //                         // always executed
  //                       });
  //                 Swal.fire(
  //                   'Status Changed!',
  //                   'Room switched to Ready',
  //                   'success'
  //                 )
  //               }
  //             });
  //   }
</script>

<script>
const dineInButton = document.getElementById('dineInButton');
const popup = document.getElementById('popup');
const closeButton = document.getElementById('closeButton');
const tableDataContainer = document.getElementById('tableData');

dineInButton.addEventListener('click', () => {
    // Make an AJAX request to fetch data based on the selected tableNumber
    $.ajax({
        url: '/get_table_data', // Replace with your route for fetching data
        type: 'GET',
        dataType: 'json', // Expect JSON response
        success: function(response) {
            // Access the data from the JSON response
            const data = response.data;

            // Construct the HTML for the data
            const tableNumHTML = data.map(function(item) {
                return `
                    <small class="table-item">
                        <input type="checkbox" id="table_${item.id}" value="${item.id}">
                        <label for="table_${item.id}">${item.table_num}</label>
                    </small>&nbsp;
                `;
            }).join('');

            data.forEach(function(item) {
           sessionStorage.setItem(`table_num_${item.id}`, item.table_num);
           });
            // Display the HTML inside the tableDataContainer
            tableDataContainer.innerHTML = tableNumHTML;

            // Show the popup
            popup.style.display = 'block';


            
        }
    });
});

closeButton.addEventListener('click', () => {
    popup.style.display = 'none';
});



    // Event listener for opening the popup (you can attach this to a button or another element)
   

</script>


  	<script>

var qr = UPI(10, "rps4161@paytm", "rpsingh", 250);
console.log(qr);
function UPI(amount, merchant_upi, merchant_name, size) {
  if (amount.map) {
    return amount.map(function (amount2) {
      return UPI(amount2, merchant_upi, merchant_name, size);
    });
  }

  const googleChart = 'https://chart.googleapis.com/chart?cht=qr&choe=UTF-8';
  const upiData = "upi://pay?pn=${merchant_name}&pa=${merchant_upi}&am=${amount}";
  return googleChart+"&chs=250x250&chl="+encodeURIComponent(upiData);
}


// var amount = 10;
// var merchant_upi = "rps4161@paytm";
// var size = 250;
// var merchant_name = "rpsingh";

// var qr = UPI(amount, merchant_upi, merchant_name, size);
// console.log(qr);
// function UPI(amount, merchant_upi, merchant_name, size) {
//   if (amount.map) {
//     return amount.map(function (amount2) {
//       return UPI(amount2, merchant_upi, merchant_name, size);
//     });
//   }

//   const googleChart = 'https://chart.googleapis.com/chart?cht=qr&choe=UTF-8';
//   const upiData = "upi;//pay?pn="+merchant_name+"&pa="+merchant_upi+"&am="+amount;
//   return googleChart+"&chs=250x250&chl="+encodeURIComponent(upiData);
// }

</script>
@endsection