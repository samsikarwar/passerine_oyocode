@extends('layouts.master_backend')
@section('content')
<style>
    .colorstatus1{
        color:green;
        font-weight:500;
    }
    .colorstatus{
        color:blue;
        font-weight:500;
    }
</style>
<div class="">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
            <h2>Food Report Search</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            {{ Form::model($search_data,array('url'=>route('search-foodreport'),'id'=>"search-expense", 'class'=>"form-horizontal form-label-left")) }}
              <div class="form-group col-sm-3">
                <label class="control-label"> {{lang_trans('txt_category')}}</label>
                <select class="form-control" name="category_id"><option selected="selected" value="">--Select--</option><option value="All">All</option><option value="ActiveOrders">ActiveOrders</option><option value="ClosedOrders">ClosedOrders</option></select>
                <!-- {{Form::select('category_id',$category_list,null,['class'=>"form-control",'placeholder'=>lang_trans('ph_select')])}} -->
              </div>
              <div class="form-group col-sm-2">
                <label class="control-label"> {{lang_trans('txt_date_from')}}</label>
                {{Form::text('date_from',null,['class'=>"form-control datepicker", 'placeholder'=>lang_trans('ph_date_from')])}}
              </div>
              <div class="form-group col-sm-2">
                <label class="control-label"> {{lang_trans('txt_date_to')}}</label>
                {{Form::text('date_to',null,['class'=>"form-control datepicker", 'placeholder'=>lang_trans('ph_date_to')])}}
              </div>
              <div class="form-group col-sm-3">
                <br/>
                 <button class="btn btn-success search-btn" name="submit_btn" value="search" type="submit">{{lang_trans('btn_search')}}</button>
                 <a class="btn btn-danger search-btn" href="{{route('food-sales-report')}}" name="refresh" value="refresh">Refresh</a>
                 <!--<button class="btn btn-primary export-btn" name="submit_btn" value="export" type="submit">{{--lang_trans('btn_export')--}}</button>-->
              </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
  </div>
  <div class="row">
      <div class="pt-3 pb-3" id="print_area">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
              <div class="x_title">
                  <h2>Food Collection Report</h2>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                  <br/>
                  @php
                    $totalAmount = 0;
                    $totalAmount1 = 0;
                    $totalAmountn = 0;
                    $i= 1;
                  @endphp
                  <table id="datatable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>{{lang_trans('txt_sno')}}</th>
                      <th>Food Name</th>
                      <th>KOT</th>
                      <th>Guest Name</th>
                      <th>Room Number</th>
                     
                      <!--<th>{{--lang_trans('txt_category')--}}</th>-->
                      <!--<th>{{--lang_trans('txt_title')--}}</th>-->
                      <th>{{lang_trans('txt_amount')}}</th>
                      <th>Order Type</th>
                       <th>Payment Mode</th>
                      <th>{{lang_trans('txt_date')}}</th>
                      <!--<th>{{--lang_trans('txt_remark')--}}</th>-->
                      <!--<th>{{--lang_trans('txt_action')--}}</th>-->
                    </tr>
                  </thead>
                  <tbody>
                      
                  <?php
    $orderTotal = 0;
    ?>

                      @foreach($datalist2 as $k=>$val)
                        @php
                            $totalAmount1 = $totalAmount1 + $val->item_price;
                        @endphp
                        <tr>
                            <td>{{$i}}</td>
                            <td>
                            <?php
                    $foodNames = DB::table('order_items')
                        ->where('order_id', $val->id)
                        ->whereDate('order_items.created_at', '>=', dateConvert($search_data['date_from'], 'Y-m-d'))
                        ->whereDate('order_items.created_at', '<=', dateConvert($search_data['date_to'], 'Y-m-d'))
                        ->pluck('item_name')
                        ->implode(',');
                    echo $foodNames;
                ?>
                            </td>
                            <td> <?php
                    $foodNames = DB::table('order_items')
                    ->where('order_id', $val->id)
                    ->whereDate('order_items.created_at', '>=', dateConvert($search_data['date_from'], 'Y-m-d'))
                    ->whereDate('order_items.created_at', '<=', dateConvert($search_data['date_to'], 'Y-m-d'))
                    ->distinct()
                    ->pluck('order_history_id')
                    ->implode(',');
                
                echo $foodNames;
                ?></td>
                            <td><?php
                            $foodNames = DB::table('order_items')
                            ->join('reservations', 'order_items.reservation_id', '=', 'reservations.id')
                            ->join('customers', 'reservations.customer_id', '=', 'customers.id')
                            ->where('order_id', $val->id)
                            ->whereDate('order_items.created_at', '>=', dateConvert($search_data['date_from'], 'Y-m-d'))
                            ->whereDate('order_items.created_at', '<=', dateConvert($search_data['date_to'], 'Y-m-d'))
                            ->pluck('customers.name')
                            ->implode(',');
                        
                        echo $foodNames;
                            ?></td>
                            <td>{{$val->table_num}}</td>
                            
                           

                            <td>
                              
                            <?php
            $order = DB::table('order_items')
            ->where('order_id', $val->id)
            ->whereDate('order_items.created_at', '>=', dateConvert($search_data['date_from'], 'Y-m-d'))
            ->whereDate('order_items.created_at', '<=', dateConvert($search_data['date_to'], 'Y-m-d'))
            ->sum('item_price');
            $orderTotal += $order;
        
           if ($order) {
        echo getCurrencySymbol() . ' ' . $order;
      } 
      ?>

</td>


<td class="colorstatus1">{{$val->statusonoff}}</td>
                            <td>
                                @if($val->payment_mode==1)
                             Cash
                            @elseif($val->payment_mode==4)
                            Paytm
                             @elseif($val->payment_mode==5)
                            Phone pe
                             @elseif($val->payment_mode==6)
                            Complementary
                            
                            @endif
                            </td>
                            <!-- <td>{{dateConvert($val->created_at,'d-m-Y') }}</td> -->
                            <td>
                            <?php
                         $order = DB::table('order_items')
                       ->where('order_id', $val->id)
                       ->whereDate('order_items.created_at', '>=', dateConvert($search_data['date_from'], 'Y-m-d'))
                       ->whereDate('order_items.created_at', '<=', dateConvert($search_data['date_to'], 'Y-m-d'))
                      //  ->sum('item_price');
                      ->first();
                      //  $orderTotal += $order;
        
                       if ($order) {
                        $created_at = $order->created_at;
                        $date = "2023-06-05 18:21:11";
                        $formattedDate = date("d-m-Y", strtotime($created_at));

                        echo $formattedDate; 
                        // echo $created_at;
                       } 
                          ?>

                            </td>


                        </tr>
                        
                        <?php $i++; ?>
                    @endforeach
                
                    @foreach($datalist as $k=>$val)
                      @php
                        $totalAmount = $totalAmount+$val->amount;
                      @endphp
                      <tr>
                        <td>{{$i}}</td>
                        <td>{{$val->name}}</td>
                        <td>{{getCurrencySymbol()}} {{$val->amount}}</td>
                        <td class="colorstatus">{{$val->statusonoff}}</td>
                         <td>{{$val->payment_mode}}</td>
                        <td>{{dateConvert($val->order_date,'d-m-Y')}}</td>
                        <!--<td>-->
                        <!--  <a class="btn btn-sm btn-info" href="{{--route('edit-expense',[$val->id])--}}"><i class="fa fa-pencil"></i></a>-->
                        <!--  <button class="btn btn-danger btn-sm delete_btn" data-url="{{--route('delete-expense',[$val->id])--}}" title="{{--lang_trans('btn_delete')--}}"><i class="fa fa-trash"></i></button>-->
                        <!--</td>-->
                      </tr>
                      <?php $i++; ?>
                    @endforeach
                    
                    
                   <tr> 
                     <th></th>
                      <th class="text-right" width="50%">{{lang_trans('txt_grand_total')}}</th>
                      <th></th>
                      <th></th>
                      <th><?php echo getCurrencySymbol() . ' ' . $orderTotal;?></th>
                      <!-- <th width="50%"><b>{{getCurrencySymbol()}} {{numberFormat($totalAmount) + numberFormat($totalAmount1)}}</b></th> -->
                      
                    </tr>
                  </tbody>
                   
                </table>
                <!--<table class="table table-striped table-bordered">-->
                <!--    <tr>-->
                <!--      <th class="text-right" width="50%">{{lang_trans('txt_grand_total')}}</th>-->
                <!--      <th width="50%"><b>{{getCurrencySymbol()}} {{numberFormat($totalAmount) + numberFormat($totalAmount1)}}</b></th>-->
                <!--    </tr>-->
                <!--</table>-->
              </div>
          </div>
      </div>
    
  </div>
    <div class="col-md-12 pt-3 pb-3 text-right">
                  <button onclick='downloadDiv();' class="btn btn-primary">Download PDF</button>
                  <button onclick="ExportToExcel('xlsx')" class="btn btn-primary">Download Excel</button>
                </div>
  </div>
  
</div>  



@endsection
<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.1/html2pdf.bundle.min.js" ></script>
 <script>
   function ExportToExcel(type, fn, dl) {
            var elt = document.getElementById('datatable');
            var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
            return dl ?
                XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
                XLSX.writeFile(wb, fn || ('foodsalerep.' + (type || 'xlsx')));
        }
  
  function downloadDiv(){
      const element = document.getElementById("print_area");
      html2pdf()
      .from(element)
      .save();
  };

</script>