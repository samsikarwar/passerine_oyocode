<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link rel="stylesheet" href="css/style.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<style>
section.splash-sec {
    background-color: #FA7070;
    height: 100vh;
}

.main-div {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 20px;
}

.pan-div {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 150px;
    
}
.content-div {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 30px,0;
}

.content-div p {
    color: white;
    font-size: 18px;
    font-weight: 700;
    text-align: center;
    margin: 20px,0;
    
}

.order-again-div {
    display: flex;
    justify-content: center;
    align-items: center;
    border: none;
    border-radius: 12px;
    background-color: white;
    width: 152px;
    height: 38px;
    margin: auto;
}
.order-again-div a {
    text-decoration: none;
    color: black;
    font-size: 14px;
    font-weight: 700;
}

.view-div {
    display: flex;
    justify-content: center;
    align-items: center;
    border: none;
    background-color: #fa7070;
    width: 83px;
    margin: auto;
    height: 40px;
}
.view-div a {
    color: white;
    font-size: 14px;
    font-weight: 700;
}

.main-div span.cross-height {
    visibility: hidden;
}

</style>
<body>
    <section class="splash-sec">

        <div class="container">
            <div class="main-div">
                <span class="cross-height">
                    <img src="/public/images/cross12.png">
                </span>
                <span>
                    <img src="/public/images/logo.svg">
                </span><span class="cross">
                    <img scr="/public/images/cross.png">
                </span>
                
            </div>
            <div class="pan-div">
                <img src="/public/images/pan.png">
            </div>
            <div class="content-div">
                <p>We are Preparing Your Food Order<br> Please Wait.</p>
            </div>
            <div class="order-div">
                <div class="order-again-div">
                    <a href="#">Order Again</a>
                </div>
                <div class="view-div">
                    <a href="#">View Order</a>
                </div>
            </div>
        </div>

        
    </section>
</body>
</html>