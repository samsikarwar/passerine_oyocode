
<?php $__env->startSection('content'); ?>


<div class="container">
    <h1>Room Numbers</h1>
    <div>
        <?php $__currentLoopData = $counts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $room): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div><button><?php echo e($room->room_num); ?></button></div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>

 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/backend/rooms/continue_rooms.blade.php ENDPATH**/ ?>