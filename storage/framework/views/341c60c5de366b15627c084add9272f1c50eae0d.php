<?php $__env->startSection('content'); ?>

<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Collection Reportsss</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="post" action="<?php echo e(route('food_collection_report_action')); ?>">
                         <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <label> Range</label>
                                <select name="range" id="range" class="form-control custom-select">
                                    <option value="" selected>-- Select Range --</option>
                                    <option value="1">Yesterday</option>
                                    <option value="2">This Week</option>
                                    <option value="3">Last Month</option>
                                    <option value="4">Month Till Now</option>
                                    <option value="5">Month Till Yesterday</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                               <label> Date</label>
                               <input type="date" class="form-control" value="<?php echo e($start ?? ''); ?>" required name="start" id="start">
                            </div>
                            <div class="col-md-3">
                               <label>End Date</label>
                               <input type="date" class="form-control" value="<?php echo e($end ?? ''); ?>" required name="end" id="end">
                            </div>
                            <div class="col-md-3"><button class="btn btn-info"style="margin-top:25px;">Search</button></div>
                        </div>
                    </form>
                </div> 
                <div class="row">
               
              
                
               
                        
                </div>
                    
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('jquery'); ?>
<script>
    $(document).ready(function() {
    $('#datatable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
            
        ]
    } );
} );
</script>

<script>
$("#range option").eq(localStorage.getItem('range')).prop('selected', false);
$(document).ready(function(){
    // alert('hello');
    if (localStorage.getItem('range')) {
        $("#range option").eq(localStorage.getItem('range')).prop('selected', true);
    }
    $(".custom-select").click(function(){
        var status = $(this).val();
        if(status == '1'){
            var yesterday = moment().subtract(1, 'day').format('YYYY-MM-DD');
            $('#start').val(yesterday);
            $('#end').val(yesterday);
            // var optionsText = this.options[this.selectedIndex].text;
            // alert(optionsText);
            localStorage.setItem('range', $('option:selected', this).index());
        }else if(status == '2'){
            var startOfWeek = moment().startOf('week').format('YYYY-MM-DD');
            var endOfWeek   = moment().endOf('week').format('YYYY-MM-DD');
            $('#start').val(startOfWeek);
            $('#end').val(endOfWeek);
            // alert(startOfWeek + endOfWeek);
            localStorage.setItem('range', $('option:selected', this).index());
        }else if(status == '3'){
            var laststartOfMonth = moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD');
            var lastendOfMonth = moment().subtract(1,'months').endOf('month').format('YYYY-MM-DD');
            // var startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
            // var endOfMonth   = moment().endOf('month').format('YYYY-MM-DD');
            $('#start').val(laststartOfMonth);
            $('#end').val(lastendOfMonth);
            // alert(startOfMonth + endOfMonth);
            localStorage.setItem('range', $('option:selected', this).index());
        }else if(status == '4'){
            var startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
            var today = moment().format('YYYY-MM-DD');
            $('#start').val(startOfMonth);
            $('#end').val(today);
            // alert(startOfMonth + today);
            localStorage.setItem('range', $('option:selected', this).index());
        }else if(status == '5'){
            var startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
            var yesterday = moment().subtract(1, 'day').format('YYYY-MM-DD');
            $('#start').val(startOfMonth);
            $('#end').val(yesterday);
            // alert(startOfMonth + yesterday);
            localStorage.setItem('range', $('option:selected', this).index());
        }else{
            $('#start').val('');
            $('#end').val('');
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar_oyo\lajpatnagar-de96beb8549e44bc1425ca3a2e0b268401376b9f\lajpatnagar-de96beb8549e44bc1425ca3a2e0b268401376b9f\resources\views/backend/sell_collection_report.blade.php ENDPATH**/ ?>