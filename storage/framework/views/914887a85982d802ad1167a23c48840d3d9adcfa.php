
<?php $__env->startSection('content'); ?>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?php echo e(lang_trans('heading_manage_inventory')); ?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo e(Form::open(array('url'=>route('save-stock'),'id'=>"stock-form", 'class'=>"form-horizontal form-label-left"))); ?>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo e(lang_trans('txt_product')); ?> <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="form-control" name="product_id">
                            <option value="">--Select--</option>
                                <?php $__currentLoopData = $product_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($product->id); ?>"><?php echo e($product->hk); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo e(lang_trans('txt_stock')); ?> <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php echo e(Form::select('stock_is',['add'=>'Add','subtract'=>'Subtract'],1,['class'=>'form-control','id'=>'stock_is','placeholder'=>lang_trans('ph_select')])); ?>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty"><?php echo e(lang_trans('txt_qty')); ?> <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php echo e(Form::number('qty',null,['class'=>"form-control col-md-7 col-xs-12", "id"=>"qty", "required"=>"required"])); ?>

                        </div>
                    </div>
                    <div class="form-group hide_elem" id="price_section">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price"><?php echo e(lang_trans('txt_price')); ?></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php echo e(Form::text('price',null,['class'=>"form-control col-md-7 col-xs-12", "id"=>"price"])); ?>

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-primary" type="reset"><?php echo e(lang_trans('btn_reset')); ?></button>
                            <button class="btn btn-success" type="submit"><?php echo e(lang_trans('btn_submit')); ?></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('public/js/page_js/page.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/backend/stock_in_out_hk.blade.php ENDPATH**/ ?>