<?php $__env->startSection('content'); ?>
<style type="text/css">
   #results {
      border: 1px solid;
      background: #ccc;
   }

   .results_guest {
      border: 1px solid;
      background: #ccc;
   }
</style>

<script>
$(document).on('focus',".date", function(){ //bind to all instances of class "date".
   $(this).datepicker({
      dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true,
      yearRange: "-50:+0"
   });
});
   $(function() {
      $(".datePickerDefault").datepicker({
         dateFormat: 'yy-mm-dd',
         minDate: 0

      });
      $(".datePickerDefault1").datepicker({
         dateFormat: 'dd-mm-yy',
         changeMonth: true,
          changeYear: true,
          yearRange: "-90:+0"
      });
   });
</script>
<div class="">

   <?php echo e(Form::open(array('url'=>route('save-banquet'),'class'=>"form-horizontal form-label-left",'files'=>true))); ?>


   <div class="row hide_elem" id="existing_guest_section">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <script type="text/javascript">
               var path = "<?php echo e(route('autocomplete')); ?>";
               $('input.typeahead').typeahead({
                  source: function(query, process) {
                     return $.get(path, {
                        query: query
                     }, function(data) {
                        console.log(data);
                        console.log(process.id);
                        console.log(data.name);
                        return process(data);
                     });
                  }
               });
            </script>
         </div>
      </div>
   </div>
   <div class="row" id="new_guest_section">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h2>Banquet Invoice</h2>
               <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Booking ID <span ></span></label>-->
                    <!--    -->
                    <!--</div>-->
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> And Number </label>-->
                    <!--    -->
                    <!--</div>-->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Name <span class="required">*</span></label>
                        <?php echo e(Form::text('name',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"name", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_fullname'), 'required'])); ?>

                    </div>
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Address <span class="required">*</span></label>
                        <input type="text" id="address" class="form-control col-md-6 col-xs-12" placeholder="Address" required name="address">
                        <!--<?php echo e(Form::text('address',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"address", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_address'), 'required'])); ?>-->
                    </div>
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> <?php echo e(lang_trans('txt_email')); ?> </label>-->
                    <!--    -->
                    <!--</div>-->
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> <?php echo e(lang_trans('txt_mobile_num')); ?> <span class="required">*</span></label>
                        <?php echo e(Form::text('mobile',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"mobile","maxlength"=>"10", "placeholder"=>lang_trans('ph_enter').lang_trans('txt_mobile_num'), 'required'])); ?>

                    </div>

                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> <?php echo e(lang_trans('txt_gender')); ?> <span class="required">*</span></label>-->
                    <!--    -->
                    <!--</div>-->
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> <?php echo e(lang_trans('txt_age')); ?> </label>-->
                    <!--    -->
                    <!--</div>-->
                </div>
                <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    <?php echo e(Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])); ?>-->
                    <!--</div>-->
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Date of Party <span class="required">*</span></label>
                        <!--<input type="datetime-local" id="meeting-time" class="form-control col-md-6 col-xs-12" name="date_time">-->
                        <?php echo e(Form::date('date',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"date", "placeholder"=>"Date", 'required'])); ?>

                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Time <span class="required">*</span></label>
                        <input type="time" id="time" class="form-control col-md-6 col-xs-12" required name="time">
                        <!--<?php echo e(Form::time('time',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"time", "placeholder"=>"Date", 'required'])); ?>-->
                    </div>
                </div>
                
                 <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    <?php echo e(Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])); ?>-->
                    <!--</div>-->
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Type of Party <span class="required">*</span></label>
                        <input type="text" id="type_of_party" class="form-control col-md-6 col-xs-12" placeholder="Type of Party"required name="type_of_party">
                        <!--<?php echo e(Form::text('party',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"type_of_party", "placeholder"=>"Date", 'required'])); ?>-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Food Pax <span class="required">*</span></label>
                        <input type="text" id="food_pax" class="form-control col-md-6 col-xs-12"Placeholder="Food Pax" required name="food_pax">
                        <!--<?php echo e(Form::text('time',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"food_pax", "placeholder"=>"Date", 'required'])); ?>-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Rate <span class="required">*</span></label>
                        <input type="text" id="rate" class="form-control col-md-6 col-xs-12" Placeholder="rate" name="rate">
                        <!--<?php echo e(Form::text('rate',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"rate", "placeholder"=>"Rate", 'required'])); ?>-->
                    </div>
                </div>
                
                <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    <?php echo e(Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])); ?>-->
                    <!--</div>-->
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Banquet Hall Charges <span class="required">*</span></label>
                        <input type="text" id="hall_charges" class="form-control col-md-6 col-xs-12"placeholder="Hall Charges" required name="hall_charges">
                        <!--<?php echo e(Form::text('hall_charges',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"hall_charges", "placeholder"=>"Date", 'required'])); ?>-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Room Charges <span class="required">*</span></label>
                        <input type="text" id="room_charges" class="form-control col-md-6 col-xs-12" Placeholder="Room Charges" required name="room_charges">
                        <!--<?php echo e(Form::text('room_charges',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"room_charges", "placeholder"=>"Date", 'required'])); ?>-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Other charges <span class="required">*</span></label>
                        <input type="text" id="other_charges" class="form-control col-md-6 col-xs-12"Placeholder="other_charges" name="other_charges">
                        <!--<?php echo e(Form::text('other_charges',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"other_charges", "placeholder"=>"Rate", 'required'])); ?>-->
                    </div>
                </div>
                
                 <div class="row">
                    <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                    <!--    <label class="control-label"> Particulars <span class="required">*</span></label>-->
                    <!--    <?php echo e(Form::text('title',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"title", "placeholder"=>"Title", 'required'])); ?>-->
                    <!--</div>-->
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Total Amount <span class="required">*</span></label>
                        <input type="text" id="total_amount" class="form-control col-md-6 col-xs-12" placeholder="Total" required name="total_amount">
                        <input type="hidden"id="total"val="">
                        <!--<?php echo e(Form::text('total_amount',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"total_amount", "placeholder"=>"Date", 'required'])); ?>-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Advance <span class="required">*</span></label>
                        <!--<input type="datetime-local" id="meeting-time" class="form-control col-md-6 col-xs-12" name="date_time">-->
                        <input type="text" id="advance" class="form-control col-md-6 col-xs-12" placeholder="advance" required name="advance">
                        <!--<?php echo e(Form::text('advance',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"advance", "placeholder"=>"Date", 'required'])); ?>-->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label"> Balance <span class="required">*</span></label>
                         <input type="text" id="balance" class="form-control col-md-6 col-xs-12" placeholder="balance" required name="balance">
                        <!--<?php echo e(Form::text('balance',null,['class'=>"form-control col-md-6 col-xs-12", "id"=>"balance", "placeholder"=>"Rate", 'required'])); ?>-->
                    </div>
                </div>
                
               
                
                
            </div>
         </div>
      </div>
   </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                
                <div class="x_content">
                    <div class="row">
                       
                    </div>
                </div>
                <div class="col-md-10 col-sm-12 col-xs-12">
                    <button class="btn btn-success btn-submit-form2" type="submit"  id="arrivals_btn" style="float:right;"><?php echo e(lang_trans('btn_submit')); ?></button>
                </div>
            </div>
        </div>
        
    </div>
   </div>

    <?php echo e(Form::close()); ?>

</div>



<script>

  
//   $('body').on('keyup', '#hall_charges', function() {
//          var hall_charges =  $(this).val();
//          $("#total_amount").val(hall_charges);
//          $("#total").val(hall_charges);
//         //  alert(room_charges);
//   // code to execute when the key is released
// });   
// $('body').on('keyup', '#room_charges', function() {
//          var room_charges =  $(this).val();
//       var total =  $("#total").val();
//       var sum = (parseInt(room_charges)+(parseInt(total)));
//         $("#total_amount").val(sum);
//     //   alert(sum);
       
//         //  alert(total);
//   // code to execute when the key is released
// });
  

  

    globalVar.page = 'room_reservation_add';
</script>
<script type="text/javascript" src="<?php echo e(URL::asset('public/js/page_js/page.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('public/js/custom.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/passerine/public_html/lajpatnagar.f9hotels.com/new/resources/views/banquetinvoices/add_edit_performainvoice.blade.php ENDPATH**/ ?>