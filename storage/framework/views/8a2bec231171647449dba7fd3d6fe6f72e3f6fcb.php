<?php if(session('success')): ?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <?php echo e(session('success')); ?>

    </div>
<?php elseif(session('error')): ?>
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <?php echo e(session('error')); ?>

    </div>
<?php elseif(session('info')): ?>
    <div class="alert alert-warning alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <?php echo e(session('info')); ?>

    </div>
<?php endif; ?>
<?php /**PATH E:\passerine oyocode\lajpatnagar-de96beb8549e44bc1425ca3a2e0b268401376b9f\resources\views/layouts/flash_msg.blade.php ENDPATH**/ ?>