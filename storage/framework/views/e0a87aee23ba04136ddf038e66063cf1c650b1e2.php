<?php 
  $settings = getSettings();
  $totalOrdersAmount = 0;
 
?>
<!DOCTYPE html>
<html lang="en">
     <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <title><?php echo e($settings['site_page_title']); ?>: <?php echo e(lang_trans('txt_invoice')); ?></title>
        <link href="<?php echo e(URL::asset('public/assets/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(URL::asset('public/css/invoice_style.css')); ?>" rel="stylesheet">
    </head>
    <style>
.paysta1{
                background: black;
                color: white;
                padding: 6px;
                font-size: 15px;
            } 
            .paid1cls{
                background: green;
                padding: 6px;
                color: white;
                font-size: 15px;
            } 
            .paysta2{
                background: black;
                color: white;
                padding: 6px;
                font-size: 14px;
            } 
            .notpaid2cls{
                background: green;
                padding: 6px;
                color: white;
                font-size: 14px;
            }
    </style>
    <body>

    <?php             
            
            $i = 0;
            $totalOrdersAmount = 0;
            $itemsQty = [];
            $orderedItemsArr = [];
            if($data_row->orders_items){
                foreach($data_row->orders_items as $k=>$val){
                    $jsonData = json_decode($val->json_data);
                    $itemId = $jsonData->item_id;
        
                    if(isset($itemsQty[$itemId])){
                        $itemsQty[$itemId] = $itemsQty[$itemId]+$val->item_qty;
                    } else {
                        $itemsQty[$itemId] = $val->item_qty;
                    }
              
                    $orderedItemsArr[$itemId] = [
                        'id'=>$val->id,
                        'order_history_id'=>$val->order_history_id,
                        'item_name'=>$val->item_name,
                        'item_qty'=>$itemsQty[$itemId],
                        'item_price'=>$val->item_price,
                        'amount'=>$itemsQty[$itemId]*$val->item_price,
                        'created_at'=>dateConvert($val->created_at,'d-m-Y'),
                    ];
                }
            }
        ?>


    <div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    <?php echo e($settings['hotel_name']); ?>, <?php echo e($settings['hotel_tagline']); ?>

                </font>
            </label>
        </div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    <?php echo e($settings['hotel_address']); ?>

                </font>
            </label>
        </div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    <?php echo e($settings['hotel_email']); ?>

                </font>
            </label>
        </div>
        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">
                <font size="2">
                    <!-- <?php echo e(lang_trans('txt_website')); ?>: <?php echo e($settings['hotel_website']); ?> -->
                </font>
            </label>
        </div>
    
        <div class="class-inv-15" style="margin-left:741px;">
                        <?php
                            $isBook = json_decode($data_row);
                            $isbookStatus = $isBook->order_history[0]->is_book;
                        ?>
                        <?php if($isbookStatus == 0): ?>
                        <span class="paysta1">Status:</span>
                        <span class="paid1cls">Paid</span>
                        <?php else: ?>
                        <span class="paysta2">Status:</span>
                        <span class="notpaid2cls">Not Paid</span>
                        <?php endif; ?>
                    </div>


        <div align="center" class="col-md-12 col-sm-12 col-xs-12">
            <table border="0" border-style="ridge" class="class-inv-21">
                <tr>
                    <td align="left" width="100px">
                        <div>
                          <h5>  <?php echo e(lang_trans('txt_gstin')); ?> </h5>
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                          <h5>  <?php echo e($settings['gst_num']); ?> </h5>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="100px">
                        <div>
                    <h5>  Phone No </h5>
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                       <h5> <?php echo e($settings['hotel_mobile']); ?> </h5>
                        </div>
                    </td>
                </tr>
              
                <tr>
                    <td align="left" width="100px">
                        <div>
                    <h5>Customer Name </h5>
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                       <h5> <?php echo e($data_row->name); ?> </h5>
                        </div>
                    </td>
                </tr>
               
                <tr>
                    <td align="left" width="100px">
                        <div>
                            
                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                            
                        </div>
                    </td>
                </tr>

                <tr>
                    <td align="left" width="100px">
                        <div>
                        <h5> Room - <?php echo e($data_row3->room_num_switch ?: $data_row->table_num); ?></h5>

                        </div>
                    </td>
                    <td class="txt-right" width="150px">
                        <div>
                       <!-- <h5>Tocken No <?php echo e(implode(', ', $order)); ?> </h5> -->
                        </div>
                    </td>
                </tr>
                <tr>
                <td class="txt-left" width="150px">
                        <div>
                        <h5>ORD# <?php echo e($data_row->id); ?> </h5>
                        </div>
                    </td>
                
                <td class="txt-right" width="150px">
                        <div>
                        <h5 style=""><?php echo e(dateConvert($data_row->invoice_date,'d-m-Y')); ?></h5>
                        </div>
                    </td>
                </tr>

            </table>
            <h5>Original Due</h5>
            <!-- <h5>Due</h5> -->
            

            <table border="0" border-style="ridge" class="class-inv-21">
           
</table>
            <!-- <div class="row" style=""> -->
                <!-- <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style=""> -->
                    <!-- <h5>Room - <?php echo e($data_row->table_num); ?></h5>
                    <h5>ORD  #<?php echo e($data_row->id); ?></h5>
                    <h5>Tocken No <?php echo e(implode(', ', $order)); ?> </h5> -->
                <!-- </div> -->
                <!-- <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style=""> -->
                    <!-- <h5 style=""><?php echo e(dateConvert($data_row->invoice_date,'d-m-Y')); ?></h5> -->
                <!-- </div> -->
            <!-- </div> -->

            <table border="1" border-style="ridge" style="font-size: 80%">
            <tbody>
                        <tr>
                            <td colspan="5" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <div class="" style="justify-content: space-between;">
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="">Token No</h5>
                                    </div>

                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style=""> Item Details </h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="text-align: center;"> Rate </h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="text-align: center;"> Qty </h5>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: right;"class="gr" > Amount </h5>
                                    </div>
                                </div>
                            </td>
                        </tr>    
                        <?php $__empty_1 = true; $__currentLoopData = $orderedItemsArr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                          <?php
                            $itemcountall = count($orderedItemsArr) ?? '0';
                            $totalOrdersAmount = $totalOrdersAmount + ($val['item_qty']*$val['item_price']);
                          ?>
                          
                        <tr>
                            <td colspan="5" style="border: none;">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <h5 style="">
                                    <?php
// $foodNames = DB::table('order_items')
//                 ->where('order_id', $data_row->id)
//                 ->orderBy('id', 'asc')
//                 ->pluck('id');

// foreach ($foodNames as $foodName) {
//     echo $foodName;
//     echo '<br>';
// }
?>
                                    
                                    
                                    <?php echo e($val['order_history_id']); ?> 
                                </h5>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style=""> <?php echo e($val['item_name']); ?> </h5>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <h5 style="text-align: center;"> <?php echo e($val['item_price']); ?> </h5>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <h5 style="text-align: center;"> <?php echo e($val['item_qty']); ?> </h5>
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style="text-align: right;"> <?php echo e(getCurrencySymbol()); ?> <?php echo e(numberFormat($val['item_qty']*$val['item_price'])); ?> </h5>
                                </div>
                            </td>
                        </tr> 
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <tr>
                            <td colspan="5">
                                <?php echo e(lang_trans('txt_no_orders')); ?>

                            </td>
                        </tr>
                        <?php endif; ?>
                        
                        <?php
                
                            $gstPerc = $cgstPerc = $discount = 0;
                            if($data_row->gst_apply==1){
                                $gstPerc = $data_row->gst_perc;
                                $cgstPerc = $data_row->cgst_perc;
                            }
                            $discount = ($data_row->discount>0) ? $data_row->discount : 0;
                            //$gst = gstCalc($totalOrdersAmount,'food_amount',$gstPerc,$cgstPerc);
                            $foodAmountGst = numberFormat($totalOrdersAmount*$gstPerc/100);
                            $foodAmountCGst = numberFormat($totalOrdersAmount*$cgstPerc/100);
                            
                        ?>
                        
                        <?php 
                            $finalFoodAmount = numberFormat($totalOrdersAmount+$foodAmountGst+$foodAmountCGst-$discount);
                        ?>

                        <tr>
                            <td colspan="5" style="border-top: 1px solid black;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <h5 style=""> Subtotal (<?php echo e($itemcountall); ?> items) </h5>
                                       
                                    </div>
                                    <!-- <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div> -->
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <h5 style="text-align: center;"> <?php echo e($itemcountall); ?> </h5>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <h5 style="text-align: right;"class="sa"> <?php echo e(getCurrencySymbol()); ?> <?php echo e(numberFormat($totalOrdersAmount)); ?> </h5>
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" style="border: none;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h5 style=""> CGST (2.5%)</h5>
                                       
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <!-- <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;"> <?php echo e($itemcountall); ?> </h5>
                                    </div> -->
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style="text-align: right;"><?php echo e(getCurrencySymbol()); ?> <?php echo e(numberFormat(($totalOrdersAmount/(1+(5/100)))*(2.5/100))); ?></h5>
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" style="border: none;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                    <h5 style=""> SGST (2.5%)</h5>
                                       
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <!-- <h5 style="text-align: center;"> <?php echo e($itemcountall); ?> </h5> -->
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style="text-align: right;"><?php echo e(getCurrencySymbol()); ?> <?php echo e(numberFormat(($totalOrdersAmount/(1+(5/100)))*(2.5/100))); ?></h5>
                                       
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="5" style="border: none;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <h5 style=""> S/C(<?php echo e($food_service_charges); ?>%)</h5>
                                       
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                        <!-- <h5 style="text-align: center;"> <?php echo e($itemcountall); ?> </h5> -->
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <h5 style="text-align: right;"><?php echo e(getCurrencySymbol()); ?> <?php echo e(numberFormat(($totalOrdersAmount*$food_service_charges/100))); ?></h5>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <?php if($foodAmountGst>0): ?>
                       
                        <?php endif; ?>
                        
                        <?php if($discount>0): ?>
                            <tr>
                                <td colspan="5" style="border: none;">
                                    <div class="" style="justify-content: space-between;">
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <h5 style=""> <?php echo e(lang_trans('txt_discount')); ?> </h5>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <h5 style="text-align: center;">  </h5>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <h5 style="text-align: center;"> </h5>
                                        </div>
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                            <h5 style="text-align: right;"> <?php echo e(getCurrencySymbol()); ?> <?php echo e(numberFormat($discount)); ?> </h5>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
            
                        <?php 
                            $finalFoodAmount = numberFormat($totalOrdersAmount+$foodAmountGst+$foodAmountCGst-$discount);
                        ?>
                        
                        <tr>
                            <td colspan="5" style="border-top: 2px solid black;">
                                <div class="" style="justify-content: space-between;">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <h5 style=""> Payble Amount </h5>
                                    </div>
                                    <!-- <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;">  </h5>
                                    </div>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <h5 style="text-align: center;"> </h5>
                                    </div> -->
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <h5 style="text-align: right;"> <?php echo e(getCurrencySymbol()); ?> <?php echo e($finalFoodAmount + numberFormat(($totalOrdersAmount*$food_service_charges/100))); ?> </h5>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        
                    </tbody>

            </table>
            <!-- <h4> demo </h4> -->
             <button class="btn btn-sm btn-success no-print" onclick="printSlip()">
                <?php echo e(lang_trans('btn_print')); ?>

            </button>
         <a class="btn btn-sm btn-danger no-print" href="<?php echo e(route('new-dashboard')); ?>" id="back-btn">
                <?php echo e(lang_trans('btn_go_back')); ?>

            </a> 
        </div>
    </div>
    <script type="text/javascript" src="<?php echo e(URL::asset('public/js/page_js/page.js')); ?>"></script> 
</body>
</html><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/backend/room_food_order_final_invoice.blade.php ENDPATH**/ ?>