
<?php $__env->startSection('content'); ?>

<style>
    .form-container{
        margin-top: 61px;
        margin-left: 261px;
    }
        .gold-info {
            display: flex; 
          
        }
        table {
        border-collapse: collapse; 
        width: 100%;
    }

    th {
        padding: 10px;
        background-color: #f2f2f2;
    }

    td {
        padding: 5px;
    }
</style>




    
<!-- <div style="margin-left:261px;"> -->

    <h2>User Transaction History</h2>
    <table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Quantity</th>
            <th>Order ID</th>
            <th>Transaction ID</th>
            <th>Total Amount</th>
            <th>CustomerRefNo</th>
            <th>Hotel Name</th>
            <th>Execution Date Time</th>
        </tr>
    </thead>
    <tbody>
            <?php $__currentLoopData = $transactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($transaction['id']); ?></td>
                    <td><?php echo e($transaction['quantity']); ?></td>
                    <td><?php echo e($transaction['orderId']); ?></td>
                    <td><?php echo e($transaction['transactionId']); ?></td>
                    <td><?php echo e($transaction['totalAmount']); ?></td>
                    <td><?php echo e($transaction['customerRefNo']); ?></td>
                    <td><?php echo e($transaction['txnfrom']); ?></td>
                   
                    <td><?php echo e($transaction['executionDateTime']); ?></td>
                    <!-- Add more fields as needed -->
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
</table>
 <!-- </div> -->
                    
 <!-- "id":360,
         "quantity":"0.0087",
         "user_id":800,
         "pricePerG":"6685.59",
         "taxAmount":"0",
         "orderId":"4410090d-6af0-44ed-a322-a282918c430a",
         "transactionId":"4410090d-6af0-44ed-a322-a282918c430a",
         "netAmount":"60",
         "totalAmount":"60",
         "invoiceId":"",
         "customerRefNo":"RSEGOLDBCK1710420494",
         "executionDateTime":"2024-03-15 12:58:54",
         "taxPercent":"0",
         "remainingBalQuantity":"0",
         "billingAddressId":"",
         "created_at":"2024-03-15T07:28:54.000000Z",
         "updated_at":"2024-03-15T07:28:54.000000Z",
         "response":"{\"metalType\":\"gold\",\"quantity\":\"0.0087\",\"goldBalance\":\"2.0709\",\"receiverGoldBalance\":\"0.0261\",\"transactionId\":\"TG937217104877348750200060\",\"merchantTransactionId\":\"4410090d-6af0-44ed-a322-a282918c430a\",\"createdAt\":\"2024-03-15T07:28:54.000000Z\"}",
         "quote_id":"7lKAwdjq",
         "source":"Transfer",
         "payment_id":"",
         "is_hold_qty":0,
         "txnfrom":"Imperial Villa"             
                 -->
                 


<?php $__env->stopSection(); ?>
<?php $__env->startSection('jquery'); ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<scrit>

</script>
<script>
    $(document).ready(function() {
        var settings = {
  "url": "http://dev.goldbck.com/api/v1/mmtc-portfolio/IMPGOLDBCK1695634474",
  "method": "GET",
  "timeout": 0,
};

$.ajax(settings).done(function (response) {
  console.log(response);

  var availableGold = response.qty;
  var goldAmount = response.totalAmount;

 
  $('#availableGold').text('Available Gold: ' + availableGold);
  $('#goldAmount').text('Gold Amount:  Rs  ' + goldAmount);

 var tableBody = document.getElementById("goldTransferTableBody");


var mmtcGoldTransfer = response.mmtcGoldTransfer;


var increment = 1;


for (var i = 0; i < mmtcGoldTransfer.length; i++) {
    var transfer = mmtcGoldTransfer[i];

   
    var newRow = document.createElement("tr");

   
    var idCell = document.createElement("td");
    idCell.textContent = increment++; 

   
    var qtyCell = document.createElement("td");
    qtyCell.textContent = transfer.qty;

    var orderIdCell = document.createElement("td");
    orderIdCell.textContent = transfer.orderId;
    
    var customer_ref_noCell = document.createElement("td");
    customer_ref_noCell.textContent = transfer.customer_ref_no;

    var executionDateTimeCell = document.createElement("td");
    executionDateTimeCell.textContent = transfer.executionDateTime;

   
    newRow.appendChild(idCell);
    newRow.appendChild(qtyCell);
    newRow.appendChild(orderIdCell);
    newRow.appendChild(customer_ref_noCell);
    newRow.appendChild(executionDateTimeCell);

    
    tableBody.appendChild(newRow);
}
});



    $('#datatable').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
            
        ]
    } );
} );
</script>

<script>



</script>

<script>
        $(document).ready(function() {
            // Capture keyup event on gold_amount input
            $('#gold_amount').on('keyup', function() {
                // Get the value from gold_amount input
                var amount = $(this).val();
                var gram = $('#getgram').val();

                        //   alert(gram);
                // Perform your calculation here, for example, let's assume 1 unit of gold for every 10 units of amount
                var grams = (amount / gram);
// alert(grams);
                // Update the value of gold_grams input
                $('#gold_grams').val(grams.toFixed(5)); // Adjust the decimal places as needed
            });
        });
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/gold/user_transaction.blade.php ENDPATH**/ ?>