
<?php $__env->startSection('content'); ?>

 <div class="container">
 <form action="submit-add-bar-rate" method="POST">
 <?php echo csrf_field(); ?>
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label for="bar_rate">Bar Rate:</label>
        <input type="text" class="form-control" id="bar_rate" name="bar_rate" placeholder="Bar Rate" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="gm_name">GM Name:</label>
        <input type="text" class="form-control" id="gm_name" name="gm_name" placeholder="GM Name">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="gm_phone">GM Phone Number:</label>
        <input type="number" class="form-control" id="gm_phone" name="gm_phone" placeholder="GM Phone Number">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="manager_name">Manager Name:</label>
        <input type="text" class="form-control" id="manager_name" name="manager_name" placeholder="Manager Name">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="manager_phone">Manager Phone Number:</label>
        <input type="number" class="form-control" id="manager_phone" name="manager_phone" placeholder="Manager Phone Number">
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="receptionist_phone">Receptionist Phone Number:</label>
        <input type="number" class="form-control" id="receptionist_phone" name="receptionist_phone" placeholder="Receptionist Phone Number">
      </div>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/backend/add_bar_rate.blade.php ENDPATH**/ ?>