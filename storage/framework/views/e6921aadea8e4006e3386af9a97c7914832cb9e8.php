

<?php $__env->startSection('content'); ?>
<style>
    .form-container {
        margin-top: 61px;
        margin-left: 261px;
    }

    .gold-info {
        display: flex;
    }

    .table-container {
        max-height: 400px; /* Adjust this value based on your requirements */
        overflow: auto;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        padding: 10px;
        background-color: #f2f2f2;
    }

    td {
        padding: 5px;
    }
</style>
<div id="messageContainer"style="display:none;">Updated Successfully</div>

<form action="" method="POST">
    <?php echo csrf_field(); ?>
    <select name="month_year" id="month_year">
        <?php for($i = 0; $i < 12; $i++): ?>
        <?php
        $timestamp = strtotime(date('Y-m-01') . " +$i month");
        $formatted_date = date('F Y', $timestamp);
        ?>
        <option value="<?php echo e(date('Y-m', $timestamp)); ?>"><?php echo e($formatted_date); ?></option>
        <?php endfor; ?>
    </select>
    <!-- <button type="submit">Submit</button> -->
</form>
<!-- <button id="submitButton" type="button">Update price</button> -->
<button id="submitButton" type="button" style="
    /* margin-right: 10px; */
    margin-left: 200px;
">Update price</button>

<div class="table-container">
    <table border="1">
        <thead id="tableHeader">
            <tr>
                <th>Room</th>
                <th>Currency</th>
            </tr>
        </thead>
        <tbody id="tableBody">
        </tbody>
    </table>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('jquery'); ?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    // $(document).ready(function() {
    //     function daysInMonth(month, year) {
    //         return new Date(year, month, 0).getDate();
    //     }

    //     function generateTableHeader(selectedMonthYear) {
    //         var [year, month] = selectedMonthYear.split('-');

    //         // Clear existing table header
    //         $('#tableHeader').empty();

    //         // Add static headers
    //         $('#tableHeader').append('<tr><th>Room</th><th>Currency</th></tr>');

    //         // Add dynamic date headers
    //         var numDays = daysInMonth(month, year);
    //         for (var i = 1; i <= numDays; i++) {
    //             var date = new Date(year, month - 1, i);
    //             var dayName = date.toLocaleString('en-us', { weekday: 'short' });
    //             var dayNumber = date.getDate();
    //             var monthName = date.toLocaleString('en-us', { month: 'short' }); // Get month name
    //             $('#tableHeader tr').append('<th>' + monthName + ' ' + dayNumber + ' ' + dayName + '</th>');
    //         }
    //     }

    //     // Initial table header generation
    //     generateTableHeader($('#month_year').val());

    //     // Change event listener for month selection
    //     $('#month_year').change(function() {
    //         var selectedMonthYear = $(this).val();
    //         generateTableHeader(selectedMonthYear);
    //         // Fetch data for all rooms from the database via AJAX
    //         fetchAllRooms(selectedMonthYear);
    //     });

    //     function fetchAllRooms(selectedMonthYear) {
    //         $.ajax({
    //             url: "<?php echo e(route('get_room_type')); ?>",
    //             method: 'GET',
    //             data: {
    //                 month_year: selectedMonthYear
    //             },
    //             success: function(data) {
    //                     // Clear existing table body
    //                     $('#tableBody').empty();
    //                     // Append new data to table body
    //                     data.forEach(function(room) {
    //                         $('#tableBody').append('<tr><td>' + room.title + ' ' + room.occupancy + ' ' + room.meal_plan + '</td><td>Rs</td></tr>');
    //                     });
    //                 },
    //             error: function(xhr, status, error) {
    //                 console.error(error);
    //             }
    //         });
    //     }
    // });
    $(document).ready(function() {
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }

    function generateTableHeader(selectedMonthYear) {
    var [year, month] = selectedMonthYear.split('-');
    var currentDate = new Date();
    var currentMonth = currentDate.getMonth() + 1; // getMonth() returns zero-based index

    // If the selected month is the current month, start from today's date
    var startDay = (month == currentMonth) ? currentDate.getDate() : 1;

    // Clear existing table header
    $('#tableHeader').empty();

    // Add static headers
    $('#tableHeader').append('<tr><th>Room</th><th>Currency</th></tr>');

    // Add dynamic date headers
    for (var i = startDay; i <= daysInMonth(month, year); i++) {
        var date = new Date(year, month - 1, i);
        var dayName = date.toLocaleString('en-us', { weekday: 'short' });
        var dayNumber = date.getDate();
        var monthName = date.toLocaleString('en-us', { month: 'short' });
        $('#tableHeader tr').append('<th data-date="' + year + '-' + month + '-' + i + '">' + monthName + ' ' + dayNumber + ' ' + dayName + '</th>');
    }
}


    // Initial table header generation
    generateTableHeader($('#month_year').val());

    // Change event listener for month selection
    $('#month_year').change(function() {
        var selectedMonthYear = $(this).val();
        generateTableHeader(selectedMonthYear);
        // Fetch data for all rooms from the database via AJAX
        fetchAllRooms(selectedMonthYear);
    });

    function fetchAllRooms(selectedMonthYear) {
    $.ajax({
        url: "<?php echo e(route('get_room_type')); ?>", // Assuming 'get_room_type' is the route name
        method: 'GET',
        success: function(response) {
            console.log(response);
            var roomTypes = response.roomTypes;
            var mealPlans = response.mealPlans;
            var oyodata =response.oyodata;

            // Clear existing table body
            $('#tableBody').empty();

            // Append new data to table body
            roomTypes.forEach(function(room) {
        ['SINGLE', 'DOUBLE', 'TRIPLE'].forEach(function(occupancy) {
          
        mealPlans.forEach(function(mealPlan) {
            var roomType = room.title + ' ' + occupancy + ' ' + mealPlan.name;
            var roomData = '<tr data-room-id="' + room.id + '"><td>' + roomType + '</td><td>Rs</td>';
        
            $('#tableHeader th[data-date]').each(function() {
                    var date = $(this).data('date');
                    var day = date.split('-')[2]; // Extract the day part
                    day = day.padStart(2, '0'); // Format day with leading zeros
                    var month = date.split('-')[1]; // Extract the month part
                    var year = date.split('-')[0]; // Extract the year part
                    date = year + '-' + month + '-' + day; // Reconstruct the formatted date
                    var value = ''; // Initialize value to empty string

                    // Search for the corresponding price in oyodata
                    oyodata.forEach(function(data) {
                        if (data.date === date && data.room_type === roomType) {
                            value = data.price;
                            return false; // Exit the loop once the price is found
                        }
                    });

                    roomData += '<td contenteditable="true" data-date="' + date + '">' + value + '</td>';
                    });
                    roomData += '</tr>';

            // Generate a row for each combination of room type, occupancy, and meal plan
            $('#tableBody').append(roomData);
        });
    });
});


        },
        error: function(xhr, status, error) {
            console.error(error);
        }
    });
}


    function gatherFilledData() {
        var filledData = [];

        // Iterate over each table row
        $('#tableBody tr').each(function() {
    var rowData = {};
    var roomId = $(this).data('room-id'); // Get the room ID for this row

    var room_type = $(this).find('td:first').text();
    var roomData = $(this).find('td:first').text().trim().split(' '); // Extract room data from the first cell
    var room = roomData[0]; // Get the room name
    var occupancy = roomData[2]; // Get the occupancy
    var mealPlan = roomData[3]; // Get the meal plan

    var hasNonEmptyValue = false; 

    // Iterate over each table cell within the row
    $(this).find('td[data-date]').each(function() {
        var date = $(this).data('date'); // Get the date for this cell
        var value = $(this).text().trim(); // Get the text value of the cell
        
        // Only include non-empty values
        if (value !== '') {
            rowData[date] = value;
            hasNonEmptyValue = true;
           
        }
    });
    
    // Check if rowData is not empty
   
    if (!$.isEmptyObject(rowData)) {
       
        // Include room ID and row data
        filledData.push({ roomId: roomId, occupancy: occupancy, mealPlan: mealPlan, data: rowData, room: room,room_type: room_type });
    }
});


        return filledData;
    }

    // Event listener for the submit button
    $('#submitButton').on('click', function() {
        var filledData = gatherFilledData();
        $('#messageContainer').show();
        // Log or send filled data via AJAX
        console.log(filledData);

        // Example AJAX request
        $.ajax({
            url: "<?php echo e(route('update_room_data')); ?>",
            method: 'get',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: { filledData: filledData },
            success: function(response) {
               
                console.log(response); // Optionally handle success response
            },
            error: function(xhr, status, error) {
                $('#messageContainer').text(response.message).show();
                console.error(error);
            }
        });
    });


});

</script>

<script>
$(document).ready(function(){
    // Function to update data
    function updateData() {
        $.ajax({
            url: 'no_show', // Replace 'your-api-endpoint' with the actual endpoint URL
            type: 'GET',
            success: function(data) {
                // Update your webpage content with the received data
               
            },
            error: function(xhr, status, error) {
                console.error('Error fetching data:', error);
            }
        });
    }

    // Call updateData function initially when the page loads
    updateData();

    // Call updateData function every minute
    setInterval(updateData, 60000); // 60000 milliseconds = 1 minute
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/revenue/index.blade.php ENDPATH**/ ?>