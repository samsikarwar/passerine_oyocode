

<?php $__env->startSection('content'); ?>
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Food Table</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="post" action="<?php echo e(route('add_food_table')); ?>">
                    <?php echo csrf_field(); ?>
                        <div class="row">
                        
                            <div class="col-md-3">
                               <label>Add Table</label>
                               <input type="number" class="form-control" required name="table_num">
                            </div>
                            <div class="col-md-3"><button class="btn btn-info"style="margin-top:25px;">Submit</button></div>
                        </div>
                    </form>
                </div> 
                <div class="row"> 
                </div>
                    
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('jquery'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/backend/addfoodtable.blade.php ENDPATH**/ ?>