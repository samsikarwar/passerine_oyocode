
<?php $__env->startSection('content'); ?>

    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>-->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <!--<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <style>
        .fc-event-container{
            display:none;
            /*opacity:0.1;*/
        }
        .fc-row.fc-week.fc-widget-content {
            max-height: 6em;
        }
        td.fc-day.fc-widget-content{
            vertical-align: middle;
        }
        .event-count{
            left: 40px;
            position: relative;
            font-size: 20px;
            background: darkgreen;
            color: white;
            padding: 7px;
        }
        .headingtext{
            font-size: 25px;
            color: black;
            font-weight: 600;
            background: lightgrey;
            padding: 1%;
        }
        .modal {
            background: white;
            /*position: absolute;*/
            float: left;
            left: 50%;
            top: 45%;
            transform: translate(-50%, -50%);
            width:70%;
        }
        .modal-content{
            box-shadow: none;
            border: none;
            padding: 5%;
            background: aliceblue;
        }
        
        .trstyle{
            background: black;
            color: white;
        }
        .task{
            color: black;
        }
        .event-difference{
            margin-left: 44px;
        }
    </style>
  
    <div class="container">
        <!--<h1> Inventory Details </h1>-->
        <div id='calendar'></div>
    </div>
    
    <div id="myModal" class="modal">
        <div class="modal-content">
            <div>
                <span class="headingtext">Arrival List Booked Rooms Detail</span>
                <span class="close" id="modelclosex">&times;</span>
            </div>
            
            <div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr class="trstyle">
                            <th>S.no</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Customer ID </th>
                            <th>Check In </th>
                            <th>Check Out</th>
                            <th>Duration </th>
                            <th>Amount </th>
                            <th>Source </th>
                            <th>No. of Rooms</th>
                        </tr>
                    </thead>
                    <tbody id="task" class="task">
                              
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <script>
    
        $("#modelclosex").click(function() {
            $("#myModal").modal('hide');
        });
      
    </script>


<script>
$(document).ready(function () {
   
var SITEURL = "<?php echo e(url('/')); ?>";
//   alert(SITEURL);
$.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

    // var date = new Date();
    // var d = date.getDate();
    // var m = date.getMonth();
    // var y = date.getFullYear();
  
    var calendar = $('#calendar').fullCalendar({
       
        // header: {
        //     right: 'month,agendaWeek,prev,today,next',
        //     left: 'title',
        // },
        //  initialView: 'timeGrid',
        //  visibleRange: {
        //     start: '2022-06-01',
        //     end: '2023-06-01'
        //   },
        // editable: true,
        // events: SITEURL + "/fullcalender",
        // displayEventTime: false,
        editable: false,
        eventColor: '#378006',
        displayEventTime: false,
        events: function( start, end, timezone, callback ) { 
          $.ajax({
            url: '/inventory',
            type: 'GET',
            dataType: 'JSON',
            success: function(data) {
                additionalData = data.additionalData;
              var events = [];
              if (data.events != null) {
                $.each(data.events, function(i, item) {
                  events.push({
                    
                    start: item.start,
                    title: item.id.toString(),
                    display: item.start.length,
                    room_qty: item.room_qty
                    
                  })
                
                })
              }
            //   console.log('events', events.length);
              callback(events);
            }
          })
        },

        // eventRender: function (event, element, view) { 
        //     $(element).each(function () { 
        //         $(this).attr('date-num', event.start.format('YYYY-MM-DD')); 
        //     });
        // },

        eventRender: function(event, element, view) {
  var roomQty = parseInt(event.room_qty) || 0;
  $(element).data('room-qty', roomQty); // Store room_qty as a data attribute
  $(element).each(function() {
    $(this).attr('date-num', event.start.format('YYYY-MM-DD'));
  });
},
        
eventAfterAllRender: function(view) {
  for (cDay = view.start.clone(); cDay.isBefore(view.end); cDay.add(1, 'day')) {
    var dateNum = cDay.format('YYYY-MM-DD');
    var dayEl = $('.fc-day[data-date="' + dateNum + '"]');
    var totalRoomQty = 0; // Initialize a variable to store the sum of roomQty values

    $('.fc-event[date-num="' + dateNum + '"]').each(function() {
      var roomQty = parseInt($(this).data('room-qty')) || 0; // Parse room_qty as an integer or use 0 if it's not a valid number
      totalRoomQty += roomQty; // Add the room_qty value to the total
    });

    if (totalRoomQty > 0) {
      var html = '<span class="event-count">' +
        '<i>' +
        totalRoomQty + // Display the total room_qty count
        '</i>' +
        ' / ' + additionalData +
        '</span>'+
  '<span class="event-difference">' +
  '<b>' +
  (additionalData - totalRoomQty) + ' Left' // Display the subtraction result
  '</b>' +
  '</span>';

      dayEl.append(html);
    }
  }
},

        
        selectable: false,
        selectHelper: true,
        
        eventClick: function (event,date,calEvent) {
            var datecurent = event.start.format('YYYY-MM-DD');
        
            $.ajax({
                url: '/fullcalenderAjax',
                data: {
                    eventdatemain: datecurent,
                },

                type: 'POST',
                
                dataType: 'JSON',
                success: function(data) {
                  console.log(data);
                  var html = "";
                    $.each(data, function(i, item) {
                        console.log(item.id);
                        html+=` <tr id="${item.id}">
                          <td>${i+1}</td>
                          <td>${item.customer_id}</td>
                          <td>${item.check_in}</td>
                          <td>${item.check_out}</td>
                          <td>${item.duration_of_stay}</td>
                          <td>${item.payment}</td>
                          <td>${item.referred_by_name}</td>
                          <td>${item.room_qty}</td>
                      </tr>`
                      $('#task').html(html);
                      $("#myModal").modal('show');
                    })
                    
                    // <td>${item.name}</td>
                    //       <td>${item.email}</td>
                    //       <td>${item.mobile}</td>
                    // <td class="d-flex">
                    //         <button id ="edit-exam"  data-id="${item.id}" style="color:white" class="mb-2 mr-2 btn btn-primary">Edit</button>
                    //         <button id="delete-exam" data-id="${item.id}" class="mb-2 mr-2 btn btn-danger">Delete</button>
                    //       </td>
                 
                }
            });
            
        },
        
        dayClick: function(event, jsEvent, view, resource) {
            //   alert('clicked ' + event.format());
            var datecurent = event.format('YYYY-MM-DD');
            var date = moment().format('YYYY-MM-DD');
            
            if(datecurent >= date ){
                $.ajax({
                url: '/fullcalenderAjax',
                data: {
                    eventdatemain: datecurent,
                },

                type: 'POST',
                
                dataType: 'JSON',
                success: function(data) {
                //   console.log(data);
                //   var j = 1;
                  var html = "";
                    $.each(data, function(i, item) {
                        console.log(item.id);
                        html+=` <tr id="${item.id}">
                          <td>${i+1}</td>
                          <td>${item.name}</td>
                          <td>${item.mobile}</td>
                          <td>${item.customer_id}</td>
                          <td>${item.check_in}</td>
                          <td>${item.check_out}</td>
                          <td>${item.duration_of_stay}</td>
                          <td>${item.payment}</td>
                          <td>${item.referred_by_name}</td>
                          <td>${item.room_qty}</td>
                      </tr>`
                      $('#task').html(html);
                      $("#myModal").modal('show');
                    })
                
                 
                }
            });
            }
            
        
        },
                    
    });
 
});
 
function displayMessage(message) {
    toastr.success(message, 'Event');
} 
  
</script>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master_backend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\lajpatnagar\lajpatnagar\resources\views/fullcalender.blade.php ENDPATH**/ ?>