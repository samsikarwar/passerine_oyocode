<table class="table table-bordered">
  <thead>
    <tr>
      <th><?php echo e(lang_trans('txt_sno')); ?></th>
      <th>Product</th>
      <th>Price</th>
      <th>Quantity</th>
      <th>Stock</th>
      <th>By</th>
      <th>Date</th>
    </tr>
  </thead>
  <tbody>
    <?php if($datalist->count()>0): ?>
    <?php $__currentLoopData = $datalist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td><?php echo e($k+1); ?></td>
        <td><?php echo e($val->product->name); ?></td>
        <td><?php if($val->price>0): ?> <?php echo e(getCurrencySymbol()); ?> <?php echo e($val->price); ?> <?php endif; ?></td>
        <td><?php echo e($val->qty); ?></td>
        <td><?php echo e(ucfirst($val->stock_is)); ?></td>
        <td><?php echo e(ucfirst($val->user->name)); ?></td>
        <td><?php echo e(dateConvert($val->created_at,'d-m-Y h:i')); ?></td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
  </tbody>
</table><?php /**PATH /home/passerine/public_html/lajpatnagar.f9hotels.com/new11/resources/views/excel_view/stock_history_excel.blade.php ENDPATH**/ ?>